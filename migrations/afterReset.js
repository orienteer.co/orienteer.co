#!/usr/bin/env node
const PgPromise = require("pg-promise")();

const E = process.env;
const url = E.GM_DBURL;
const pg = PgPromise(url);

(async function () {
  try {
    //let  database = E.DATABASE_NAME;
    //if(E.GM_SHADOW)
    console.log(
      await pg.any(`
GRANT CONNECT ON DATABASE ${E.GM_DBNAME} TO ${E.DATABASE_OWNER};
GRANT CONNECT ON DATABASE ${E.GM_DBNAME} TO ${E.DATABASE_AUTHENTICATOR};
GRANT ALL ON DATABASE ${E.GM_DBNAME} TO ${E.DATABASE_OWNER};

-- Some extensions require superuser privileges, so we create them before migration time.
CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;
CREATE EXTENSION IF NOT EXISTS citext WITH SCHEMA public;
CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;
`)
    );
  } catch (x) {
    console.log("ERROR: ", x);
  } finally {
    pg.$pool.end();
  }
})();
