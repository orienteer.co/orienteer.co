--! Previous: sha1:468959bd139075fb6a8fbcf708a699c4cc3e857f
--! Hash: sha1:1fbca5e347c0e9e2854a8b5cb60f4ecea75370c5

-- Enter migration here

DROP POLICY IF EXISTS public_readable ON app_public.course_users;
DROP FUNCTION IF EXISTS app_public.public_course_ids;

CREATE FUNCTION app_public.public_course_ids()
RETURNS SETOF uuid AS $$
  SELECT id FROM app_public.courses WHERE courses.public = true;
$$ language sql;
GRANT EXECUTE ON FUNCTION app_public.public_course_ids() TO ":DATABASE_VISITOR";



CREATE POLICY public_readable ON app_public.course_users FOR SELECT USING (
  course_id in (select app_public.public_course_ids())
);
