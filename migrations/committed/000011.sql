--! Previous: sha1:3cc0108d00f040aab41f11301b5a8eaedc770b4a
--! Hash: sha1:70f859eec31188b44a05af02a12f23d756f7b4ab

DROP TABLE IF EXISTS app_public."print_files";

CREATE TABLE app_public."print_files" (
id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
updated_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,

course_id uuid REFERENCES app_public.courses(id) ON DELETE CASCADE NOT NULL,
name text,
url text
);

create trigger _100_timestamps
  before insert or update on app_public."print_files"
  for each row
  execute procedure app_private.tg__timestamps();

ALTER TABLE app_public."print_files" ENABLE ROW LEVEL SECURITY;
GRANT ALL PRIVILEGES ON app_public."print_files" to ":DATABASE_VISITOR";

DROP POLICY IF EXISTS all_view_public ON app_public."print_files";
DROP POLICY IF EXISTS view_if_allowed ON app_public."print_files";
DROP POLICY IF EXISTS edit_if_allowed ON app_public."print_files";  

CREATE POLICY all_view_public ON app_public."print_files" FOR SELECT USING (( SELECT true AS bool
   FROM app_public.courses
  WHERE ((courses.id = "print_files".course_id) AND (courses.public))
  ));

CREATE POLICY view_if_allowed ON app_public."print_files" FOR SELECT USING ((
  SELECT "read" FROM app_public.course_users WHERE
    course_users.course_id = "print_files".course_id AND
    course_users.user_id = app_public.current_user_id())
    );

CREATE POLICY edit_if_allowed ON app_public."print_files" FOR ALL USING ((
  SELECT "write" FROM app_public.course_users WHERE
    course_users.course_id = "print_files".course_id AND
    course_users.user_id = app_public.current_user_id())
    );
