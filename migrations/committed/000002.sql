--! Previous: sha1:21d1ce12b26ca13dc87ebc65e2e7bab6238198fa
--! Hash: sha1:b1784e1e36d080401866828cd4f4b5d4832ed97f

DROP TABLE IF EXISTS app_public.courses CASCADE;
DROP TABLE IF EXISTS app_public.checkpoints CASCADE;  
DROP TABLE IF EXISTS app_public.course_users CASCADE;
DROP TABLE IF EXISTS app_public.courses_users CASCADE;

CREATE OR REPLACE FUNCTION app_public.set_uuid_default() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
  IF new.uuid IS NULL THEN
    new.uuid := uuid_generate_v4();
  END IF;
  return new;
end;
$$;

CREATE TABLE app_public.courses (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(), 

    "public" boolean DEFAULT false,
    created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,

    name TEXT,
    description TEXT,

    print_config JSONB
);

create trigger _100_timestamps
  before insert or update on app_public.courses
  for each row
  execute procedure app_private.tg__timestamps();

ALTER TABLE app_public.courses ENABLE ROW LEVEL SECURITY;
GRANT ALL PRIVILEGES ON app_public.courses to ":DATABASE_VISITOR";

CREATE TABLE app_public.course_users (
  user_id integer REFERENCES app_public.users(id) ON DELETE CASCADE,
  course_id uuid REFERENCES app_public.courses(id) ON DELETE CASCADE,

  created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,

  owner boolean default false,
  read boolean default false,
  write boolean default false
);

create trigger _100_timestamps
  before insert or update on app_public.course_users
  for each row
  execute procedure app_private.tg__timestamps();

ALTER TABLE ONLY app_public.course_users ADD CONSTRAINT unique_user_course UNIQUE ( user_id,course_id );
ALTER TABLE app_public.course_users ENABLE ROW LEVEL SECURITY;
GRANT ALL PRIVILEGES ON app_public.course_users to ":DATABASE_VISITOR";


DROP POLICY IF EXISTS all_view_public ON app_public.courses;
DROP POLICY IF EXISTS view_readable ON app_public.courses;
DROP POLICY IF EXISTS all_createable ON app_public.courses;

CREATE POLICY all_view_public ON app_public.courses FOR SELECT using (public);
CREATE POLICY view_readable ON app_public.courses FOR SELECT USING
  ((select course_users.read OR course_users.owner AS bool FROM app_public.course_users WHERE course_users.user_id = app_public.current_user_id() AND courses.id = course_users.course_id));
CREATE POLICY edit_editable ON app_public.courses FOR ALL USING
  ((select course_users.write OR course_users.owner AS bool FROM app_public.course_users WHERE course_users.user_id = app_public.current_user_id() AND courses.id = course_users.course_id));
  
CREATE POLICY all_createable ON app_public.courses FOR INSERT WITH CHECK (true);

DROP POLICY IF EXISTS own_readable ON app_public.course_users;
DROP POLICY IF EXISTS owner_editable ON app_public.course_users;
CREATE POLICY own_readable ON app_public.course_users FOR SELECT USING (course_users.user_id = app_public.current_user_id());

DROP FUNCTION IF EXISTS app_public.current_user_permissioned_course_ids;
CREATE FUNCTION app_public.current_user_permissioned_course_ids()
RETURNS SETOF uuid AS $$
  SELECT course_id FROM app_public.course_users WHERE app_public.course_users.user_id = app_public.current_user_id();
$$ language sql security definer;
GRANT EXECUTE ON FUNCTION app_public.current_user_permissioned_course_ids() TO ":DATABASE_VISITOR";

DROP FUNCTION IF EXISTS app_public.current_user_owned_course_ids;
CREATE FUNCTION app_public.current_user_owned_course_ids()
RETURNS SETOF uuid AS $$
  SELECT course_id FROM app_public.course_users WHERE app_public.course_users.user_id = app_public.current_user_id() AND app_public.course_users.owner = true;
$$ language sql security definer;
GRANT EXECUTE ON FUNCTION app_public.current_user_owned_course_ids() TO ":DATABASE_VISITOR";

DROP POLICY IF EXISTS view_others ON app_public.course_users;
CREATE POLICY view_others ON app_public.course_users FOR SELECT USING ((course_id IN (select app_public.current_user_permissioned_course_ids()) ));

DROP POLICY IF EXISTS create_if_owner ON app_public.course_users;
DROP POLICY IF EXISTS update_if_owner ON app_public.course_users;
DROP POLICY IF EXISTS delete_if_owner ON app_public.course_users;

CREATE POLICY create_if_owner ON app_public.course_users FOR INSERT
  WITH CHECK ( course_id in (select app_public.current_user_owned_course_ids()) );

CREATE POLICY update_if_owner ON app_public.course_users FOR UPDATE
  USING ( course_id in (select app_public.current_user_owned_course_ids()) )
  WITH CHECK ( course_id in (select app_public.current_user_owned_course_ids()) );

CREATE POLICY delete_if_owner ON app_public.course_users FOR DELETE
  USING ( course_id in (select app_public.current_user_owned_course_ids()) );

CREATE OR REPLACE FUNCTION app_public.create_empty_course(name text,description text) RETURNS app_public.courses
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
declare
  niew app_public.courses;
begin
  INSERT INTO app_public.courses (id,"name","description") VALUES (uuid_generate_v4(),name,description) RETURNING * INTO niew;
  INSERT INTO app_public.course_users (user_id,course_id,owner,read,write) VALUES
    ( app_public.current_user_id() , niew.id , true, true, true );
  return niew;
end;
$$;

GRANT EXECUTE ON FUNCTION app_public.create_empty_course(name text, description text) TO ":DATABASE_VISITOR";


CREATE TABLE app_public.checkpoints (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    
    course_id uuid references courses(id) ON DELETE CASCADE,
    
    name character varying(255),
    description text,
    lat numeric(11,8) NOT NULL,
    lng numeric(11,8) NOT NULL,
    radius double precision DEFAULT 100
);

create trigger _100_timestamps
  before insert or update on app_public.checkpoints
  for each row
  execute procedure app_private.tg__timestamps();

CREATE FUNCTION app_public.courses_within_bounds(west double precision, east double precision, south double precision, north double precision) RETURNS SETOF app_public.courses
    LANGUAGE sql STABLE
    AS $$
  SELECT courses.* from courses INNER JOIN checkpoints ON checkpoints.course_id = courses.id
  -- For some reason, earth_box takes statute miles instead of meters, so we convert here:
  WHERE south <= checkpoints.lat AND checkpoints.lat <= north AND west <= checkpoints.lng AND checkpoints.lng <= east
  GROUP BY courses.id
$$;

-- CREATE INDEX checkpoint_location_index ON app_public.checkpoints USING gist (public.ll_to_earth((lat)::double precision, (lng)::double precision));

ALTER TABLE app_public.checkpoints ENABLE ROW LEVEL SECURITY;
GRANT ALL PRIVILEGES ON app_public.checkpoints TO ":DATABASE_VISITOR";

DROP POLICY IF EXISTS all_view_public ON app_public.checkpoints;
DROP POLICY IF EXISTS view_if_allowed ON app_public.checkpoints;
DROP POLICY IF EXISTS edit_if_allowed ON app_public.checkpoints;  

CREATE POLICY all_view_public ON app_public.checkpoints FOR SELECT USING (( SELECT true AS bool
   FROM app_public.courses
  WHERE ((courses.id = checkpoints.course_id) AND (courses.public))
  ));

CREATE POLICY view_if_allowed ON app_public.checkpoints FOR SELECT USING ((
  SELECT "read" FROM app_public.course_users WHERE
    course_users.course_id = checkpoints.course_id AND
    course_users.user_id = app_public.current_user_id())
    );

CREATE POLICY edit_if_allowed ON app_public.checkpoints FOR ALL USING ((
  SELECT "write" FROM app_public.course_users WHERE
    course_users.course_id = checkpoints.course_id AND
    course_users.user_id = app_public.current_user_id())
    );
    
-- create update or destroy if part of a course where they have perms

DROP TYPE IF EXISTS add_many_checkpoints_input CASCADE;

CREATE TYPE add_many_checkpoints_input AS (
  "course_id" UUID,
  "lat" DECIMAL(11,8),
  "lng" DECIMAL(11,8),
  "name" text,
  "description" text,
  "radius" double precision
);

CREATE OR REPLACE FUNCTION add_many_checkpoints(
  checkpoints add_many_checkpoints_input[]
)
  RETURNS void AS
$$
begin
  insert into "app_public"."checkpoints" (course_id,lat,lng,name,description,radius)
    select * from unnest(checkpoints); 
end;
$$
language plpgsql;
