--! Previous: sha1:d45782d5266032b44df6b45c33353baad062a72a
--! Hash: sha1:05292693c7e832356552a5ac79193c66638e54ab

-- Enter migration here

DROP POLICY IF EXISTS all_view_public ON app_public.run_events;
CREATE POLICY all_view_public ON app_public.run_events FOR SELECT USING (EXISTS(
  SELECT true AS bool FROM app_public.runs WHERE ((runs.id = run_events.run_id) AND runs.public)
));
