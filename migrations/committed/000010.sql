--! Previous: sha1:ac3d6b23b156964a247656722b06e155110fdddd
--! Hash: sha1:3cc0108d00f040aab41f11301b5a8eaedc770b4a

-- Enter migration here

ALTER TABLE checkins DROP CONSTRAINT IF EXISTS checkins_run_event_id_fkey;
COMMENT ON TABLE checkins is E'@foreignKey (run_event_id) references run_events (id)';
