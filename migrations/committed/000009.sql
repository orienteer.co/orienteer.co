--! Previous: sha1:05292693c7e832356552a5ac79193c66638e54ab
--! Hash: sha1:ac3d6b23b156964a247656722b06e155110fdddd

-- Enter migration here

DROP POLICY IF EXISTS public_readable ON app_public.run_users;
DROP FUNCTION IF EXISTS app_public.public_run_ids;

CREATE FUNCTION app_public.public_run_ids()
RETURNS SETOF uuid AS $$
  SELECT id FROM app_public.runs WHERE runs.public = true;
$$ language sql;
GRANT EXECUTE ON FUNCTION app_public.public_run_ids() TO ":DATABASE_VISITOR";


CREATE POLICY public_readable ON app_public.run_users FOR SELECT USING (
  run_id in (select app_public.public_run_ids())
);
