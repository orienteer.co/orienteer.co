--! Previous: sha1:b1784e1e36d080401866828cd4f4b5d4832ed97f
--! Hash: sha1:5827b6350f2bf497b7301bab885b9faf9d96e303

DROP TABLE IF EXISTS app_public.runs CASCADE;
DROP TABLE IF EXISTS app_public.checkins CASCADE;
DROP TABLE IF EXISTS app_public.run_events CASCADE;
DROP TABLE IF EXISTS app_public.run_users CASCADE;

CREATE TABLE app_public.runs (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(), 
  
  created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,

  public boolean default true,

  course_id uuid REFERENCES app_public.courses(id) ON DELETE CASCADE NOT NULL,

  "started_at" timestamp,
  "ended_at" timestamp
);

CREATE TABLE app_public.run_users (
  user_id integer REFERENCES app_public.users(id) ON DELETE CASCADE NOT NULL,
  run_id uuid REFERENCES app_public.runs(id) ON DELETE CASCADE NOT NULL,

  created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL
);

ALTER TABLE ONLY app_public.run_users ADD CONSTRAINT unique_user_run UNIQUE ( user_id,run_id );

CREATE TABLE app_public.run_events (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(), 
  time timestamp without time zone not null,

  created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,

  user_id integer REFERENCES app_public.users(id) ON DELETE CASCADE NOT NULL,
  run_id UUID REFERENCES app_public.runs(id) ON DELETE CASCADE,
  type text,

  lat numeric(11,8),
  lng numeric(11,8),

  user_agent text,
  data jsonb
);

CREATE TABLE app_public.checkins (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(), 
  
  created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
  "time" timestamp without time zone NOT NULL,

  run_id UUID REFERENCES app_public.runs(id) ON DELETE CASCADE NOT NULL,
  user_id integer REFERENCES app_public.users(id) ON DELETE CASCADE,

  checkpoint_id UUID REFERENCES app_public.checkpoints(id) ON DELETE CASCADE NOT NULL,

  lat numeric(11,8) NOT NULL,
  lng numeric(11,8) NOT NULL,
  run_event_id UUID REFERENCES app_public.run_events NOT NULL
);

create or replace function app_private.default_user() returns trigger as $$
begin
  NEW.user_id = app_public.current_user_id();
  return NEW;
end;
$$ language plpgsql volatile set search_path to pg_catalog, public, pg_temp;
comment on function app_private.default_user() is
  E'This trigger should be called on all tables with created_at, updated_at - it ensures that they cannot be manipulated and that updated_at will always be larger than the previous updated_at.';


create trigger _100_timestamps
  before insert or update on app_public.runs
  for each row
  execute procedure app_private.tg__timestamps();

create trigger _100_timestamps
  before insert or update on app_public.checkins
  for each row
  execute procedure app_private.tg__timestamps();

create trigger _100_timestamps
  before insert or update on app_public.run_events
  for each row
  execute procedure app_private.tg__timestamps();

create trigger _200_default_user
  before insert or update on app_public.run_events
  for each row
  execute procedure app_private.default_user();

ALTER TABLE app_public.runs ENABLE ROW LEVEL SECURITY;
ALTER TABLE app_public.run_users ENABLE ROW LEVEL SECURITY;
ALTER TABLE app_public.run_events ENABLE ROW LEVEL SECURITY;
ALTER TABLE app_public.checkins ENABLE ROW LEVEL SECURITY;

GRANT ALL PRIVILEGES ON app_public.runs to ":DATABASE_VISITOR";
GRANT ALL PRIVILEGES ON app_public.run_users to ":DATABASE_VISITOR";
GRANT ALL PRIVILEGES ON app_public.run_events to ":DATABASE_VISITOR";
GRANT ALL PRIVILEGES ON app_public.checkins to ":DATABASE_VISITOR";

DROP POLICY IF EXISTS insert_all ON app_public.run_events;
DROP POLICY IF EXISTS select_all ON app_public.run_events;
DROP POLICY IF EXISTS insert_all ON app_public.checkins;
DROP POLICY IF EXISTS select_all ON app_public.checkins;

/*
CREATE POLICY insert_all ON app_public.run_events FOR INSERT
 with check (true);
CREATE POLICY select_all ON app_public.run_events FOR SELECT
 using (true);

CREATE POLICY insert_all ON app_public.checkins FOR INSERT
 with check (true);
CREATE POLICY select_all ON app_public.checkins FOR SELECT
 using (true);
*/

DROP POLICY IF EXISTS read_your_own ON app_public.run_users;
CREATE POLICY read_your_own ON app_public.run_users FOR SELECT USING (user_id = app_public.current_user_id());

DROP POLICY IF EXISTS all_view_public ON app_public.runs;
CREATE POLICY all_view_public ON app_public.runs FOR SELECT using (public);

DROP POLICY IF EXISTS participants_can_use ON app_public.runs;
CREATE POLICY participants_can_use ON app_public.runs FOR ALL USING
  (EXISTS(select true FROM 
    app_public.run_users WHERE run_users.user_id = app_public.current_user_id() 
      AND runs.id = run_users.run_id));
    


DROP FUNCTION IF EXISTS app_public.current_user_participating_runs;
CREATE FUNCTION app_public.current_user_participating_runs()
RETURNS SETOF uuid AS $$
  SELECT run_id FROM app_public.run_users WHERE app_public.run_users.user_id = app_public.current_user_id();
$$ language sql security definer;
GRANT EXECUTE ON FUNCTION app_public.current_user_participating_runs() TO ":DATABASE_VISITOR";

DROP POLICY IF EXISTS all_view_public ON app_public.checkins;
CREATE POLICY all_view_public ON app_public.checkins FOR SELECT USING (EXISTS(
  SELECT true AS bool FROM app_public.runs WHERE ((runs.id = checkins.run_id) AND runs.public)
));

DROP POLICY IF EXISTS edit_if_allowed ON app_public.checkins;

-- TODO: Call up benije and ask about why this:
CREATE POLICY create_any ON app_public.checkins FOR INSERT WITH CHECK (true);

CREATE POLICY edit_if_allowed ON app_public.checkins FOR UPDATE USING (EXISTS(
  SELECT true FROM app_public.run_users WHERE 
    run_users.run_id = checkins.run_id AND
    run_users.user_id = app_public.current_user_id()
)) WITH CHECK (EXISTS(
  SELECT true FROM app_public.run_users WHERE
    run_users.run_id = run_id AND 
    run_users.user_id = app_public.current_user_id()
));

-- Create a run out of thin air:

CREATE OR REPLACE FUNCTION app_public.create_empty_run(run_id uuid, course_id uuid) RETURNS app_public.runs
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
declare
  niew app_public.runs;
  existing integer; -- app_public.runs;
begin
  select count(*) into existing from app_public.runs where runs.id = run_id;

-- TODO: prevent creating runs for courses you can't view:
  if ( existing = 0 ) then
    INSERT INTO app_public.runs ("id","course_id") VALUES (run_id,course_id) RETURNING * INTO niew;
    INSERT INTO app_public.run_users ("user_id","run_id") VALUES
      ( app_public.current_user_id() , niew.id );
  else
    select * into niew from app_public.runs where runs.id = run_id;
  end if;
  return niew;
end;
$$;

GRANT EXECUTE ON FUNCTION app_public.create_empty_run(run_id uuid, course_id uuid) TO ":DATABASE_VISITOR";

CREATE POLICY edit_if_allowed ON app_public.run_events FOR ALL USING 
  (user_id = app_public.current_user_id()) WITH CHECK (user_id = app_public.current_user_id());

create or replace function set_user_id_default() returns trigger as $$
begin
  IF new.user_id IS NULL THEN
    new.user_id := app_public.current_user_id();
  END IF;
  return new;
end;
$$ language plpgsql;


CREATE TRIGGER run_events_user_id_default BEFORE INSERT
  ON app_public.run_events
  FOR EACH ROW
  EXECUTE PROCEDURE set_user_id_default();
