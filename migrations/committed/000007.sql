--! Previous: sha1:db63df9c768465bea798cf633bf10f8466362b62
--! Hash: sha1:d45782d5266032b44df6b45c33353baad062a72a

-- Enter migration here
DROP FUNCTION IF EXISTS app_public.runs_checkpoints_count(runs);
CREATE FUNCTION app_public.runs_checkpoints_count(run runs) RETURNS bigint AS $$
  select count(distinct checkins.checkpoint_id) from app_public."checkins"
  join app_public.courses ON courses.id = run.course_id
  where checkins.run_id = run.id
  ;
$$ LANGUAGE sql STABLE;

comment on function app_public.runs_checkpoints_count(run runs) is E'@filterable\n@sortable';

DROP FUNCTION IF EXISTS app_public.runs_total_time(runs);
CREATE FUNCTION app_public.runs_total_time(run runs) RETURNS interval AS $$
  select (coalesce(run.ended_at,NOW()) - coalesce(run.started_at,NOW()));
$$ LANGUAGE sql STABLE;

comment on function app_public.runs_total_time(runs) is E'@filterable\n@sortable';
