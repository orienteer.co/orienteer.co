--! Previous: sha1:70f859eec31188b44a05af02a12f23d756f7b4ab
--! Hash: sha1:f2ef8e97abdbf3c5ec4a9692de31cd54e1a0c1f0

-- Enter migration here

ALTER TABLE checkpoints ALTER COLUMN radius SET DEFAULT 50;
