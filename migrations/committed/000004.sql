--! Previous: sha1:5827b6350f2bf497b7301bab885b9faf9d96e303
--! Hash: sha1:468959bd139075fb6a8fbcf708a699c4cc3e857f

-- foo bar

CREATE OR REPLACE FUNCTION app_public.courses_within_bounds(west double precision, east double precision, south double precision, north double precision) RETURNS SETOF app_public.courses
    LANGUAGE sql STABLE
    AS $$
  SELECT courses.* from app_public.courses INNER JOIN app_public.checkpoints ON checkpoints.course_id = courses.id
  -- For some reason, earth_box takes statute miles instead of meters, so we convert here:
  WHERE south <= checkpoints.lat AND checkpoints.lat <= north AND west <= checkpoints.lng AND checkpoints.lng <= east
  GROUP BY courses.id
$$;

DROP FUNCTION IF EXISTS app_public.courses_writeable(courses);
CREATE FUNCTION app_public.courses_writeable(course courses) RETURNS boolean AS $$
  SELECT COALESCE((SELECT write FROM app_public.course_users WHERE 
    course_users.user_id = app_public.current_user_id() 
    AND course_users.course_id = course.id)
  ,false);
$$ LANGUAGE sql STABLE;

comment on function app_public.courses_writeable(course courses) is E'@filterable';
