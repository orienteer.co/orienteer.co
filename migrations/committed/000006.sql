--! Previous: sha1:1fbca5e347c0e9e2854a8b5cb60f4ecea75370c5
--! Hash: sha1:db63df9c768465bea798cf633bf10f8466362b62

-- Enter migration here

ALTER TABLE app_public.courses ADD COLUMN start_checkpoint_id uuid references app_public.checkpoints(id);
ALTER TABLE app_public.courses ADD COLUMN end_checkpoint_id uuid references app_public.checkpoints(id);
