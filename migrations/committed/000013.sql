--! Previous: sha1:f2ef8e97abdbf3c5ec4a9692de31cd54e1a0c1f0
--! Hash: sha1:d8ff5900b9c1baaa38cbf7535436899c43a4c4a7

-- Enter migration here

alter table app_public.users drop constraint users_username_check;
alter table app_public.users add constraint users_username_check check(length(username) >= 2 and length(username) <= 24 and username ~ '^[a-zA-Z]([a-zA-Z0-9_]?)+$');
