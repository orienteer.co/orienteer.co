import * as nodemailer from "nodemailer";
const mg = require("nodemailer-mailgun-transport");
import chalk from "chalk";
import fs from "fs";

const isDev = process.env.NODE_ENV !== "production";

let transporterPromise;
const etherealFilename = `${process.cwd()}/.ethereal`;

let logged = false;

export default function getTransport() {
  if (!transporterPromise) {
    transporterPromise = (async () => {
      if (isDev) {
        return nodemailer.createTransport({
          host: "mail",
          port: 1025,
          secure: false,
        });
      } else {
        console.log("Production mailgunning...");
        if (!process.env.MAILGUN_API_KEY) {
          throw new Error("Misconfiguration: no MAILGUN_API_KEY");
        }
        if (!process.env.MAILGUN_DOMAIN) {
          throw new Error("Misconfiguration: no MAILGUN_DOMAIN");
        }

        return nodemailer.createTransport(
          mg({
            auth: {
              api_key: process.env.MAILGUN_API_KEY,
              domain: process.env.MAILGUN_DOMAIN,
            },
          })
        );
      }
    })();
  }

  return transporterPromise;
}
