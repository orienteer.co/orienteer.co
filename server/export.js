var app = require("express")();
import { _gql } from "../plugins/net";

async function courseFor(req) {
  const gql = _gql(req);
  let { result, error } = await gql(
    `
query($id: UUID!) { 
  course(id: $id) {
    name
    description
    checkpoints:checkpoints(orderBy: [NAME_ASC]) { 
      nodes { id name description lat lng radius }
    }
  }
}`,
    { id: req.params.id }
  );

  if (error) {
    throw new Error(error);
  }

  return result.course;
}

app.get("/courses/:id/export.gpx", async function (req, res, next) {
  try {
    let course = await courseFor(req);

    let wpts = course.checkpoints.nodes.map(
      (checkpoint) => `
<wpt lat="${checkpoint.lat}" lon="${checkpoint.lng}">
<name>${checkpoint.name}</name>
<desc>${checkpoint.description || ""}</desc>
</wpt>`
    );

    res.set("content-type", "application/gpx+xml");
    res.set("content-disposition", `attachment; filename="${course.name}.gpx"`);
    res.send(`
<?xml version="1.0"?>
<gpx xmlns="http://www.topografix.com/GPX/1/1" xmlns:gpxx="http://www.garmin.com/xmlschemas/GpxExtensions/v3" creator="ORIENTEER.CO" version="1.1">
${wpts.join()}
</gpx>
`);
  } catch (x) {
    console.error("Import Error: ", x.stack);
    res.send(`Error: ${x.toString()}`);
  }
});

app.get("/courses/:id/export.json", async function (req, res, next) {
  try {
    let course = await courseFor(req);

    res.set(
      "content-disposition",
      `attachment; filename="${course.name}.json"`
    );
    res.send({
      type: "FeatureCollection",
      features: course.checkpoints.nodes.map((checkpoint) => ({
        type: "Feature",
        id: checkpoint.id,
        properties: {
          "marker-symbol": "c:ring",
          "marker-color": "FF0000",
          description: checkpoint.description,
          title: checkpoint.name,
          class: "Marker",
          folderId: null,
          "marker-rotation": null,
        },
        geometry: {
          coordinates: [checkpoint.lng, checkpoint.lat, 0, 0],
          type: "Point",
        },
      })),
    });
  } catch (x) {
    console.error("Import Error: ", x.stack);
    res.send(`Error: ${x.toString()}`);
  }
});

module.exports = app;
