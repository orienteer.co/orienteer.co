import { _gql } from "../plugins/net";
import bodyParser from "body-parser";

module.exports = function (app) {
  console.log("Mounting tracker:");

  app.post("/track/:runId", bodyParser.json(), async (req, res, next) => {
    console.log("tracking.");

    const rootPgPromise = app.get("rootPgPromise");

    const currentSession = await await rootPgPromise.oneOrNone(
      "select * from app_private.sessions where uuid = $1",
      [req.user.session_id]
    );

    console.log("CURR SES: ", currentSession);

    const gql = _gql(req);
    try {
      let { result, error } = await gql(MUTATION, {
        ...req.body[0],
        runId: req.params.runId,
        time: new Date(req.body[0].time),
        userId: currentSession.user_id,
      });

      console.log("RETURNED: ", result, error);
    } catch (x) {
      console.log("ERROR YO: ", x);
    }
    res.status(200).end();
  });
};

const MUTATION = `
mutation($runId: UUID! $latitude: BigFloat $longitude:BigFloat $data:JSON $time:Datetime! $userId: Int!) {
  createRunEvent(input: { runEvent: {
    runId: $runId
    lat: $latitude
    lng: $longitude
    time: $time
    data: $data
    userId: $userId
  }}) { runEvent { runId id lat lng time data user { name } }}
}
`;
