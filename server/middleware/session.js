import session from "express-session";
import ConnectPgSimple from "connect-pg-simple";

const PgStore = ConnectPgSimple(session);

const MILLISECOND = 1;
const SECOND = 1000 * MILLISECOND;
const MINUTE = 60 * SECOND;
const HOUR = 60 * MINUTE;
const DAY = 24 * HOUR;

const { COOKIE_SECRET } = process.env;
if (!COOKIE_SECRET) {
  throw new Error("Server misconfigured");
}
const MAXIMUM_SESSION_DURATION_IN_MILLISECONDS =
  parseInt(process.env.MAXIMUM_SESSION_DURATION_IN_MILLISECONDS || "", 10) ||
  365 * DAY;

function redisStore(options) {
  const ConnectRedis = require("connect-redis");
  const redis = require("redis");

  const RedisStore = ConnectRedis(session);

  return new RedisStore(options);
}

export default (app) => {
  const store = process.env.REDIS_URL
    ? /*
       * Using redis for session storage means the session can be shared across
       * multiple Node.js instances (and survives a server restart), see:
       *
       * https://medium.com/mtholla/managing-node-js-express-sessions-with-redis-94cd099d6f2f
       */
      redisStore({
        client: redis.createClient({
          url: process.env.REDIS_URL,
        }),
      })
    : /*
       * Using PostgreSQL for session storage is easy to set up, but increases
       * the load on your database. We recommend that you graduate to using
       * redis for session storage when you're ready.
       */
      new PgStore({
        pgPromise: app.get("rootPgPromise"),
        schemaName: "app_private",
        tableName: "connect_pg_simple_sessions",
      });

  const sessionMiddleware = session({
    rolling: true,
    saveUninitialized: false,
    resave: false,
    cookie: {
      maxAge: MAXIMUM_SESSION_DURATION_IN_MILLISECONDS,
    },
    store,
    secret: COOKIE_SECRET,
  });

  app.use(sessionMiddleware);
  app.get("websocketMiddleware").push(sessionMiddleware);
};
