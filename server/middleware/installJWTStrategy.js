import passport from "passport";
import { Strategy as JwtStrategy, ExtractJwt } from "passport-jwt";
import JWT from "jsonwebtoken";

var opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = process.env.JWT_SECRET;
opts.issuer = "orienteer.co";
opts.audience = "orienteer.co";

export default (app) => {
  const rootPgPromise = app.get("rootPgPromise");

  passport.use(
    new JwtStrategy(opts, (jwt_payload, done) => {
      done(null, { session_id: jwt_payload.sub });
    })
  );

  app.use((req, res, next) => {
    if (!req.get("authorization")) return next();

    passport.authenticate(
      "jwt",
      { session: false },
      function (err, user, info) {
        if (user) req.user = user;

        next();
      }
    )(req, res, next);
  });

  app.use("/app/*", async (req, res, next) => {
    if (!req.user) return next();

    const currentSession = await await rootPgPromise.oneOrNone(
      "select * from app_private.sessions where uuid = $1",
      [req.user.session_id]
    );

    if (currentSession) {
      const newSession = await rootPgPromise.oneOrNone(
        `insert into app_private.sessions (user_id) values ($1) returning *`,
        [currentSession.user_id]
      );

      req.authToken = JWT.sign({}, process.env.JWT_SECRET, {
        expiresIn: "365d",
        audience: "orienteer.co",
        issuer: "orienteer.co",
        subject: String(newSession.uuid),
      });
    }

    next();
  });
};
