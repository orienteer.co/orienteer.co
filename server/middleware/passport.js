import passport from "passport";
import { Strategy as GoogleStrategy } from "passport-google-oauth20";
import installPassportStrategy from "./installPassportStrategy.js";
import { get } from "../../lib/utils";
import installJWTStrategy from "./installJWTStrategy.js";

export default async function (app) {
  passport.serializeUser((sessionObject, done) => {
    done(null, sessionObject.session_id);
  });

  passport.deserializeUser((session_id, done) => done(null, { session_id }));

  const initMiddleware = passport.initialize();
  app.use(initMiddleware);
  // TODO: Add websocket middleware?

  const sessMiddleware = passport.session();
  app.use(sessMiddleware);

  app.get("/logout", (req, res) => {
    req.logout();
    res.redirect("/");
  });

  installJWTStrategy(app);

  if (process.env.GOOGLE_CLIENT_ID) {
    installPassportStrategy(
      app,
      "google",
      GoogleStrategy,
      {
        clientID: process.env.GOOGLE_CLIENT_ID,
        clientSecret: process.env.GOOGLE_CLIENT_SECRET,
        // callbackURL: 'https://stage.orienteer.co/auth/google/callback',
        /*callbackURL:
          "http://orienteer.co.192.168.64.5.xip.io/auth/google/callback", */
        callbackURL: `${process.env.BASE_URL}/auth/google/callback`,
      },
      { scope: ["profile", "email"] },
      async (profile, _accessToken, _refreshToken, _extra, _req) => {
        console.log("PROFILE: ", profile);
        console.log("EXTRA: ", _extra);

        return {
          id: profile.id,
          displayName: profile.displayName || "",
          // username: profile.username,
          email: get(profile, "emails.0.value"),
          avatarUrl: get(profile, "photos.0.value"),
        };
      },
      ["access_token", "id_token"]
    );
  }
}
