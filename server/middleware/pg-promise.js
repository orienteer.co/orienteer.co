const PgPromise = require("pg-promise");

function cachedPromise(url) {
  if (process.env.NODE_ENV == "production") return PgPromise()(url);

  global.__pg_cache = global.__pg_cache || {};
  if (global.__pg_cache[url]) return global.__pg_cache[url];

  return (global.__pg_cache[url] = PgPromise()(url));
}

function authUrl(env) {
  return `postgresql://${env.DATABASE_AUTHENTICATOR}:${env.DATABASE_AUTHENTICATOR_PASSWORD}@${env.POSTGRES_HOST}:5432/${env.DATABASE_NAME}`;
}

function rootUrl(env) {
  return `postgresql://${env.DATABASE_OWNER}:${env.DATABASE_OWNER_PASSWORD}@${env.POSTGRES_HOST}:5432/${env.DATABASE_NAME}`;
}

export default (app) => {
  // todo swallow global errors?
  app.set(
    "rootPgPromise",
    cachedPromise(
      process.env.DATABASE_OWNER_URL ||
        process.env.ROOT_DATABASE_URL ||
        rootUrl(process.env)
    )
  );

  app.set(
    "authPgPromise",
    cachedPromise(
      process.env.DATABASE_USER_URL ||
        process.env.DATABASE_URL ||
        authUrl(process.env)
    )
  );
};
