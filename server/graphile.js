var express = require("express");
var JWT = require("jsonwebtoken");
var stream = require("stream");
import { resolve } from "path";

import { NodePlugin } from "graphile-build";
import PgSimplifyInflectorPlugin from "@graphile-contrib/pg-simplify-inflector";
import PassportLoginPlugin from "./graphile/PassportLoginPlugin";
import PgConnectionFilterPlugin from "postgraphile-plugin-connection-filter";

const {
  ConstraintErrorTagsPlugin,
  parseErrors,
} = require("@graphile-contrib/constraint-error-tags");

const StripePlugin = require("./graphile/stripe");

// import { PgMutationUpsertPlugin } from 'o@fullstackio/postgraphile-upsert-plugin';
// import { PgMutationUpsertPlugin } from './graphile/upsert';
import { PgMutationUpsertPlugin } from "postgraphile-upsert-plugin";

const { postgraphile } = require("postgraphile");
import { makePgSmartTagsFromFilePlugin } from "postgraphile/plugins";

const TagsFilePlugin = makePgSmartTagsFromFilePlugin(
  // We're using JSONC for VSCode compatibility; also using an explicit file
  // path keeps the tests happy.
  resolve(__dirname, "postgraphile.tags.json5")
);

function pipeOut(external, internal, response) {
  var pipe = new stream.PassThrough();

  external.writeHead(response.statusCode, response.headers);

  internal.on("data", function (f) {
    external.write(f);
  });
  internal.on("end", function (f) {
    external.end();
  });
}

function uuidOrNull(input) {
  if (!input) return null;
  const str = String(input);
  if (
    /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i.test(
      str
    )
  ) {
    return str;
  } else {
    return null;
  }
}

var pg;

module.exports = function (options = {}) {
  var app = new express();

  var prefix = options.prefix || "";
  var isDev = process.env.NODE_ENV == "development";
  console.log("IS DEV: ", isDev);

  app.use(async (req, res, next) => {
    const rootPgPromise = app.get("rootPgPromise");
    const rootPgPool = rootPgPromise.$pool;

    pg =
      pg ||
      postgraphile(app.get("authPgPromise").$pool, "app_public", {
        jwtSecret: process.env.JWT_SECRET,
        jwtAudiences: ["orienteer.co"],
        graphiql: true,
        ownerConnectionString: rootPgPool,
        extendedErrors: isDev ? ["hint", "detail", "errcode"] : undefined,
        enableQueryBatching: true,
        showErrorStack: isDev ? "json" : undefined,
        dynamicJson: true,
        enhanceGraphiql: isDev,
        allowExplain: isDev,
        appendPlugins: [
          TagsFilePlugin,
          PgSimplifyInflectorPlugin,
          ConstraintErrorTagsPlugin,
          PgConnectionFilterPlugin,

          StripePlugin,
          PgMutationUpsertPlugin,
          PassportLoginPlugin,
        ],
        skipPlugins: [
          // Disable the 'Node' interface
          NodePlugin,
        ],
        watchPg: isDev,
        handleErrors: !isDev ? (errors) => parseErrors(errors) : null,

        async pgSettings(req) {
          const sessionId = req.user && uuidOrNull(req.user.session_id);
          if (sessionId) {
            // Update the last_active timestamp (but only do it at most once every 15 seconds to avoid too much churn).
            await rootPgPromise.any(
              "UPDATE app_private.sessions SET last_active = NOW() WHERE uuid = $1 AND last_active < NOW() - INTERVAL '15 seconds'",
              [sessionId]
            );
          }
          return {
            // Everyone uses the "visitor" role currently
            role: process.env.DATABASE_VISITOR,

            /*
             * Note, though this says "jwt" it's not actually anything to do with
             * JWTs, we just know it's a safe namespace to use, and it means you
             * can use JWTs too, if you like, and they'll use the same settings
             * names reducing the amount of code you need to write.
             */
            "jwt.claims.session_id": sessionId,
          };
        },

        async additionalGraphQLContextFromRequest(req) {
          return {
            sessionId: req.user && uuidOrNull(req.user.session_id),
            rootPgPool,
            rootPgPromise,

            // Use this to tell Passport.js we're logged in
            login: (user) =>
              new Promise((resolve, reject) => {
                req.login(user, (err) => (err ? reject(err) : resolve()));
              }),

            logout: () => {
              req.logout();
              return Promise.resolve();
            },
          };
        },
      });

    try {
      return await pg(req, res, next);
    } catch (x) {
      console.log("ERROR: ", x);
      res.send(500, { error: x });
    }
  });

  return app;
};
