const app = require("express")();
import installPgPromise from "./middleware/pg-promise";
import installSession from "./middleware/session";
import installPassport from "./middleware/passport";
import bodyParser from "body-parser";

if (!process.env.CI_BUILD_ID) {
  app.set("websocketMiddleware", []);
  installPgPromise(app);
  installSession(app);
  installPassport(app);

  app.use(require("./import"));
  app.use(require("./export"));
  app.post("/upload", ...require("./upload"));

  require("./track")(app);

  app.use("/print", require("./print"));

  var Graphile = require("./graphile");

  app.use(Graphile());

  if (false && process.env.NODE_ENV == "development") {
    console.log("WORKER!");

    const { run } = require("graphile-worker");
    const path = require("path");

    async function main() {
      const runner = await run({
        watch: true,
        connectionString: process.env.ROOT_DATABASE_URL,
        concurrency: 1,
        pollInterval: 1000,
        taskDirectory: path.resolve(`${__dirname}/../tasks`),
      });
    }

    main().catch((err) => {
      console.error(err);
    });
  }
}

module.exports = app;
