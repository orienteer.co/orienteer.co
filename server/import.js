var multer = require("multer");
var app = require("express")();
import { _gql } from "../plugins/net";

const xml2jslib = require("xml2js");

async function xml2js(string) {
  return new Promise((resolve, reject) => {
    xml2jslib.parseString(string, (err, result) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(result);
    });
  });
}

// Multer is required to process file uploads and make them available via
// req.files.
const m = multer({
  storage: multer.memoryStorage(),
  limits: {
    fileSize: 5 * 1024 * 1024, // no larger than 5mb
  },
});

function gpxToCheckpoints(gpx) {
  return gpx.wpt.map((wpt) => ({
    lat: wpt.$.lat,
    lng: wpt.$.lon,
    name: wpt.name && wpt.name[0],
    description: (wpt.cmt && wpt.cmt[0]) || (wpt.desc && wpt.desc[0]),
  }));
}

app.post("/courses/import", m.single("file"), async function (req, res, next) {
  try {
    const gql = _gql(req);

    if (!req.file) {
      console.error("no file present");
      return res.status(400).json({ error: { message: "no file present" } });
    }

    var string = req.file.buffer.toString("UTF-8");
    const xml = await xml2js(string);

    const name = req.file.originalname
      .match(/(.*?)(?:\..*)?$/)[1]
      .replace(/[_-]+/g, " ");
    const checkpoints = gpxToCheckpoints(xml.gpx);

    let course;

    {
      let { result, error } = await gql(
        `
mutation($name: String!) { 
  createEmptyCourse(input: { name: $name }) {
    course {
      id
    }
  }
}`,
        { name: name }
      );

      if (error) {
        console.log("ERROR: ", error);
        return res.json({ error });
      }

      course = result.createEmptyCourse.course;
    }

    {
      let { result, error } = await gql(
        `
mutation($checkpoints: [AddManyCheckpointsInputRecordInput]) {
addManyCheckpoints(input: { checkpoints: $checkpoints }) {
  clientMutationId
}
}`,
        { checkpoints: checkpoints.map((a) => ({ ...a, courseId: course.id })) }
      );
      if (error) {
        console.error("ERROR: ", error);
        return res.json({ error });
      }
    }

    res.json({ result: { course } });
  } catch (x) {
    console.error("Import Error: ", x.stack);
    res.json({ error: x.toString() });
  }
});

module.exports = app;
