// Load dependencies
const aws = require("aws-sdk");

const multer = require("multer");
const multerS3 = require("multer-s3");
const uuid = require("uuid/v4");

// Use our env vars for setting credentials.
// Remove lines 11-14 if using ~/.aws/credentials file on a local server.

const s3 = require("./s3.js");

// Change bucket property to your Space name
const upload = multer({
  storage: multerS3({
    s3: s3,
    bucket: process.env.AWS_BUCKET || "default-wont-work",
    acl: "public-read",
    key: function (request, file, cb) {
      console.log(file);
      cb(null, uuid() + "/" + file.originalname);
    },
  }),
}).array("file", 1);

module.exports = [
  function (req, res, next) {
    console.log("PASS: ", req.session.passport);
    var passport = req.session.passport;
    if (!passport || !passport.user) {
      return res.send(401, "not authorized");
    }
    next();
  },
  function (request, response, next) {
    upload(request, response, function (error) {
      if (error) {
        console.log(error);
        return response.send(400, "server error");
      }

      response.send(200, {
        result: {
          url: request.files[0].location,
          uuid: request.files[0].location.match(
            /[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}/
          )[0],
          name: request.files[0].location.match(/([^\/]*?)$/)[0],
        },
      });
    });
  },
];
