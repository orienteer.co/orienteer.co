const { makeExtendSchemaPlugin, gql } = require("graphile-utils");

const stripe = require("stripe")(process.env.STRIPE_SECRET_KEY);
stripe.setApiVersion("2018-11-08");

async function prepareCustomer(customer) {
  return {
    ...customer,
    subscription: customer.subscriptions && customer.subscriptions.data[0],
    default_source: !!customer.default_source,
  };
}

async function getCustomer(stripeId) {
  return prepareCustomer(await stripe.customers.retrieve(stripeId));
}

async function getStripeCustomer(context, options = {}) {
  const { rows } = await context.pgClient.query(
    "select id,username from  app_public.current_user();"
  );
  if (!rows) throw new Error("not signed in");

  const { id, username } = rows[0];
  let { stripe_customer_id: stripeId } =
    (await context.rootPgPromise.oneOrNone(
      "select stripe_customer_id from app_private.stripe_accounts where user_id = $1",
      [id]
    )) || {};

  if (stripeId) {
    if (options.idOnly) return { id: stripeId };
    else return await getCustomer(stripeId);
  }

  // console.log("ID/USERNAME: ",id,username);
  let customer = await stripe.customers.create({
    description: username,
    metadata: { orienteer_id: id },
  });
  console.log("CUSTOMER: ", customer);

  let res = await context.rootPgPromise.oneOrNone(
    "INSERT INTO app_private.stripe_accounts (user_id,stripe_customer_id) VALUES ($1,$2) RETURNING *;",
    [id, customer.id]
  );
  console.log("Created customer rel: ", res);
  return customerd;
}

const StripePlugin = makeExtendSchemaPlugin((build) => {
  const { pgSql: sql } = build;
  return {
    typeDefs: gql`
      type StripePlan {
        id: String
        nickname: String
        active: Boolean
        amount: Int
        currency: String
        interval: String
        metadata: JSON
      }

      type StripeSubscription {
        id: String
        current_period_end: String
        current_period_start: String
        plan: StripePlan
        status: String
      }

      type StripeAccount {
        id: String
        email: String
        default_source: Boolean
        subscription: StripeSubscription
      }

      extend type Query {
        getStripeAccount: StripeAccount
        allStripePlans(product: String): [StripePlan]
      }

      extend type Mutation {
        stripeSubscribe(token: String, planId: String!): StripeAccount
        stripeUnsubscribe(subscriptionId: String, planId: String): StripeAccount
        createDefaultSource(token: String!): StripeAccount
        deleteDefaultSource: StripeAccount
      }
    `,
    resolvers: {
      Query: {
        getStripeAccount: async function (
          _query,
          args,
          context,
          resolveInfo,
          { selectGraphQLResultFromTable }
        ) {
          return await getStripeCustomer(context);
        },
        allStripePlans: async function (_query, inArgs, context, resolveInfo) {
          var args = { ...inArgs };
          if (!("product" in args)) {
            args.product = process.env.STRIPE_DEFAULT_PRODUCT;
          }

          return (await stripe.plans.list(args)).data;
        },
      },
      Mutation: {
        stripeSubscribe: async (
          _query,
          args,
          context,
          resolveInfo,
          { selectGraphQLResultFromTable }
        ) => {
          const customer = await getStripeCustomer(context);
          var source = null;

          console.log("Creating subscription...", customer);

          if (!customer.default_source && !args.token) {
            throw new Error("No source specified");
          }

          for (var i in customer.subscriptions.data) {
            var s = customer.subscriptions.data[i];

            if (s.status == "active") {
              await stripe.subscriptions.del(s.id);
            }
          }

          if (args.token) {
            source = await stripe.sources.create({
              token: args.token,
              type: "card",
              usage: "reusable",
            });
            await stripe.customers.createSource(customer.id, {
              source: source.id,
            });
          }

          let opts = {
            customer: customer.id,
            items: [{ plan: args.planId }],
          };

          console.log("Creating subscription: ", opts);
          var subscription = await stripe.subscriptions.create(opts);

          console.log("SUB: ", subscription);

          return await getCustomer(customer.id);
        },
        stripeUnsubscribe: async (
          _query,
          args,
          context,
          resolveInfo,
          { selectGraphQLResultFromTable }
        ) => {
          const customer = await getStripeCustomer(context);
          var source = null;

          var subs = customer.subscriptions && customer.subscriptions.data;

          if (!args.subscriptionId && !args.planId) {
            throw new Error("subscription or plan ID required");
          }

          var sub = subs.find(
            (s) => s.id == args.subscriptionId || s.plan.id == args.planId
          );

          if (!sub) {
            throw new Error("Could not find your subscription");
          }

          console.log("DELETING SUBSCRIPTION ", sub.id);

          await stripe.subscriptions.del(sub.id);

          return await getCustomer(customer.id);
        },
        createDefaultSource: async (_query, args, context, resolveInfo) => {
          const customer = await getStripeCustomer(context, { idOnly: true });

          return prepareCustomer(
            await stripe.customers.update(customer.id, { source: args.token })
          );
        },
        deleteDefaultSource: async (_query, args, context, resolveInfo) => {
          const customer = await getStripeCustomer(context);

          var subs = customer.subscriptions && customer.subscriptions.data;

          if (subs.length > 0) {
            throw new Error(
              "Cannot remove default source while subscriptions are active"
            );
          }

          await stripe.customers.deleteSource(
            customer.id,
            customer.default_source
          );
          return getCustomer(customer.id);
        },
      },
    },
  };
});

module.exports = StripePlugin;
