const app = require("express")();
const unet = require("unet");
const s3 = require("./s3");
const util = require("util");

const getSignedUrl = util.promisify(s3.getSignedUrl.bind(s3));

const { _gql } = require("../plugins/net");

const { get } = require("../lib/utils");

app.get("/meta/:courseId", async (req, res, next) => {
  if (!req.get("accept") || req.get("accept").includes("text/html"))
    return next();

  const rootPg = app.get("rootPgPromise");
  const gql = _gql(req);

  const { result, error } = await gql(
    `
    query($id: UUID!) {
      course : course(id: $id) { ...fields }
    }

    fragment fields on Course { 
      updatedAt 
      id name description public
      printConfig
      users:courseUsers { nodes { user { id name } } }
      checkpoints {
        count:totalCount
        _:nodes {
          name
          description
          lat
          lng
          radius
          updatedAt
        }
        }
    }
  `,
    { id: req.params.courseId }
  );

  if (error) {
    console.log("ERRORIZ: ", error);
    res.status(500).send(error);
    return;
  }

  const checkpoints = get(result, "course.checkpoints._");
  const updatedAts = [
    result.course.updatedAt,
    ...checkpoints.map((c) => c.updatedAt),
  ].map((d) => new Date(d).getTime());

  const mapAt = req.query.map_at;

  const latest = mapAt || Math.max(...updatedAts);
  const file = `${result.course.id}-${latest}.pdf`;

  // look up file in S3, otherwise create task and wait...

  let object;
  const key = `generated-maps/v2/${file}`;
  const params = { Bucket: process.env.AWS_BUCKET, Key: key };

  object = await s3
    .headObject(params)
    .promise()
    .catch((e) => {
      if (!e || e.code == "NotFound") {
        console.log("Returning null");
        return null;
      }
      throw new Error(e.code);
    });

  if (object) {
    if (req.get("accept") == "application/json") {
      res.status(200).json({
        waiting: false,
        file: await getSignedUrl("getObject", params),
      });
      return;
    }

    res.status(200);

    res.set("content-type", object.ContentType);
    res.set("content-length", object.ContentLength);

    if (req.query.download) {
      res.set(`content-disposition`, `inline; filename="${file}"`);
    }

    s3.getObject(params).createReadStream().pipe(res);

    return;
  }

  console.log("Doing the real work...");

  const e = await rootPg.manyOrNone(
    `SELECT * FROM graphile_worker.jobs WHERE queue_name = $1`,
    [result.course.id]
  );

  const style = require("../lib/map-style/thunderforest").default;

  if (e.length == 0) {
    const r = await rootPg.one(
      `SELECT graphile_worker.add_job('print',$1,$2);`,
      [
        {
          key: key,
          course: result.course,
          pages: result.course.printConfig.pages,
          style: style,
        },
        result.course.id,
      ]
    );
  }

  if (req.get("accept") == "application/json") {
    return res.status(200).json({ waiting: true, job: e[e.length - 1] });
  }

  // let Nuxt handle the rest, if necessary:
  next();
});

module.exports = app;
