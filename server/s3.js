const aws = require("aws-sdk");

if (process.env.AWS_ACCESS_KEY_ID && process.env.AWS_SECRET_ACCESS_KEY) {
  aws.config.update({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  });
} else {
  console.warn("AWS access keys not configured");
}

let spacesEndpoint;

if (process.env.AWS_ENDPOINT) {
  // Set S3 endpoint to DigitalOcean Spaces
  spacesEndpoint = new aws.Endpoint(process.env.AWS_ENDPOINT);
}

const s3 = new aws.S3({
  endpoint: spacesEndpoint,
});

module.exports = s3;
