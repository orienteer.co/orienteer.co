const task = async (inPayload, { addJob }) => {
  const payload = inPayload;
  const { email } = payload;

  const sendEmailPayload = {
    options: {
      to: email,
      subject: `Password reset request failed: you don't have an Orienteer.co account`,
    },
    template: "password_reset_unregistered.mjml",
    variables: {
      url: process.env.BASE_URL,
    },
  };
  await addJob("send_email", sendEmailPayload);
};

module.exports = task;
