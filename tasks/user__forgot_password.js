const task = async (inPayload, { addJob, withPgClient }) => {
  console.log("SEND EMAIL");

  const payload = inPayload;
  const { id: userId, email, token } = payload;
  const {
    rows: [user],
  } = await withPgClient((pgClient) =>
    pgClient.query(
      `
        select users.*
        from app_public.users
        where id = $1
      `,
      [userId]
    )
  );
  if (!user) {
    console.error("User not found; aborting");
    return;
  }
  const sendEmailPayload = {
    options: {
      to: email,
      subject: "Password reset",
    },
    template: "password_reset.mjml",
    variables: {
      token,
      verifyLink: `${process.env.BASE_URL}/reset?user_id=${encodeURIComponent(
        user.id
      )}&token=${encodeURIComponent(token)}`,
    },
  };
  await addJob("send_email", sendEmailPayload);
};

module.exports = task;
