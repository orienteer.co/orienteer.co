// TODO: Text!
const app = require("express")();
const unet = require("unet");
const { PassThrough } = require("stream");
const utm = require("utm");
const { UTMProjection } = require("./print/projections");
const Renderer = require("./print/Renderer");

const concurrently = require("../lib/concurrently");
const fs = require("fs");
const path = require("path");
const crypto = require("crypto");
const cachedir = require("cachedir");
var zlib = require("zlib");
// const style = require("./print-styles");
// const union = require("./print/union");

const { report } = require("../plugins/errors");
const Turf = require("@turf/turf");
const { featureToGeoJSON, union } = require("./print/geojson");

const { get, streamToBuffer, deepEqual, uuid } = require("../lib/utils");

const PDFDocument = require("pdfkit");
const { _gql } = require("../plugins/net");
const s3 = require("../server/s3");

import createGeometryChecker from "./print/geometry-checker";

const uploadStream = (args) => {
  const pass = new PassThrough();
  return {
    writeStream: pass,
    promise: s3.upload({ ...args, Body: pass }).promise(),
  };
};

function unzip(data) {
  return new Promise((res, rej) =>
    zlib.gunzip(data, (err, buffer) => (err ? rej(err) : res(buffer)))
  );
}

function latLngBounds(points) {
  let w, e, s, n;

  points.forEach((p) => {
    w = Math.min(p.lng, w) || p.lng;
    e = Math.max(p.lng, e) || p.lng;
    s = Math.min(p.lat, s) || p.lat;
    n = Math.max(p.lat, n) || p.lat;
  });

  return { w, e, s, n };
}

function hash(object) {
  const shasum = crypto.createHash("sha1");
  shasum.update(JSON.stringify(object));
  return shasum.digest("hex");
}

function cache(callback) {
  return async function (...args) {
    const key = hash({ v: 0, args });
    const dir = cachedir("orienteer");
    const f = `${dir}/${key}`;

    if (fs.existsSync(f)) {
      return await streamToBuffer(fs.createReadStream(f));
    }

    const r = await callback.call(this, ...args);
    fs.mkdirSync(dir, { recursive: true });
    fs.writeFileSync(f, r);
    return r;
  };
}

const utmToPage = function (page, u) {
  return {
    x: toUnit(page.bounds.w, page.bounds.e, u.easting) * page.width,
    y: toUnit(page.bounds.n, page.bounds.s, u.northing) * page.height,
  };
};

const latLngToPage = function (page, p) {
  const u = utm.fromLatLon(Number(p.lat), Number(p.lng));
  return utmToPage(page, u);
};

const image = cache(async function (url) {
  const res = await unet({ method: "GET", url, middleware: [] });
  return res.body();
});

function latLngCenter(bounds) {
  return {
    lat: (bounds.s + bounds.n) / 2,
    lng: (bounds.e + bounds.w) / 2,
  };
}

const EARTH_EQUATORIAL_CIRCUMFERENCE_IN_METERS = 40.075e6;
const EARTH_MERIDIONAL_CIRCUMFERENCE_IN_METERS = 40.008e6;

function latitudeMeters(meters) {
  return (meters / EARTH_MERIDIONAL_CIRCUMFERENCE_IN_METERS) * 360;
}

// return the 0-1 value mapping value to low/high
function toUnit(start, end, value) {
  return (value - start) / (end - start);
}

function fromUnit(value, start, end) {
  return start + value * (end - start);
}

// modulo 1, GTE 0.
function mod1(value) {
  let c = value % 1;
  return c >= 0 ? c : c + 1;
}

function long2tile(lon, zoom) {
  return ((lon + 180) / 360) * Math.pow(2, zoom);
}
function lat2tile(lat, zoom) {
  return (
    ((1 -
      Math.log(
        Math.tan((lat * Math.PI) / 180) + 1 / Math.cos((lat * Math.PI) / 180)
      ) /
        Math.PI) /
      2) *
    Math.pow(2, zoom)
  );
}
function tile2long(x, z) {
  return (x / Math.pow(2, z)) * 360 - 180;
}
function tile2lat(y, z) {
  var n = Math.PI - (2 * Math.PI * y) / Math.pow(2, z);
  return (180 / Math.PI) * Math.atan(0.5 * (Math.exp(n) - Math.exp(-n)));
}

function latLngToTile(zoom, p) {
  return {
    x: long2tile(p.lng, zoom),
    y: lat2tile(p.lat, zoom),
  };
}

function tileToLatLng(zoom, t) {
  return {
    lat: tile2lat(t.y, zoom),
    lng: tile2long(t.x, zoom),
  };
}

function utmToTile(zoom, u) {
  if (
    typeof u.northing == "undefined" ||
    typeof u.easting == "undefined" ||
    !u.zoneNum ||
    !u.zoneLetter
  ) {
    console.log("invalid UTM: ", u);
    throw new Error("invalid UTM ");
  }
  const { latitude: lat, longitude: lng } = utm.toLatLon(
    u.easting,
    u.northing,
    u.zoneNum,
    u.zoneLetter
  );
  const tile = latLngToTile(zoom, { lat, lng });

  return tile;
}

function imageTilesForBounds(bounds, zoom) {
  let nw, se;

  if (bounds.zoneNum) {
    nw = utmToTile(zoom, {
      northing: bounds.n,
      easting: bounds.w,
      zoneNum: bounds.zoneNum,
      zoneLetter: bounds.zoneLetter,
    });
    se = utmToTile(zoom, {
      northing: bounds.s,
      easting: bounds.e,
      zoneNum: bounds.zoneNum,
      zoneLetter: bounds.zoneLetter,
    });
  } else {
    nw = latLngToTile(zoom, { lat: bounds.n, lng: bounds.w });
    se = latLngToTile(zoom, { lat: bounds.s, lng: bounds.e });
  }

  const tiles = [];

  for (var x = Math.floor(nw.x); x <= Math.ceil(se.x); x++) {
    for (var y = Math.floor(nw.y); y <= Math.ceil(se.y); y++) {
      const tileNW = tileToLatLng(zoom, { x: x, y });
      const tileNE = tileToLatLng(zoom, { x: x + 1, y: y });
      const tileSW = tileToLatLng(zoom, { x: x, y: y + 1 });
      const tileSE = tileToLatLng(zoom, { x: x + 1, y: y + 1 });

      const corners = { nw: tileNW, ne: tileNE, sw: tileSW, se: tileSE };

      tiles.push({ x: x, y: y, z: zoom, corners });
    }
  }

  return tiles;
}

const POINTS_TO_INCHES = 1 / 72;
const INCHES_TO_METERS = 0.0254;
const pdfPointsToMeters = (points) =>
  points * POINTS_TO_INCHES * INCHES_TO_METERS;
const metersToPDFPoints = (meters) =>
  meters / (POINTS_TO_INCHES * INCHES_TO_METERS);

async function renderUTMGrid(doc, options) {
  const page = doc.page;
  const bounds = page.bounds;

  for (let n = Math.floor(bounds.s / 1000) * 1000; n < bounds.n; n += 1000) {
    const left = utmToPage(page, {
      northing: n,
      easting: bounds.w,
      zoneLetter: bounds.zoneLetter,
      zoneNum: bounds.zoneNum,
    });
    const right = utmToPage(page, {
      northing: n,
      easting: bounds.e,
      zoneLetter: bounds.zoneLetter,
      zoneNum: bounds.zoneNum,
    });
    doc
      .save()
      .moveTo(left.x, left.y)
      .lineTo(right.x, right.y)
      .dash(5, { space: 5 })
      .stroke("#aaaabb")
      .restore();
  }

  for (let e = Math.floor(bounds.w / 1000) * 1000; e < bounds.e; e += 1000) {
    const top = utmToPage(page, {
      northing: bounds.n,
      easting: e,
      zoneLetter: bounds.zoneLetter,
      zoneNum: bounds.zoneNum,
    });
    const bottom = utmToPage(page, {
      northing: bounds.s,
      easting: e,
      zoneLetter: bounds.zoneLetter,
      zoneNum: bounds.zoneNum,
    });
    doc
      .save()
      .moveTo(top.x, top.y)
      .lineTo(bottom.x, bottom.y)
      .dash(5, { space: 5 })
      .stroke("#aaaabb")
      .restore();
  }
}

async function renderInfo(doc, { scale }) {
  const page = doc.page;
  const bounds = page.bounds;

  if (scale) {
    doc
      .roundedRect(10, 10, 100, 45, 4)
      .fillColor("white")
      .fillOpacity(0.7)
      .strokeColor("black")
      .lineWidth(0.5)
      .strokeOpacity(0.9)
      .fillAndStroke();

    // doc.moveTo(15, 15);
    doc.fillColor("black");

    doc.text(`Scale: 1:${Number(scale).toFixed(0)}`, 15, 15, { width: 90 });
    doc.moveDown(0.5);
    doc.text("orienteer.co");
  }
}

const { VectorTile, VectorTileFeature } = require("@mapbox/vector-tile");
const Protobuf = require("pbf");

class Feature {
  constructor(tileInfo, vectorTileFeature) {}
}

async function renderTileJSONLayer(renderer, options) {
  const zoom = options.zoom;

  console.log("URL: ", options.url);
  const { result: config, error } = await unet({
    method: "GET",
    url: options.url,
  });
  if (error) throw new Error(error);

  //  console.log("CONFIG: ",config);
  const tiles = imageTilesForBounds(renderer.bounds(), options.zoom);
  const types = [];

  const layers = {};

  await concurrently(3, tiles, async (t) => {
    // console.log(`BOO: ${config.tiles[0]} x${t.x} y${t.y} z${t.z}`);
    // const h = { x: 0 , y: 0, z: 1 };
    const h = { ...t };
    // h.y = (1 << t.z) - t.y;

    let base = config.tiles[0];
    // base = `https://api.mapbox.com/v4/mapbox.mapbox-terrain-v2,mapbox.mapbox-streets-v8/{z}/{x}/{y}.vector.pbf?access_token=pk.eyJ1IjoiZHN0YXVkaWdlbCIsImEiOiJjazNueTNoNm0xdjZ5M2ZudXY3ZWN4aW0xIn0.tCf460GP2JsHd1DxJ-nMNA`;
    const url = base.replace(/\{([xyz])\}/g, (m, k) => h[k]);

    // console.log("GET: ",url);
    const data = await image(url);

    const pb = new Protobuf(await unzip(data));
    const tile = new VectorTile(pb);

    for (let layerType in tile.layers) {
      if (!types.includes(layerType)) types.push(layerType);
      // if(!['hillshade','contour','road'].includes(layerType)) continue;

      const layer = tile.layers[layerType];
      layer.zoom = zoom;

      for (let i = 0; i < layer.length; i++) {
        const feature = featureToGeoJSON(layer.feature(i), t.x, t.y, t.z);
        const L = (layers[layerType] = layers[layerType] || []);
        L.push(feature);

        /*        try {
          tileTransformed(doc,t,{ scale: 1/layer.extent, clip: true },(doc) => renderGeom(doc,tile,layerType,feature));
        } catch(x) {
          console.error("Error tile transfomring: ",x);
          throw x;
        } */
      }
    }
  });

  function consolidate(features) {
    const groups = [];

    const start = new Date();
    features.forEach((f) => {
      // TODO: use a hash (stable object-to-json required) to speed
      // this up, a _lot_ of iterating over long arrays without it:
      const group = groups.find((g) =>
        deepEqual(g[0].properties, f.properties)
      );
      if (group) group.push(f);
      else {
        groups.push([f]);
      }
    });

    const sortedT = new Date();
    const unioned = groups.map((g) => union(g));
    const unionedT = new Date();
    const concatted = unioned.reduce((a, b) => a.concat(b), []);
    const concattedT = new Date();

    console.log(
      `Consolidating ${concattedT - start}ms total, Sorting ${
        sortedT - start
      }ms, Unioning: ${unionedT - sortedT}ms, concatting: ${
        concattedT - unionedT
      }ms.`
    );

    return concatted;
  }

  for (var k in layers) {
    const beforeC = new Date();
    const features = consolidate(layers[k]);
    const afterC = new Date();
    console.log(`Consolidating ${k} - ${(afterC - beforeC) / 1000}s`);

    // WIP: Anything that has a corner edges count 2 (i.e. is a
    // corner) should get turned from a Polygon into a Line, and have
    // the corners removed.
    const beforeR = new Date();

    for (var i in features) {
      const feature = features[i];

      //if (i == 75 && k == "elevation") {
      renderer.render(k, feature);
      //}
    }
    const afterR = new Date();
    console.log(`Rendering ${k} - ${(afterR - beforeR) / 1000}s`);
  }

  // debugger;
}

function tileTransformed(doc, tile, options, callback) {
  if (typeof options == "function") {
    callback = options;
    options = null;
  }

  doc.save();

  const scale = options && "scale" in options ? options.scale : 1;
  const clip = options && "clip" in options ? options.clip : false;

  try {
    // upper left, upper right, lower left, lower right
    const UL = latLngToPage(doc.page, tile.corners.nw);
    const UR = latLngToPage(doc.page, tile.corners.ne);
    const LL = latLngToPage(doc.page, tile.corners.sw);
    const LR = latLngToPage(doc.page, tile.corners.se);

    doc.transform(
      // vector of X axis in original coordinate system:
      (UR.x - UL.x) * scale,
      (UR.y - UL.y) * scale,
      // vector of Y axis in original coordinate system:
      (LL.x - UL.x) * scale,
      (LL.y - UL.y) * scale,
      UL.x,
      UL.y
    );
  } catch (x) {
    console.error("Error transforming for tile.", x);
    doc.restore();
    return;
  }

  try {
    if (options && options.clip) {
      doc.rect(0, 0, 1 / scale, 1 / scale).clip();
    }

    callback(doc);
  } catch (x) {
    console.error("Error in tile transformed callback: ", x);
    doc.restore();
    return;
    // THROW x;
  }

  doc.restore();
}

async function renderImageLayer(doc, options) {
  const bounds = doc.page.bounds;
  const tiles = imageTilesForBounds(bounds, options.zoom);

  doc.save().fillOpacity(options.opacity || 1);

  await concurrently(10, tiles, async (t) => {
    const url = options.url.replace(/\{([xyz])\}/g, (m, k) => t[k]);
    const buf = await image(url);

    tileTransformed(doc, t, (doc) =>
      doc.image(buf, 0, 0, { width: 1, height: 1 })
    );
  });
}

module.exports = async (payload, helpers) => {
  const { key, course, pages, style } = payload;
  // first, check if it already exists:

  const object = await s3
    .headObject({ Bucket: process.env.AWS_BUCKET, Key: key })
    .promise()
    .catch((x) => {
      if (x.code == "NotFound") return null;
      throw x;
    });

  if (object) {
    console.log("Already built, complete easy!");
    return;
  }

  const doc = new PDFDocument({ autoFirstPage: false });

  const checkpoints = get(course, "checkpoints._");

  for (var i in pages) {
    const pageMeta = pages[i];
    if (pageMeta.width == 0 || pageMeta.height == 0) continue;

    const centerUTM = pageMeta.center; // utm.fromLatLon(center.lat,center.lng);
    const center = utm.toLatLon(
      pageMeta.center.easting,
      pageMeta.center.northing,
      pageMeta.center.zoneNum,
      pageMeta.center.zoneLetter
    );

    const metersWide = Math.min(pageMeta.width, pageMeta.height);
    const pointsWide = 612.0;
    const scale = metersWide / pdfPointsToMeters(pointsWide);

    const layout = pageMeta.width > pageMeta.height ? "landscape" : "portrait";

    doc.addPage({ size: "letter", layout: layout });
    const page = doc.page;

    page.checkTextPath = createGeometryChecker();

    const projection = new UTMProjection({
      center,
      width: pageMeta.width,
      height: pageMeta.height,
      page,
    });

    const renderer = new Renderer({ projection, doc, style });

    const mapWidthInMeters = pageMeta.width;
    const mapHeightInMeters = pageMeta.height;

    // doc.height && width in PDF points - 72 / in.
    const mapBounds = {
      w: centerUTM.easting - mapWidthInMeters / 2,
      e: centerUTM.easting + mapWidthInMeters / 2,
      n: centerUTM.northing + mapHeightInMeters / 2,
      s: centerUTM.northing - mapHeightInMeters / 2,
      zoneNum: centerUTM.zoneNum,
      zoneLetter: centerUTM.zoneLetter,
    };

    doc.page.bounds = mapBounds;

    try {
      /*      await renderImageLayer(doc,{
        zoom: 15,
        url: `https://tile.thunderforest.com/outdoors/{z}/{x}/{y}.png?apikey=f2e014081ef54bc69ae4966ed9310b2c`,
      }); */

      await renderTileJSONLayer(renderer, {
        url: `https://tile.thunderforest.com/thunderforest.outdoors-v2.json?apikey=f2e014081ef54bc69ae4966ed9310b2c`,
        zoom: 14,
      });

      await renderUTMGrid(doc);
      await renderInfo(doc, { scale });

      checkpoints.forEach((cp) => {
        let c = latLngToPage(page, cp);
        if (
          c.x < 0 ||
          c.y < 0 ||
          c.x > doc.page.width ||
          c.y > doc.page.height
        ) {
          return;
        }
        let r = metersToPDFPoints(cp.radius) / scale;

        doc.circle(c.x, c.y, r).stroke("#ff0000");

        doc
          .fill("#ff0000")
          .fontSize(12)
          .text(cp.name || "unnamed cp", c.x + r, c.y + r, {
            lineBreak: false,
          });
      });
    } catch (x) {
      console.log("FUCK, FATAL FUCKUP");
      console.error(x);

      throw x;
    }
  }

  doc.end();

  console.log("UPLOADING TO ", process.env.AWS_BUCKET);

  const { writeStream, promise } = uploadStream({
    Bucket: process.env.AWS_BUCKET,
    Key: key,
    ContentType: "application/pdf",
  });

  doc.pipe(writeStream);

  await promise;
};
