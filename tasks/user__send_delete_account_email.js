const task = async (inPayload, { addJob }) => {
  const payload = inPayload;
  const { email, token } = payload;
  const sendEmailPayload = {
    options: {
      to: email,
      subject: "Confirmation required: really delete account?",
    },
    template: "delete_account.mjml",
    variables: {
      token,
      deleteAccountLink: `${
        process.env.BASE_URL
      }/settings/delete?token=${encodeURIComponent(token)}`,
    },
  };

  await addJob("send_email", sendEmailPayload);
};

module.exports = task;
