const { VectorTileFeature } = require("@mapbox/vector-tile");

const path = require("path");
const simplify = require("simplify-js");

const fs = require("fs");
const { HSLToRGB, RGBToHex } = require("../../lib/colors");

const StylePath = path.resolve(__dirname, "./custom");

const STYLE = require("../../lib/map-style/thunderforest");

import SVGtoPDF from "svg-to-pdfkit";
import fitCurve from "fit-curve";
import { report } from "../../plugins/errors";

function linearInterpolate(
  stop1Input,
  stop1Output,
  input,
  stop2Input,
  stop2Output
) {
  //  console.log("LININT: ",...arguments);
  if (!input || !stop2Input || stop1Input == stop2Input) return stop1Output;

  let unit = ((input - stop1Input) / (stop2Input - stop1Input)) % 1;
  if (unit < 0) ++unit;

  if (typeof stop1Output == "string") {
    if (stop1Output.match(/^hsl/)) {
      stop1Output = HSLToRGB(stop1Output);
    } else {
      console.log("Unkonwn interpolation for ", stop2Output);
    }
  }

  if (typeof stop2Output == "string") {
    if (stop2Output.match(/^hsl/)) {
      stop2Output = HSLToRGB(stop2Output);
    } else {
      console.log("Unkonwn interpolation for ", stop2Output);
    }
  }

  if (typeof stop1Output == "number") {
    const r = stop1Output + unit * (stop2Output - stop1Output);
    //    console.log("Number number ",stop1Output,unit,stop2Output,r);
    return r;
  }

  if (typeof stop1Output == "object" && stop1Output.length) {
    const r = [
      stop1Output[0] + unit * (stop2Output[0] - stop1Output[0]),
      stop1Output[1] + unit * (stop2Output[1] - stop1Output[1]),
      stop1Output[2] + unit * (stop2Output[2] - stop1Output[2]),
    ];
    //    console.log("returning interpolated: ",r);
    return r;
  }

  return stop1Output;
}

function drawGeom(typeName, geom) {
  return (doc) => {
    doc.moveTo(geom[0].x, geom[0].y);

    for (let j = 1; j < geom.length; j++) {
      doc.lineTo(geom[j].x, geom[j].y);
    }

    if (typeName == "Polygon") {
      doc.lineTo(geom[0].x, geom[0].y);
    }
  };
}

function pathToSVG(typeName, geom) {
  const svg = geom.map((p, i) => `${i == 0 ? "M" : "L"}${p.x},${p.y}`);
  if (typeName == "Polygon") svg.push("Z");
  return svg.join(" ");
}

const FILTERS = {
  all: function (args) {
    for (var i = 0; i < args.length; i++) {
      if (!this.op(args[i])) return false;
    }
    return true;
  },
  any: function (args) {
    for (var i = 1; i < args.length; i++) {
      if (this.op(args[i])) return true;
    }
    return false;
  },
  "!": function (args) {
    if (args.length != 1) {
      console.error("unexpected ! statement: ", args);
      return false;
    }

    return !this.op(args[0]);
  },
  "==": function (args) {
    if (args.length != 2) {
      console.error("unexpected == statement: ", args);
      return false;
    }

    return this.op(args[0]) == this.op(args[1]);
  },
  in: function (args) {
    if (args[1].includes(args[0])) return true;
    return false;
  },
  "<=": function (args) {
    if (args.length != 2) {
      console.error("unexpected <= statement: ", args);
      return false;
    }

    return this.op(args[0]) <= this.op(args[1]);
  },
  "<": function (args) {
    if (args.length != 2) {
      console.error("unexpected < statement: ", args);
      return false;
    }

    return this.op(args[0]) < this.op(args[1]);
  },
  concat: function (args) {
    return args.map((a) => this.op(a)).join("");
  },
  "to-string": function (args) {
    return String(this.op(args[0]));
  },
  has: function (args) {
    if (args.length == 1) {
      return args[0] in this.feature.properties;
    }
    console.log("Didn't understand GET statemnt: ", args);
    debugger;
  },
  literal: function (args) {
    return args[0];
  },
  "geometry-type": function () {
    return VectorTileFeature.types[this.feature.type];
  },
  ">=": function (args) {
    if (args.length != 2) {
      console.error("unexpected >= statement: ", args);
      return false;
    }
    return this.op(args[0]) >= this.op(args[1]);
  },
  "+": function (args) {
    return args.map((a) => this.op).reduce((a, b) => a + b);
  },
  ">": function (args) {
    if (args.length != 2) {
      console.error("unexpected > statement: ", args);
      return false;
    }

    return this.op(args[0]) > this.op(args[1]);
  },
  "!=": function (args) {
    if (args.length != 2) {
      console.error("unexpected != statement: ", args);
      return false;
    }
    return this.op(args[0]) != this.op(args[1]);
  },
  zoom: function (args) {
    return this.tile.layers[this.layer].zoom;
  },
  get: function (args) {
    if (args.length == 1) {
      const r = this.feature.properties[args[0]];
      // console.log("GET ",args[0],r,this.feature.properties);
      if (!r) return null; // throw new Error("Couldn't find ",args[0]);
      return r;
    }
    console.log("Didn't understand GET statemnt: ", args);
  },
  coalesce: function (args) {
    let r;
    for (var i = 0; i < args.length; i++) {
      r = this.op(args[i]);
      if (r != null) return r;
    }
    return r;
  },
  case: function (args) {
    for (var i = 0; i < args.length - 1; i += 2) {
      const c = this.op(args[i]);
      if (c == true) return this.op(args[i + 1]);
    }
    return this.op(args[args.length - 1]);
  },
  match: function (args) {
    const v = this.op(args[0]);
    //    console.log("MATCH ",args,v);

    // skip first and fallback (last)
    for (var i = 1; i < args.length - 1; i += 2) {
      const m = args[i];
      const o = this.op(args[i + 1]);

      //      console.log(`is ${v} in ${m}?`);

      if ((m.includes && m.includes(v)) || m == v) {
        //        console.log("YES!");
        return o;
      }
      //      console.log("NO :(");
    }

    // fallback:
    return this.op(args[args.length - 1]);
  },
  step: function (args) {
    const value = this.op(args[0]);

    for (let i = 1; i < args.length - 1; i += 2) {
      const stop = this.op(args[i + 1]);
      if (value <= stop) {
        return this.op(args[i]);
      }
    }
    return this.op(args[args.length - 1]);
  },
  interpolate: function (args) {
    // console.log("INTERPOLATE: ",args);
    const type = "linear" || args[0];
    const input = this.op(args[1]);

    // short circuit for now to the first stop output:
    // return this.op(args[3]);

    let stop1Input = this.op(args[2]);
    let stop1Output = this.op(args[3]);

    //  if(input <= this.op(args[2])) return this.op(args[3]);

    let stop2Input, stop2Output;
    let i = 2;
    for (; i < args.length; i += 2) {
      stop2Input = this.op(args[i]);
      stop2Output = this.op(args[i + 1]);

      if (input < this.op(args[i])) {
        break;
      }

      stop1Input = this.op(args[i]);
      stop1Output = this.op(args[i + 1]);
    }

    // TODO: Exponential interpolation
    const r = linearInterpolate(
      stop1Input,
      stop1Output,
      input,
      stop2Input,
      stop2Output
    );

    return r;
  },
  let: function (args) {
    for (let i = 0; i < args.length - 1; i += 2) {
      const name = args[i];
      const value = this.op(args[i + 1]);
      this.variables[name] = value;
    }
    return this.op(args[args.length - 1]);
  },
  var: function (args) {
    return this.variables[args[0]];
  },
};

function op(args) {
  if (args && typeof args == "object" && typeof args.length == "number") {
    if (typeof args[0] != "string") return args;

    const fn = FILTERS[args[0]];
    if (fn) return fn.call(this, args.slice(1));
    else {
      console.log("Unknown filter ", args[0]);
      return args;
    }
  }
  return args;
}

function filter(statement) {
  if (!statement) return true;

  if (statement && typeof statement == "object" && statement.length) {
    return this.op(statement);
  }

  console.log("unknown statement type " + typeof statement, statement);
}

class StyleSet {
  constructor(styles, variables, tile, layer, feature) {
    this.tile = tile;
    this.styles = styles;
    this.variables = variables;
    this.layer = layer;
    this.feature = feature;
  }

  op() {
    return op.call(this, ...arguments);
  }

  value(style, key, cb) {
    let ret;
    if (style[key]) {
      /* if(key == 'line-dasharray') {
         console.log("STYLEs: ",this.styles);
         console.log("DASH-ARRAY: ",style[key]); 
         }*/

      const v = op.call(this, style[key]);
      const m = typeof v == "string" && v.match(/hsl\((.*?)\)/);
      if (m) {
        const [r, g, b] = HSLToRGB(m[1]);
        const hex = RGBToHex(r, g, b);
        // console.log(`VAL: (was HSL): ${hex}`);
        ret = "#" + hex;
      } else if (typeof v == "number") {
        ret = v;
      } else {
        ret = v;
      }
    }

    if (cb && typeof ret != "undefined") {
      try {
        cb(ret);
      } catch (x) {
        report(x);
      }
    }

    return ret;
  }

  // TODO: Rename, this is the generalized draw routine now :-\
  paint(doc, typeName, sub) {
    const draw = drawGeom(typeName, sub);

    try {
      const typeName = VectorTileFeature.types[this.feature.type];

      this.styles.forEach((style) => {
        if (style.type == "fill") {
          // typeName == "Polygon") {
          //          console.log("TYpe FILL");
          doc.fillOpacity(1);
          doc.fillColor("#000000");
          doc.strokeOpacity(0);
          doc.strokeColor("#000000");
        }

        if (style.type == "line") {
          // typeName == 'LineString') {
          //        console.log("TYpe LINE");
          doc.lineWidth(15);
          doc.fillOpacity(0);
          doc.fillColor("#000000");
          doc.strokeOpacity(1);
          doc.strokeColor("#000000");
        }

        let lineGap = 0;
        let lineWidth = 15;

        this.value(style.paint, "fill-opacity", (v) => {
          doc.fillOpacity(v);
        });

        this.value(style.paint, "fill-color", (v) => {
          if (typeof v == "object") {
            doc.fillColor("#" + RGBToHex(...v));
          } else {
            doc.fillColor(v);
          }
        });
        this.value(style.paint, "line-opacity", (v) => {
          //          console.log("LIne opacity ",v,typeof v);
          doc.strokeOpacity(Number(v));
        });

        this.value(style.paint, "line-color", (v) => {
          if (typeof v == "object") {
            //            console.log(`Line color (from RGB): #${RGBToHex(...v)}`);
            doc.strokeColor("#" + RGBToHex(...v));
          } else {
            //          console.log("Line color: ",v);
            doc.strokeColor(v);
          }
        });

        this.value(style.paint, "line-width", (v) => {
          lineWidth = v * 15;
          doc.lineWidth(v * 15);
        });

        this.value(style.paint, "line-dasharray", (v) => {
          doc.dash(v[0] * 20, { length: v.reduce((a, b) => a * 20 + b * 20) });
        });

        this.value(style.paint, "line-gap-width", (v) => {
          lineGap = v * 15;
        });

        // console.log("TYPE: ",typeName);
        if (style.type == "line") {
          // typeName == 'LineString') {
          //          doc.lineWidth(20);
          //doc.strokeColor('#c1c2c5')
          // doc.strokeOpacity(1);
          if (lineGap) {
            doc.save();
            {
              doc.lineWidth(lineGap + lineWidth);
              draw(doc);
              doc.stroke();
            }
            doc.restore();

            doc.save();
            {
              doc.lineWidth(lineGap);
              doc.strokeOpacity(1);
              draw(doc);
              doc.stroke("#ffffff");
            }
            doc.restore();
          } else {
            draw(doc);
            doc.stroke();
          }
        } else if (style.type == "fill") {
          // typeName == 'Polygon') {
          draw(doc);
          doc.fillAndStroke();
        } else if (style.type == "symbol") {
          let text = this.value(style.layout, "text-field");
          const iconImage = this.value(style.layout, "icon-image");

          if (iconImage) {
            const svg = fs.readFileSync(
              path.resolve(StylePath, `icons/${iconImage}.svg`),
              "utf-8"
            );
            const i = Math.floor(sub.length / 2);
            SVGtoPDF(doc, svg, sub[i].x, sub[i].y, {
              width: 1000,
              height: 200,
            });
            doc.fontSize(150);
            doc.text("foo", sub[i].x, sub[i].y, { lineBreak: false });
            return;
          }

          if (text) {
            text = text.replace(
              /{(.*?)}/g,
              (m, a) => this.feature.properties[a]
            );
            text = text.replace(`Nat'l Forest Development Rd`, "NFD");
            const fontSize = this.value(style.layout, "text-size") || 12;
            const textColor = this.value(style.paint, "text-color", (v) =>
              doc.fillColor(v)
            );
            const rotation = this.value(
              style.layout,
              "text-rotation-alignment"
            );
            const textMaxAngle = this.value(style.layout, "text-max-angle");

            const extent = this.tile.layers[this.layer].extent;

            let s = simplify(
              sub.filter((p) => {
                if (0 < p.x && p.x < extent && 0 < p.y && p.y < extent) {
                  return true;
                } else {
                  return false;
                }
              }),
              100
            );

            if (s.length >= 2 && s[0].x > s[s.length - 1].x) {
              s = s.reverse();
            }

            // remove points outside of box:

            const curve = fitCurve(
              s.map(({ x, y }) => [x, y]),
              200
            );

            let path = [];

            for (let C of curve) {
              path.push(`M${C[0][0]},${C[0][1]}`);
              path.push(
                `C ${C[1][0]} ${C[1][1]}, ${C[2][0]} ${C[2][1]}, ${C[3][0]} ${C[3][1]}`
              );
            }

            if (path.length == 0) {
              path.push(`M${sub[0].x},${sub[0].y}`);
              path.push(`L${sub[1].x},${sub[1].y}`);
            }

            let shadow = "initial";
            const shadowColor = this.value(style.paint, "text-halo-color");
            const shadowSize = this.value(style.paint, "text-halo-width");
            if (shadowColor && shadowSize) {
              shadow = `0px 0px ${shadowSize * 15}px ${shadowColor}`;
            }

            if (!rotation || rotation == "map") {
              const svg = `
<svg viewBox="0 0 4096 4096">
<style>
text {
fill: ${textColor};
font-size: ${fontSize * 15};
text-shadow: ${shadow};
}
</style>
<path fill="transparent" stroke="transparent" stroke-width="15" d="${path.join(
                " "
              )}" />
<defs><path id="text-path" d="${path.join(" ")}" /></defs>
<text  ><textPath startOffset="100px" xlink:href="#text-path">${text}</textPath></text>
</svg>
`;

              SVGtoPDF(doc, svg, 0, 0, { width: 4096, height: 4096 });
            } else {
              // if(rotation == 'map' || rotation == 'viewport') {
              if (fontSize) {
                doc.fontSize(fontSize * 15);
              } else {
                doc.fontSize(150);
              }

              doc.fillColor("black");
              doc.fillOpacity(1);
              // doc.fontSize(150);
              //              doc.strokeColor(textColor);

              doc.text(text, sub[0].x, sub[0].y, { lineBreak: false });
            }

            /* debugger;

            this.value(style.paint,'text-halo-color',v => doc.strokeColor(v));
            this.value(style.paint,'text-halo-width',v => doc.lineWidth(v*15));
            doc.fillOpacity(1);
            
            doc.text(text,sub[0].x,sub[0].y,{lineBreak: false}); */
          }
        }
      });
    } catch (x) {
      console.error("Error applying paint styles: ", x);
      throw x;
    }
  }
}

module.exports = function (doc, tile, layer, feature) {
  const ctx = { variables: {}, doc, tile, layer, feature };

  ctx.op = op.bind(ctx);
  ctx.filter = filter.bind(ctx);

  const S = STYLE.layers.filter((style) => {
    // if(style.id != 'hillshade') return false;
    // if(style.id == 'road-pedestrian-polygon-pattern') debugger;
    //    if(style.id == 'contour-label') debugger;
    if (style["source-layer"] != layer) return false;
    return ctx.filter(style.filter);
  });

  return new StyleSet(S, ctx.variables, tile, layer, feature);
};
