// import mjml2html from "mjml";
import * as html2text from "html-to-text";
import getTransport from "../server/mail-transport";
import { template as lodashTemplate } from "lodash";

import * as nodemailer from "nodemailer";
import chalk from "chalk";
import mjml2html from "mjml";
import path from "path";

const util = require("util");
const fs = require("fs");
const pfs = {
  readFile: util.promisify(fs.readFile),
};

const fromEmail = "support@orienteer.co";
console.log("Loading up");

const isDev = process.env.NODE_ENV !== "production";

const projectName = "Orienteer.co";
const legalText =
  "This email was sent to you at (hopefully) your request.  Please contact support@orienteer.co if you think this is in error.";

const task = async (inPayload) => {
  const payload = inPayload;
  const transport = await getTransport();
  const { options: inOptions, template, variables } = payload;
  const options = {
    from: fromEmail,
    ...inOptions,
  };

  try {
    if (template) {
      const templateString = await pfs.readFile(
        path.resolve(__dirname, `./email-templates/${template}`),
        "utf-8"
      );
      const templateFn = lodashTemplate(templateString, {
        escape: /\[\[([\s\S]+?)\]\]/g,
      });

      const mjml = templateFn({
        projectName,
        legalText,
        ...variables,
      });

      const { html, errors } = mjml2html(mjml);
      if (errors && errors.length) {
        console.error(errors);
      }

      const html2textableHtml = html.replace(/(<\/?)div/g, "$1p");
      const text = html2text
        .fromString(html2textableHtml, {
          wordwrap: 120,
        })
        .replace(/\n\s+\n/g, "\n\n");
      Object.assign(options, { html, text });
    }

    const info = await transport.sendMail(options);
  } catch (x) {
    console.error("ERROR: " + x.stack);
    // throw x;
  }
};

export default task;
