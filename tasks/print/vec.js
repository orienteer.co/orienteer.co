export function dot(a, b) {
  return a.map((v, i) => v * b[i]).reduce((a, b) => a + b);
}

export function len(a) {
  return Math.sqrt(dot(a, a));
}

export function add(a, b) {
  return a.map((v, i) => v + b[i]);
}

export function negative(a) {
  return a.map((v) => -v);
}

export function mul(a, b) {
  return a.map((v, i) => v * (b[i] || b));
}

export function divide(a, b) {
  return a.map((v, i) => v / (b[i] || b));
}

export function subtract(a, b) {
  return add(a, negative(b));
}
