const VM = require("./vm");
const { HSLToRGB, RGBToHex } = require("../../lib/colors");
const simplified = require("simplify-js");
const fitCurve = require("fit-curve");
const turf = require("@turf/turf");
const { get } = require("../../lib/utils");

const SVGtoPDF = require("svg-to-pdfkit");

const { report } = require("../../plugins/errors");

const DEFAULT_STYLE = {
  line: {
    stroke: "#000000",
    "stroke-opacity": 1,
    "stroke-width": 1,
    fill: "#000000",
    "fill-opacity": 0,
  },

  fill: {
    stroke: "#000000",
    "stroke-opacity": 0,
    fill: "#000000",
    "fill-opacity": 1,
  },

  symbol: {
    "fill-opacity": 0,
  },

  text: {
    "font-size": 16,
  },

  circle: {
    "circle-opacity": 1,
    "circle-color": "#000000",
    "circle-radius": 5,
    "circle-stroke-width": 0,
    "circle-stroke-color": "#000000",
    "circle-stroke-opacity": 1,
  },
};

const ANCHOR_STYLE = {
  // text align: start middle end
  // alignment-baseline: hanging middle baseline

  center: { "text-align": "middle", "alignment-baseline": "middle" },
  left: { "text-align": "end", "alignment-baseline": "middle" },
  right: { "text-align": "start", "alignment-baseline": "middle" },
  top: { "text-align": "middle", "alignment-baseline": "baseline" },
  bottom: { "text-align": "middle", "alignment-baseline": "hanging" },
  "top-left": { "text-align": "end", "alignment-baseline": "baseline" },
  "top-right": { "text-align": "start", "alignment-baseline": "baseline" },
  "bottom-left": { "text-align": "end", "alignment-baseline": "hanging" },
  "bottom-right": { "text-align": "start", "alignment-baseline": "hanging" },
};

function flatten(array) {
  return array.reduce((a, b) => a.concat(b), []);
}

function getPoints(feature) {
  let coords = turf.getCoords(feature);
  if (!Array.isArray(coords[0])) return [coords];

  while (Array.isArray(coords[0][0])) {
    coords = flatten(coords);
  }

  return coords;
}

function hashToCSS(hash) {
  const parts = [];
  for (let k in hash) {
    let v = hash[k];

    if (k == "stroke-width") {
      // maybe adjust this in the future?
      v = v;
    }

    parts.push(`${k}: ${v};`);
  }
  return parts.join("\n");
}

function simplify(array) {
  //  console.log("unsimplified ",array.length);
  const S = simplified(
    array.map((o) => ({ x: o[0], y: o[1] })),
    0.25
  );
  //  console.log("simplified ",S.length);
  //  debugger;

  const R = S.map((o) => [o.x.toFixed(2), o.y.toFixed(2)]);
  return R;
}

function svgPath(array, index) {
  if (typeof array[0] == "number" || typeof array[0] == "string") {
    return `${index == 0 ? "M" : "L"}${array.join(",")}`;
  }

  if (typeof array[0][0] == "number" || typeof array[0] == "string") {
    return simplify(array).map(svgPath).join(" ");
  }

  return array.map(svgPath).join(" ");
}

function featureToSVGPath(feature) {
  const type = feature.type;

  if (type == "FeatureCollection") {
    const r = feature.features.map((f) => featureToSVGPath(f)).join(" ");
    return r;
  } else if (type == "Feature") {
    return featureToSVGPath(feature.geometry);
  } else {
    const p = svgPath(feature.coordinates);

    if (p.includes("undefined")) {
      debugger;
    }

    return p;
  }
}

function pathForText(feature) {
  let unclipped, clipped, curve;

  if (feature.type == "FeatureCollection") {
    throw new Error("Cannot get path for Featurecollection");
  }

  unclipped = turf.getCoords(feature);

  if (typeof unclipped[0][0] == "object") {
    return unclipped
      .map((sub) =>
        pathForText({
          type: "Feature",
          clip: feature.clip,
          geometry: { type: "LineString", coordinates: sub },
        })
      )
      .join(" ");
  }

  // Left-to-right?
  if (unclipped[0][0] > unclipped[unclipped.length - 1][0])
    unclipped = unclipped.reverse();

  if (
    feature.clip &&
    feature.clip.coordinates &&
    feature.clip.coordinates.length
  ) {
    clipped = unclipped.filter((c) =>
      turf.booleanPointInPolygon(c, feature.clip)
    );
  } else {
    clipped = unclipped;
  }

  // const simplified = simplify(clipped,10);
  curve = fitCurve(clipped, 150);

  let path = [];

  for (let C of curve) {
    path.push(`M${C[0][0]},${C[0][1]}`);
    path.push(
      `C ${C[1][0]} ${C[1][1]}, ${C[2][0]} ${C[2][1]}, ${C[3][0]} ${C[3][1]}`
    );
  }

  if (path.length == 0 && clipped.length > 1) {
    path.push(`M${clipped[0][0]},${clipped[0][1]}`);
    path.push(`L${clipped[1][0]},${clipped[1][1]}`);
  }

  return path.join(" ");
}

function unpack(nestedArray) {
  let a = nestedArray;

  if (typeof a[0] == "number") return [a];

  while (a && Array.isArray(a[0][0])) {
    a = a[0];
  }

  return a;
}

class Style {
  constructor(renderer, style) {
    this.style = style;
    this.renderer = renderer;
  }

  value(key, cb) {
    const style =
      this.style.paint && key in this.style.paint
        ? this.style.paint
        : this.style.layout;

    let ret;
    if (style && style[key]) {
      const v = this.ctx.op.call(this, style[key]);
      const m = typeof v == "string" && v.match(/hsl\((.*?)\)/);
      if (m) {
        const [r, g, b] = HSLToRGB(m[1]);
        const hex = RGBToHex(r, g, b);
        // console.log(`VAL: (was HSL): ${hex}`);
        ret = "#" + hex;
      } else if (typeof v == "number") {
        ret = v;
      } else {
        ret = v;
      }
    }

    if (cb && typeof ret != "undefined") {
      try {
        cb(ret);
      } catch (x) {
        report(x);
      }
    }

    return ret;
  }

  render(feature) {
    const ctx = (this.ctx = Object.create(this));

    ctx.variables = {};
    ctx.zoom = 14;
    ctx.feature = feature;

    ctx.op = VM.op.bind(ctx);

    if (this.style.filter && !ctx.op(this.style.filter)) {
      // abandon render if filter says not to render:
      return;
    }

    let style = { ...DEFAULT_STYLE[this.style.type] };

    let textStyle = { ...DEFAULT_STYLE["text"] };

    this.value("fill-opacity", (v) => (style["fill-opacity"] = v));
    this.value("fill-color", (v) => (style["fill"] = v));
    this.value("line-opacity", (v) => (style["line-opacity"] = v));
    this.value("line-color", (v) => (style["stroke"] = v));

    this.value("line-width", (v) => (style["stroke-width"] = v));
    this.value("line-dasharray", (v) => (style["stroke-dasharray"] = v));

    const gapWidth = this.value("line-gap-width") || 0;

    if (gapWidth > 0) {
      style["stroke-width"] += gapWidth;
    }

    // WIP: Convert the feature into a SVG path, add the styling,
    // create an SVG thing, and print it onto the document.

    let styles = [];

    const body = [];
    const defs = [];

    if (
      feature.clip &&
      feature.clip.coordinates &&
      feature.clip.coordinates.length
    ) {
      const path = svgPath(feature.clip.coordinates);
      // body.push(`<path d="${path}" fill="rgba(0,0,0,0.05)" stroke="red" />`);

      defs.push(
        `<clipPath id="clipper">
        <path d="${path}" />
        </clipPath>`
      );
    }

    body.push(
      `<path id="main" d="${featureToSVGPath(
        feature
      )}" clip-path="url(#clipper)" />`
    );

    if (this.style.type == "line" && gapWidth) {
      body.push(
        `<path id="gap" d="${featureToSVGPath(
          feature
        )}" clip-path="url(#clipper)" />`
      );
    }

    if (this.style.type == "symbol") {
      let text = this.value("text-field");
      const anchor = this.value("text-anchor") || "center";
      const placement = this.value("symbol-placement") || "point";

      textStyle = {
        ...textStyle,
        ...ANCHOR_STYLE[anchor],
      };

      if (text) {
        let textPath = null;
        const f =
          feature.type == "FeatureCollection" ? feature.features[0] : feature;

        text = text.replace(
          /{\s*(.*?)\s*}/g,
          (m, a) => this.ctx.feature.properties[a]
        );

        this.value("text-color", (v) => (textStyle["fill"] = v));
        const s = this.value("text-size", (v) => (textStyle["font-size"] = v));

        if (placement == "point") {
          let point = unpack(f.coordinates || f.geometry.coordinates)[0];

          const box = this.renderer.doc.page.checkTextPath(
            [
              [point[0], point[1]],
              [point[0] + s * text.length, point[1]],
            ],
            { text, size: s }
          );
          if (!box) {
            return;
          }

          body.push(`<text x="${point[0]}" y="${point[1]}">${text}</text>`);
        } else if (placement == "line") {
          const p = pathForText(f);

          if (p.match(/^\s*$/)) {
            // abandon!
            delete this.ctx;
            return;
          }

          const box = this.renderer.doc.page.checkTextPath(turf.getCoords(f), {
            text,
            size: s,
          });

          if (!box) return;

          body.push(
            `<path fill="transparent" stroke="transparent" id="text-path" d="${p}" />`
          );

          body.push(
            `<text><textPath xlink:href="#text-path">${text}</textPath></text>`
          );
          /*
          const S = new Multiline(box.toSegments()).svg({
            stroke: "salmon",
            fill: "transparent"
          });
          body.push(S); */
        }
      }
    }

    if (["fill", "line", "symbol", "text"].includes(this.style.type)) {
      styles = styles.concat([
        `text {`,
        hashToCSS(textStyle),
        `}`,
        `#main {`,
        hashToCSS(style),
        `}`,
        `#gap {`,
        hashToCSS({
          "fill-opacity": "0",
          stroke: gapWidth ? "white" : "transparent",
          "stroke-width": gapWidth,
        }),
        `}`,
      ]);

      const svg = [
        `<svg viewBox="0 0 ${this.renderer.doc.page.width} ${this.renderer.doc.page.height}">`,
        `<style>`,
        ...styles,
        `</style>`,
        `<defs>`,
        ...defs,
        `</defs>`,
        ...body,
        `</svg>`,
      ];

      // console.log("SVG: ", svg.join("\n"));

      try {
        SVGtoPDF(this.renderer.doc, svg.join("\n"), 0, 0);
      } catch (x) {
        console.error("Errored SVG: ", svg.join("\n"));
        throw x;
      }
    } else if (this.style.type == "circle") {
      this.value("circle-color", (v) => (style["circle-color"] = v));
      this.value("circle-radius", (v) => (style["circle-radius"] = v));
      this.value("circle-opacity", (v) => (style["circle-opacity"] = v));
      this.value(
        "circle-stroke-color",
        (v) => (style["circle-stroke-color"] = v)
      );
      this.value(
        "circle-stroke-opacity",
        (v) => (style["circle-stroke-opacity"] = v)
      );
      this.value(
        "circle-stroke-width",
        (v) => (style["circle-stroke-width"] = v)
      );

      const coords = getPoints(ctx.feature);

      this.renderer.doc.save();

      this.renderer.doc;
      for (var i = 0; i < coords.length; i++) {
        const c = coords[i];
        if (
          0 < c[0] &&
          c[0] <= this.renderer.doc.page.width &&
          0 < c[1] &&
          c[1] <= this.renderer.doc.page.height
        ) {
          this.renderer.doc
            .circle(c[0], c[1], style["circle-radius"])
            .lineWidth(style["circle-stroke-width"])
            .fillOpacity(style["circle-opacity"])
            .strokeOpacity(style["circle-stroke-opacity"])
            .fillAndStroke(style["circle-color"], style["circle-stroke-color"]);
        }
      }

      this.renderer.doc.restore();
    } else {
      console.log("unsupported type: ", this.style.type);
    }

    delete this.ctx;
  }
}

module.exports = Style;
