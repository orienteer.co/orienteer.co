const Style = require("./style");

class Renderer {
  constructor({ doc, projection, style }) {
    this.styles = {};

    for (let layer of style) {
      if (!this.styles[layer["source-layer"]]) {
        this.styles[layer["source-layer"]] = [];
      }

      this.styles[layer["source-layer"]].push(new Style(this, layer));
    }

    this.projection = projection;
    this.doc = doc;
  }

  bounds() {
    return this.projection.bounds();
  }

  projectToPage(item) {
    if (!item) return item;
    if (Array.isArray(item)) {
      if (typeof item[0] == "number") {
        if (item[1] > 84 || item[1] < -80) {
          debugger;
        }

        let r = this.projection.llToPage(item);

        return r;
      }
      return item.map((i) => this.projectToPage(i));
    }
    if (typeof item == "object") {
      let r = {};
      for (var k in item) {
        r[k] = this.projectToPage(item[k]);
      }
      return r;
    }
    return item;
  }

  render(layer, item) {
    const matchingStyles = this.styles[layer] || [];

    for (const style of matchingStyles) {
      style.render(this.projectToPage(item));
    }
  }
}

module.exports = Renderer;
