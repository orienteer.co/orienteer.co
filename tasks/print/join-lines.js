import {
  getCoords,
  lineSlice,
  point,
  nearestPointOnLine,
  lineString,
  distance,
} from "@turf/turf";

function superReduce(inArray, callback) {
  const array = [...inArray];

  for (var i = array.length - 1; i >= 0; i--) {
    for (var j = array.length - 1; j >= 0; --j) {
      if (i == j) continue;

      const a = array[i];
      const b = array[j];

      const c = callback(a, b);

      if (c) {
        // replace a:
        array[i] = a;
        // drop b:
        array.slice(j, 1);
      }
    }
  }

  return array;
}

const EPSILON = 0.01;

module.exports = function (lines) {
  return superReduce(lines, function (a, b) {
    const minDistance = Math.min(
      distance(a[0], b[0]),
      distance(a[0], b[b.length - 1]),
      distance(a[a.length - 1], b[0]),
      distance(a[a.length - 1], b[b.length - 1])
    );

    if (minDistance > 0.1) return;

    // We only care about lines that overlap at their ends.
    // The lines can be either "touching" or a "continuation" - i.e.they can either be in reversing directions or the same direction.

    const aStartClosest = nearestPointOnLine(lineString(b), a[0]);
    const aEndClosest = nearestPointOnLine(lineString(b), a[a.length - 1]);

    let splitPoint = null;
    let aFirst;

    // 1m tolerance
    if (
      aStartClosest.properties.dist < EPSILON &&
      aEndClosest.properties.dist < EPSILON
    ) {
      // console.log("BOTH CLOSEST");
      // this means that <a> is a subline of <b> (both ends of it are close to or on b), just return b and ditch a.
      return b;
    } else if (aStartClosest.properties.dist < EPSILON) {
      aFirst = false;
      splitPoint = aStartClosest;
    } else if (aEndClosest.properties.dist < EPSILON) {
      aFirst = true;
      splitPoint = aEndClosest;
    } else {
      return;
    }

    const bStartClosest = nearestPointOnLine(lineString(a), b[0]);
    const bEndClosest = nearestPointOnLine(lineString(a), b[b.length - 1]);

    let endPoint = null;

    if (
      bStartClosest.properties.dist < EPSILON &&
      bEndClosest.properties.dist < EPSILON
    ) {
      // b is a sub-line of a, just return a and ditch b:
      return a;
    } else if (bStartClosest.properties.dist < EPSILON) {
      // start is closest, end at the end:
      //console.log("bStartClosest!!");
      endPoint = b[b.length - 1];
    } else if (bEndClosest.properties.dist < EPSILON) {
      // end is closest, end at the start:
      //console.log("bEndClosest!!");
      endPoint = b[0];
    } else {
      // more of an intersection than an overlap.
      //console.log("NO ENDPOINT: ", bStartClosest, bEndClosest);
      return;
    }
    /*
    console.log("SPLIT POINT: ", splitPoint);
    console.log("ENDPOINT: ", endPoint);
    console.log("B: ", b);
*/
    const lineB = getCoords(lineSlice(splitPoint, endPoint, lineString(b)));

    if (aFirst) {
      const between = distance(a[a.length - 1], lineB[0]);
      const flipped = distance(a[a.length - 1], lineB[lineB.length - 1]);

      if (flipped < between) {
        lineB.reverse();
      }

      return [...a, ...lineB];
    } else {
      let between = distance(lineB[lineB.length - 1], a[0]);
      let flipped = distance(lineB[0], a[0]);

      if (flipped < between) {
        lineB.reverse();
      }

      return [...lineB, ...a];
    }
  });
};
