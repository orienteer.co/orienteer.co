function linearInterpolate(
  stop1Input,
  stop1Output,
  input,
  stop2Input,
  stop2Output
) {
  //  console.log("LININT: ",...arguments);
  if (!input || !stop2Input || stop1Input == stop2Input) return stop1Output;

  let unit = ((input - stop1Input) / (stop2Input - stop1Input)) % 1;
  if (unit < 0) ++unit;

  if (typeof stop1Output == "string") {
    if (stop1Output.match(/^hsl/)) {
      stop1Output = HSLToRGB(stop1Output);
    } else {
      console.log("Unkonwn interpolation for ", stop2Output);
    }
  }

  if (typeof stop2Output == "string") {
    if (stop2Output.match(/^hsl/)) {
      stop2Output = HSLToRGB(stop2Output);
    } else {
      console.log("Unkonwn interpolation for ", stop2Output);
    }
  }

  if (typeof stop1Output == "number") {
    const r = stop1Output + unit * (stop2Output - stop1Output);
    //    console.log("Number number ",stop1Output,unit,stop2Output,r);
    return r;
  }

  if (typeof stop1Output == "object" && stop1Output.length) {
    const r = [
      stop1Output[0] + unit * (stop2Output[0] - stop1Output[0]),
      stop1Output[1] + unit * (stop2Output[1] - stop1Output[1]),
      stop1Output[2] + unit * (stop2Output[2] - stop1Output[2]),
    ];
    //    console.log("returning interpolated: ",r);
    return r;
  }

  return stop1Output;
}

const OPERATIONS = {
  all: function (args) {
    for (var i = 0; i < args.length; i++) {
      if (!this.op(args[i])) return false;
    }
    return true;
  },
  any: function (args) {
    for (var i = 0; i < args.length; i++) {
      const v = this.op(args[i]);
      if (v) return true;
    }
    return false;
  },
  in: function (args) {
    if (args[1].includes(args[0])) return true;
    return false;
  },
  "!": function (args) {
    if (args.length != 1) {
      console.error("unexpected ! statement: ", args);
      return false;
    }

    return !this.op(args[0]);
  },
  "==": function (args) {
    if (args.length != 2) {
      console.error("unexpected == statement: ", args);
      return false;
    }

    const a = this.op(args[0]);
    const b = this.op(args[1]);

    return a == b;
  },
  "<=": function (args) {
    if (args.length != 2) {
      console.error("unexpected <= statement: ", args);
      return false;
    }

    return this.op(args[0]) <= this.op(args[1]);
  },
  "<": function (args) {
    if (args.length != 2) {
      console.error("unexpected < statement: ", args);
      return false;
    }

    return this.op(args[0]) < this.op(args[1]);
  },
  "%": function (args) {
    if (args.length != 2) {
      console.error("unexpected % statement: ", args);
      return false;
    }

    return this.op(args[0]) % this.op(args[1]);
  },
  concat: function (args) {
    return args.map((a) => this.op(a)).join("");
  },
  "to-string": function (args) {
    return String(this.op(args[0]));
  },
  has: function (args) {
    if (args.length == 1) {
      return args[0] in this.feature.properties;
    }
    console.log("Didn't understand GET statemnt: ", args);
    debugger;
  },
  literal: function (args) {
    return args[0];
  },
  /*  'geometry-type': function() {
    return this.feature.type;
  }, */
  ">=": function (args) {
    if (args.length != 2) {
      console.error("unexpected >= statement: ", args);
      return false;
    }
    return this.op(args[0]) >= this.op(args[1]);
  },
  "+": function (args) {
    return args.map((a) => this.op).reduce((a, b) => a + b);
  },
  ">": function (args) {
    if (args.length != 2) {
      console.error("unexpected > statement: ", args);
      return false;
    }

    return this.op(args[0]) > this.op(args[1]);
  },
  "!=": function (args) {
    if (args.length != 2) {
      console.error("unexpected != statement: ", args);
      return false;
    }
    return this.op(args[0]) != this.op(args[1]);
  },
  zoom: function (args) {
    return this.zoom;
  },
  get: function (args) {
    if (args.length == 1) {
      if (!this.feature.properties) debugger;
      const r = this.feature.properties[args[0]];
      // console.log("GET ",args[0],r,this.feature.properties);
      if (!r) return null; // throw new Error("Couldn't find ",args[0]);
      return r;
    }
    console.log("Didn't understand GET statemnt: ", args);
  },
  coalesce: function (args) {
    let r;
    for (var i = 0; i < args.length; i++) {
      r = this.op(args[i]);
      if (r != null) return r;
    }
    return r;
  },
  case: function (args) {
    for (var i = 0; i < args.length - 1; i += 2) {
      const c = this.op(args[i]);
      if (c == true) return this.op(args[i + 1]);
    }
    return this.op(args[args.length - 1]);
  },
  match: function (args) {
    const v = this.op(args[0]);
    //    console.log("MATCH ",args,v);

    // skip first and fallback (last)
    for (var i = 1; i < args.length - 1; i += 2) {
      const m = args[i];
      const o = this.op(args[i + 1]);

      //      console.log(`is ${v} in ${m}?`);

      if ((m.includes && m.includes(v)) || m == v) {
        //        console.log("YES!");
        return o;
      }
      //      console.log("NO :(");
    }

    // fallback:
    return this.op(args[args.length - 1]);
  },
  step: function (args) {
    const value = this.op(args[0]);

    for (let i = 1; i < args.length - 1; i += 2) {
      const stop = this.op(args[i + 1]);
      if (value <= stop) {
        return this.op(args[i]);
      }
    }
    return this.op(args[args.length - 1]);
  },
  interpolate: function (args) {
    // console.log("INTERPOLATE: ",args);
    const type = "linear" || args[0];
    const input = this.op(args[1]);

    // short circuit for now to the first stop output:
    // return this.op(args[3]);

    let stop1Input = this.op(args[2]);
    let stop1Output = this.op(args[3]);

    //  if(input <= this.op(args[2])) return this.op(args[3]);

    let stop2Input, stop2Output;
    let i = 2;
    for (; i < args.length; i += 2) {
      stop2Input = this.op(args[i]);
      stop2Output = this.op(args[i + 1]);

      if (input < this.op(args[i])) {
        break;
      }

      stop1Input = this.op(args[i]);
      stop1Output = this.op(args[i + 1]);
    }

    // TODO: Exponential interpolation
    const r = linearInterpolate(
      stop1Input,
      stop1Output,
      input,
      stop2Input,
      stop2Output
    );

    return r;
  },
  let: function (args) {
    for (let i = 0; i < args.length - 1; i += 2) {
      const name = args[i];
      const value = this.op(args[i + 1]);
      this.variables[name] = value;
    }
    return this.op(args[args.length - 1]);
  },
  var: function (args) {
    return this.variables[args[0]];
  },
};

function op(args) {
  if (args && typeof args == "object" && typeof args.length == "number") {
    if (typeof args[0] != "string") return args;

    const fn = OPERATIONS[args[0]];
    if (fn) return fn.call(this, args.slice(1));
    else {
      console.log("Unknown filter ", args[0]);
      return args;
    }
  }
  return args;
}

function filter(statement) {
  if (!statement) return true;

  if (statement && typeof statement == "object" && statement.length) {
    return this.op(statement);
  }

  console.log("unknown statement type " + typeof statement, statement);
}

module.exports = {
  OPERATIONS,
  filter,
  op,
};
