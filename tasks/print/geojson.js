const { VectorTile, VectorTileFeature } = require("@mapbox/vector-tile");
const turf = require("@turf/turf");
const martinez = require("martinez-polygon-clipping");
const joinLines = require("./join-lines");

const EPSILON = 0.000000001;

export function featureToGeoJSON(feature, x, y, z) {
  var size = feature.extent * Math.pow(2, z),
    x0 = feature.extent * x,
    y0 = feature.extent * y,
    coords = feature.loadGeometry(),
    type = VectorTileFeature.types[feature.type],
    i,
    j;

  function project(line) {
    // WIP: we need to annotate edges and corners
    for (var j = 0; j < line.length; j++) {
      let edges = 0;
      for (let k = 0; k < line[j].length; k++) {
        if (line[j][k] <= 0) ++edges;
        if (line[j][k] >= feature.extent) ++edges;
      }

      var p = line[j],
        y2 = 180 - ((p.y + y0) * 360) / size;
      line[j] = [
        ((p.x + x0) * 360) / size - 180,
        (360 / Math.PI) * Math.atan(Math.exp((y2 * Math.PI) / 180)) - 90,
      ];
    }

    return line;
  }

  switch (feature.type) {
    case 1:
      var points = [];
      for (i = 0; i < coords.length; i++) {
        points[i] = coords[i][0];
      }
      coords = points;
      project(coords);
      break;

    case 2:
      for (i = 0; i < coords.length; i++) {
        project(coords[i]);
      }
      break;

    case 3:
      coords = classifyRings(coords);
      for (i = 0; i < coords.length; i++) {
        for (j = 0; j < coords[i].length; j++) {
          project(coords[i][j]);
        }
      }
      break;
  }

  if (coords.length === 1) {
    coords = coords[0];
  } else {
    type = "Multi" + type;
  }

  const swne = [
    { x: 0, y: feature.extent },
    { x: feature.extent, y: 0 },
  ];
  project(swne);

  var result = {
    type: "Feature",
    geometry: {
      type: type,
      coordinates: coords,
    },
    properties: feature.properties,
    clip: turf.bboxPolygon(swne[0].concat(swne[1])).geometry,
  };

  // result.bbox = turf.bbox(result);

  if ("id" in feature) {
    result.id = feature.id;
  }

  return result;
}

// classifies an array of rings into polygons with outer rings and holes

function classifyRings(rings) {
  var len = rings.length;

  if (len <= 1) return [rings];

  var polygons = [],
    polygon,
    ccw;

  for (var i = 0; i < len; i++) {
    var area = signedArea(rings[i]);
    if (area === 0) continue;

    if (ccw === undefined) ccw = area < 0;

    if (ccw === area < 0) {
      if (polygon) polygons.push(polygon);
      polygon = [rings[i]];
    } else {
      polygon.push(rings[i]);
    }
  }
  if (polygon) polygons.push(polygon);

  return polygons;
}

function bbox(o) {
  const b = turf.bbox(o);
  if (b[0] == Infinity) return null;
  return b;
}

function signedArea(ring) {
  var sum = 0;
  for (var i = 0, len = ring.length, j = len - 1, p1, p2; i < len; j = i++) {
    p1 = ring[i];
    p2 = ring[j];
    sum += (p2.x - p1.x) * (p1.y + p2.y);
  }
  return sum;
}

function multiCoordinates(geometry) {
  if (geometry.type.includes("Multi")) return geometry.coordinates;
  return [geometry.coordinates];
}

function bboxOverlap(a, b) {
  const acx = (a[0] + a[2]) / 2;
  const acy = (a[1] + a[3]) / 2;
  const aw = Math.abs(a[2] - a[0]);
  const ah = Math.abs(a[3] - a[1]);

  const bcx = (b[0] + b[2]) / 2;
  const bcy = (b[1] + b[3]) / 2;
  const bw = Math.abs(b[2] - b[0]);
  const bh = Math.abs(b[3] - b[1]);

  // https://gamedev.stackexchange.com/a/587:
  return Math.abs(acx - bcx) * 2 < aw + bw && Math.abs(acy - bcy) * 2 < ah + bh;
}

export function union(set) {
  return set;

  const r = [];

  for (const i in set) {
    let a = set[i];

    // iterate backwards so we can remove items if we want to:
    for (let j = r.length - 1; j >= 0; --j) {
      const b = r[j];

      a.bbox = a.bbox || bbox(a);
      b.bbox = b.bbox || bbox(b);

      // if the bounding boxes do not overlap, skipparoo:
      if (a.bbox && b.bbox && !bboxOverlap(a.bbox, b.bbox)) {
        // skipping!
      } else if (a.geometry.type == "Point" && b.geometry.type == "Point") {
        const dx = Math.abs(
          a.geometry.coordinates[0] - b.geometry.coordinates[0]
        );
        const dy = Math.abs(
          a.geometry.coordinates[1] - b.geometry.coordinates[1]
        );

        if (
          dx < Math.abs(a.geometry.coordinates[0] * EPSILON) &&
          dy < Math.abs(a.geometry.coordinates[1] * EPSILON)
        ) {
          // drop original:
          r.splice(j, 1);
        }
      } else if (a.geometry.type == "Polygon" && b.geometry.type == "Polygon") {
        const unioned = martinez.union(
          a.geometry.coordinates,
          b.geometry.coordinates
        );

        if (unioned.length === 0) {
          // shouldn't happen
          console.error("Unioned length of zero - unexpected");
          debugger;
        } else if (unioned.length === 1) {
          // if(!bbox) { console.error("BBOX FAIL: no bbox match, but unioned match"); }
          // the two geometries are overlapping and should be joined.
          // remove the item from the the result set, and set the
          // coordinates of `a` to be the new union, which will cause it
          // to be added to the list.  Keep going to see if there are
          // any more overlaps:

          r.splice(j, 1);

          a = {
            properties: a.properties,
            geometry: {
              type: "Polygon",
              coordinates: unioned,
            },
            clip: {
              coordinates: martinez.union(
                a.clip.coordinates,
                b.clip.coordinates
              ),
            },
            type: "Feature",
          };

          // a.bbox = turf.bbox(a);
        } else {
          // not overlapping - bail bail bail
        }
      } else if (
        a.geometry.type.includes("LineString") &&
        b.geometry.type.includes("LineString")
      ) {
        // drop B:
        r.splice(j, 1);

        // consolidate into A:
        const coordinates = joinLines([
          ...multiCoordinates(a.geometry),
          ...multiCoordinates(b.geometry),
        ]);

        let clip = null;

        if (a.clip && a.clip.coordinates && b.clip && b.clip.coordinates) {
          clip = {
            coordinates: martinez.union(a.clip.coordinates, b.clip.coordinates),
          };
        }

        // if (a.properties.waterway == "stream") debugger;

        a = {
          properties: a.properties,
          geometry: {
            type: "MultiLineString",
            coordinates,
          },
          clip,
          type: "Feature",
        };
      }
    }

    r.push(a);
  }

  return r;
}
