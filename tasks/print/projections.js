const utm = require("utm");
const V = require("./vec");

const POINTS_TO_INCHES = 1 / 72;
const INCHES_TO_METERS = 0.0254;
const pdfPointsToMeters = (points) =>
  points * POINTS_TO_INCHES * INCHES_TO_METERS;
const metersToPDFPoints = (meters) =>
  meters / (POINTS_TO_INCHES * INCHES_TO_METERS);

function coerce(arg) {
  // two args?  x,y pair:

  if (arg.length == 2) {
    if (typeof arg[0] != "number" || typeof arg[1] != "number")
      throw new Error(
        "InvalidArgument - projection calls, two arg must be number"
      );

    return [arg[0], arg[1]];
  } else if (arg.length == 1) {
    if ("easting" in arg[0] && "northing" in arg[0]) return arg[0];

    const a = [
      Number(
        arg[0][0] || arg[0].x || arg[0].lng || arg[0].lon || arg[0].longitude
      ),
      Number(arg[0][1] || arg[0].y || arg[0].lat || arg[0].latitude),
    ];

    if (typeof a[0] != "number" || isNaN(a[0])) {
      throw new Error(
        "InvalidArgument - cannot coerce x coordinate into number"
      );
    }
    if (typeof a[1] != "number" || isNaN(a[1])) {
      throw new Error(
        "InvalidArgument - cannot coerce y coordinate into number"
      );
    }

    return a;
  } else {
    throw new Error(`invalid projection call - array of length ${arg.length}`);
  }
}

function wrap(fn) {
  const inner = function () {
    return fn.call(this, coerce(arguments));
  };

  return function () {
    // no args? curry it:
    if (arguments.length == 0) return inner.bind(this);

    return inner.apply(this, arguments);
  };
}

function project(origin, axis, point) {
  const a = V.subtract(axis, origin);
  const aLength = V.len(a);
  const ua = V.divide(a, aLength);

  const p = V.subtract(point, origin);

  // return the actual projection of point relative to unit vector:
  const unitProjection = V.dot(ua, p);

  // now return that divided by the lenght of the original vector,
  // giving us projection along actual vector:

  return unitProjection / aLength;
}

function unproject(origin, axis, amount) {
  const rAxis = V.sub(axis, origin);

  return V.add(origin, V.mul(rAxis, amount));
}

function UTMProjection({ center, scale, width, height, page }) {
  this.page = page;

  this.centerLL = coerce([center]);

  this.centerUTM = this.llToUTM(center);

  let mapWidthInMeters, mapHeightInMeters;

  if (scale) {
    mapWidthInMeters = pdfPointsToMeters(page.width) * scale;
    mapHeightInMeters = pdfPointsToMeters(page.height) * scale;
  } else if (width && height) {
    mapWidthInMeters = width;
    mapHeightInMeters = height;
  }

  const w = this.centerUTM.easting - mapWidthInMeters / 2;
  const e = this.centerUTM.easting + mapWidthInMeters / 2;
  const n = this.centerUTM.northing + mapHeightInMeters / 2;
  const s = this.centerUTM.northing - mapHeightInMeters / 2;

  this.corners = {
    nw: [w, n],
    ne: [e, n],
    se: [e, s],
    sw: [w, s],
  };
}

UTMProjection.prototype = {
  bounds() {
    const nw = this.utmToLL(this.corners.nw);
    const ne = this.utmToLL(this.corners.ne);
    const se = this.utmToLL(this.corners.se);
    const sw = this.utmToLL(this.corners.sw);

    return {
      w: Math.min(nw[0], sw[0]),
      n: Math.max(nw[1], ne[1]),
      e: Math.max(ne[0], se[0]),
      s: Math.min(se[1], sw[1]),
    };
  },

  utmToLL: wrap(function (u) {
    return coerce([
      utm.toLatLon(
        u[0],
        u[1],
        this.centerUTM.zoneNum,
        this.centerUTM.zoneLetter
      ),
    ]);
  }),

  llToUTM: wrap(function (ll) {
    return utm.fromLatLon(
      ll[1],
      ll[0],
      this.centerUTM && this.centerUTM.zoneNum
    );
  }),

  utmToPage: wrap(function (utm) {
    /* 
       if(utm.zoneNum != this.centerUTM.zoneNum || utm.zoneLetter != this.centerUTM.zoneLetter) {
       console.log(`ZONES ${utm.zoneNum}${utm.zoneLetter} ${this.centerUTM.zoneNum }${this.centerUTM.zoneLetter}`);
       throw new Error("zones do not match");
       } 
    */

    const x = project(this.corners.nw, this.corners.ne, [
      utm.easting,
      utm.northing,
    ]);
    const y = project(this.corners.nw, this.corners.sw, [
      utm.easting,
      utm.northing,
    ]);

    return [x * this.page.width, y * this.page.height];
  }),

  llToPage: wrap(function (ll) {
    const u = this.llToUTM(ll);
    const page = this.utmToPage(u);

    return page;
  }),
};

module.exports = { UTMProjection };
