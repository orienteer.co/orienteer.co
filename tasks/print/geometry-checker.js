// import Flatten from "./flatten-js/dist/main.esm";

// const { Point, Box, PlanarSet, Segment, Multiline } = Flatten;
import { Point, Box, PlanarSet, Segment } from "@flatten-js/core";

function unpack(nestedArray) {
  let a = nestedArray;
  while (Array.isArray(a[0][0])) {
    a = a[0];
  }

  return a;
}

function segmentsForPoints(data) {
  const points = unpack(data).map(([x, y]) => new Point(x, y));
  const set = [];

  for (var i = 0; i < points.length - 1; i++) {
    set.push(new Segment(points[i], points[i + 1]));
  }

  return set;
}

function grow(box, amount) {
  return new Box(
    box.xmin - amount,
    box.ymin - amount,
    box.xmax + amount,
    box.ymax + amount
  );
}

export default function createChecker() {
  const set = new PlanarSet();

  return function (pts, options = {}) {
    const { text, size } = options;
    const segments = segmentsForPoints(pts);

    const length = segments.map((s) => s.length).reduce((a, b) => a + b, 0);
    const textLengthApprox = size * text.length * 0.6;
    if (length < textLengthApprox) {
      return null;
    }

    // const box = segments.box;

    let d;
    if (set.size > 0) {
      let ds = segments.map((s) => s.distanceTo(set)[0]);
      d = Math.min(...ds);
      if (d < size) return null;
    }

    let box;

    for (let s of segments) {
      set.add(s);
      box = (box && box.merge(s.box)) || s.box;
    }

    return box;
  };
}
