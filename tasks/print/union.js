import { topology } from "topojson-server";
import { feature, meshArcs } from "topojson-client";
import { object } from "topojson-client/src/feature.js";
import { presimplify, simplify, quantile } from "topojson-simplify";
import { deepEqual } from "../../lib/utils";
import { mergeArcs, default as merge } from "./topojson-merge";

function filterInPlace(a, condition) {
  let i = 0,
    j = 0;

  while (i < a.length) {
    const val = a[i];
    if (condition(val, i, a)) a[j++] = val;
    i++;
  }

  a.length = j;
  return a;
}

/*
function deepEqual(a, b) {
  for (var k in Object.keys(a).concat(Object.keys(b))) {
    if (!(k in a) || !(k in b) || a[k] != b[k]) return false;
  }
  return true;
}
*/

function coordinatesToClippedPolygon(coordinates, properties, clippers) {
  let clip = topology({
    clip: {
      type: "FeatureCollection",
      features: clippers.map((g) => ({ type: "Feature", geometry: g })),
    },
  });

  // Apparently topology doesn't merge _identical_ polygons, so we need to eliminate polygons that are totally identical.

  filterInPlace(clip.objects.clip.geometries, unique);

  const c = merge(clip, clip.objects.clip.geometries);

  return {
    type: "Feature",
    geometry: { type: "Polygon", coordinates },
    clip: c,
    properties,
  };
}

function unique(object, index, array) {
  return (
    array.findIndex((o) => {
      return deepEqual(o, object);
    }) == index
  );
}

module.exports = function union(group) {
  let type = group[0].type;

  if (type == "Feature") {
    type = group[0].geometry.type;
  }

  if (type.includes("Point")) {
    return group;
  }

  let annotatedGroup = group.map((g, i) => ({
    ...g,
    properties: {
      index: i,
    },
  }));

  let topo = presimplify(
    topology(
      {
        content: { type: "FeatureCollection", features: annotatedGroup },
      },
      1e6
    )
  );

  let merged;

  const q = quantile(topo, 0.5);
  topo = simplify(topo, q);

  if (type.includes("Polygon")) {
    const intermediate = mergeArcs(topo, topo.objects.content.geometries);
    const merged = object(topo, intermediate);

    return merged.coordinates.map((c, i) =>
      coordinatesToClippedPolygon(
        c,
        group[0].properties,
        intermediate.indexes[i].map((i) => group[i].clip)
      )
    );
  } else if (type.includes("Line")) {
    merged = meshArcs(topo);
  } else {
    console.log("NOT anything: ", type, group);
    return group;
  }

  const out = object(topo, merged);

  out.properties = { ...group[0].properties };

  return out;
};
