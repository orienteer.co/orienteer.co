export function unpack(object, key = "_") {
  let o = object;
  while (o && key in o) o = o[key];
  return o;
}

export default function (query, variablesCallback) {
  return async function (...args) {
    const variables =
      typeof variablesCallback == "function"
        ? await variablesCallback.call(this, this.$nuxt.context)
        : variablesCallback;

    const { result, error } = await this.$gql(query, variables);

    if (error) throw error;

    for (let k in result) {
      this[k] = unpack(result[k]);
    }
  };
}
