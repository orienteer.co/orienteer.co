module.exports = function (concurrency, list, callback) {
  return new Promise((resolve, reject) => {
    let todo = list.map((o, i) => ({ index: i, work: o }));
    let error = null;

    const active = [];

    function finish(index) {
      const i = active.findIndex((o) => o.index == index);
      active.splice(i, 1);

      go();
    }

    function workOn({ index, work }) {
      active.push({ index, work });

      callback(work).then(
        (r) => finish(index),
        (e) => {
          error = e;
          reject(e);
          todo = [];
        }
      );
    }

    function go() {
      if (active.length == 0 && todo.length == 0) {
        resolve();
      }

      while (active.length < concurrency && todo.length > 0) {
        workOn(todo.shift());
      }
    }

    go();
  });
};
