export default [
  {
    name: "ESRI Topographic",
    type: "xyz",
    url:
      "//services.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}",
    attribution: "© ESRI ArcGIS",
  },
  {
    name: "OpenStreetMap",
    type: "xyz",
    url: "//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
    attribution: "© OpenStreetMap contributors",
    options: {
      subdomains: ["a", "b", "c"],
      continuousWorld: true,
    },
  },
  {
    name: "US Forest",
    type: "xyz",
    url: "//ctusfs.s3.amazonaws.com/2016a/{z}/{x}/{y}.png",
    attribution: "US Forest Service",
  },
  {
    name: "CalTopo MapBuilder",
    type: "xyz",
    url: "//caltopo.com/tile/mb_topo/{z}/{x}/{y}.png",
    attribution: "CalTopo",
  },
  {
    name: "CalTopo",
    type: "xyz",
    url: "//caltopo.s3.amazonaws.com/topo/{z}/{x}/{y}.png",
    layerOptions: {
      attribution: "CalTopo",
    },
  },
  /*  {
    name: 'Google Satellite',
    type: 'xyz',
    url: 'https://khms1.googleapis.com/kh?v=802&hl=en-US&x={x}&y={y}&z={z}',
    attributions: '© Google',
    options : {
      subdomains: ['1','2','3'],
      continuousWorld: true
    }
  } */
];
