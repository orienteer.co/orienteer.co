import DP from "dot-prop";

export const get = DP.get;
const set = DP.set;

export function uuid() {
  return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
    var r = (Math.random() * 16) | 0,
      v = c == "x" ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
}

export function getMap(elem) {
  let e = elem;
  while (e) {
    // return the actual map instance, not the Vue container:
    if (e.$refs.map) return e.$refs.map;
    e = e.$parent;
  }
  return null;
}

export const leftPad = (string, length, padding = " ") => {
  var r = string;
  while (r.length < length) {
    r = padding + r;
  }
  return r;
};

export const pause = (delay) => new Promise((res) => setTimeout(res, delay));

export function updateAccessor(priorities, valuePath) {
  return {
    get() {
      var stack = priorities.map((p) => this[p]).map((v) => get(v, valuePath));
      return stack.find((s) => typeof s != "undefined");
    },
    set(value) {
      var p0 = priorities[0];
      var u = { ...this[p0] };
      set(u, valuePath, value);
      this[p0] = u;

      if (typeof this.onChange == "function") this.onChange(valuePath);
    },
  };
}

export function deepEqual(a, b) {
  if (typeof a != typeof b) return false;
  if (a == null && b == null) return true;

  if (typeof a == "object") {
    for (let k of Object.keys(a).concat(Object.keys(b))) {
      if (!deepEqual(a[k], b[k])) return false;
    }
    return true;
  }

  return a == b;
}

export function graphError(e) {
  console.error(e);
  if (!("graphQLErrors" in e)) return null;
  let messages = e.graphQLErrors.map((err) => err.message);
  try {
    messages = messages.concat(
      e.networkError.result.errors.map((err) => err.message)
    );
  } catch (x) {}

  return messages.join(" | ");
}

export async function streamToBuffer(stream) {
  return new Promise((resolve, reject) => {
    var bufs = [];
    stream.on("data", function (d) {
      bufs.push(d);
    });
    stream.on("end", function () {
      var buf = Buffer.concat(bufs);
      resolve(buf);
    });
    stream.on("error", function (error) {
      reject(error);
    });
  });
}

export function graphql(mapping, callback) {
  if (typeof mapping == "function") {
    callback = mapping;
    mapping = null;
  }

  return async function (ctx) {
    try {
      var res = await callback(ctx);
    } catch (x) {
      var message = graphError(x);
      if (message) {
        ctx.error({ statusCode: 400, message: graphError(x) });
        return;
      } else throw x;
    }

    if (mapping) {
      var ret = {};
      for (var k in mapping) {
        var v = mapping[k];
        if (typeof v == "function") ret[k] = v.call(this, res);
        else if (typeof v == "string") ret[k] = get(res, v);
        else throw new Error(`Unsupported type ${typeof v} for mapping`);
      }

      return ret;
    } else {
      if ("data" in res) return res.data._ || res.data;
      else if ("_" in res) return res._;
      else return res;
    }
  };
}

// add a locale variable to the top-level query/mutation if it's
// present in the actual query/mutation:
export function addLocale(query) {
  if (query.match(/$locale/)) {
    return query.replace(
      /(query|mutation)(.*?)\((.*?)\)/,
      function (match, a, b, c) {
        return `${a}${b}(${c} $locale: String)`;
      }
    );
  }
  return query;
}

export function getter(path) {
  return function (object) {
    return get(object || this, path);
  };
}

function inheritProp(name) {
  return {
    inherited: true,
    get: function () {
      var p = this.$parent;
      while (p) {
        // if it has something, and it's not a computed property:
        if (
          name in p &&
          !(
            name in p.$options.computed &&
            p.$options.computed[name].inherited == true
          )
        ) {
          return p[name];
        }

        p = p.$parent;
      }
      console.log(`Returning NULL for ${name}`);
      return null;
    },
    set: function (value) {
      var p = this.$parent;

      while (p) {
        if (
          name in p &&
          !(
            name in p.$options.computed &&
            p.$options.computed[name].inherited == true
          )
        ) {
          return (p[name] = value);
        }

        p = p.$parent;
      }

      return null;
    },
  };
}

export function inherit(...args) {
  var r = {};
  args.forEach((a) => (r[a] = inheritProp(a)));
  return r;
}

// stolen from https://www.movable-type.co.uk/scripts/latlong.html,
// didn't know that greek letters were totes fine in JS!

const DEG_TO_RADIANS = Math.PI / 180;
export function geodistance(lat1, lon1, lat2, lon2) {
  lat1 = Number(lat1);
  lat2 = Number(lat2);
  lon1 = Number(lon1);
  lon2 = Number(lon2);

  var R = 6371e3; // metres

  var φ1 = lat1 * DEG_TO_RADIANS;
  var φ2 = lat2 * DEG_TO_RADIANS;
  var Δφ = (lat2 - lat1) * DEG_TO_RADIANS;
  var Δλ = ((lon2 - lon1) % 360) * DEG_TO_RADIANS;

  var a =
    Math.sin(Δφ / 2) * Math.sin(Δφ / 2) +
    Math.cos(φ1) * Math.cos(φ2) * Math.sin(Δλ / 2) * Math.sin(Δλ / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

  return R * c;
}

export function ascending(a, b) {
  if (!a) return 0;
  if (!b) return 0;

  if (a < b) return -1;
  if (a > b) return 1;
  return 0;
}

export function byKey(key, fn) {
  return function (a, b) {
    a = a && a[key];
    b = b && b[key];
    return fn(a, b);
  };
}

export function boundsFor(set) {
  let w = null,
    s = null,
    n = null,
    e = null;

  const min = (a, b) => (a == null ? b : Math.min(a, b));
  const max = (a, b) => (a == null ? b : Math.max(a, b));

  for (const o of set) {
    w = min(w, o.lng || o.easting);
    e = max(e, o.lng || o.easting);
    s = min(s, o.lat || o.northing);
    n = max(n, o.lat || o.northing);
  }

  return [
    [w, s],
    [e, n],
  ];
}

export function filterInPlace(
  a,
  condition,
  set = (target, key, value) => (target[key] = value)
) {
  let i = 0,
    j = 0;

  while (i < a.length) {
    const val = a[i];
    if (condition(val, i, a)) set(a, j++, val);
    i++;
  }

  set(a, "length", j);
  return a;
}

export function hashWithout(hash, withoutKeys) {
  const r = {};
  for (var k in hash) {
    if (hash.hasOwnProperty(k) && !withoutKeys.includes(k)) {
      r[k] = hash[k];
    }
  }
  return r;
}

export function difference(a, b, comparator = (c, d) => c == d) {
  return a.filter((ao) => !b.find((bo) => comparator(ao, bo)));
}
