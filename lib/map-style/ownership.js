const ownershipColor = [
  "match",
  ["get", "admin_agency_code"],

  "USFS",
  "rgb(204,231,212)",

  "BLM",
  "rgb(254,225,153)",

  "NPS",
  "rgb(199,197,225)",

  ["DOD", "USCG", "NAVY", "USAF", "USMC", "ARMY"],
  "rgb(247,193,207)",

  ["FAA", "GSA", "BPA"],
  "rgb(227,201,178)",

  "PVT",
  "rgb(254,254,254)",

  "BIA",
  "rgb(250,188,139)",

  "ST",
  "rgb(254,254,254)",

  "LG",
  "rgb(230,230,230)",

  "red",
];

export default [
  {
    id: "ownership-fill",
    type: "fill",
    source: "land-ownership",
    "source-layer": "surfacemanagementagency",
    layout: {
      visibility: "none",
    },
    paint: {
      "fill-color": ownershipColor,
      "fill-opacity": 0.3,
    },
    minzoom: 10,
  },

  {
    id: "ownership-line",
    type: "line",
    source: "land-ownership",
    "source-layer": "surfacemanagementagency",
    paint: {
      "line-width": 2,
      "line-color": ownershipColor,
      "line-opacity": 0.8,
    },
    layout: {
      visibility: "none",
    },
    minzoom: 10,
  },
];
