const DEFAULTS = {
  maxzoom: 10,
};

const all = [
  {
    id: "land",
    type: "background",
    layout: {},
    paint: {
      "background-color": [
        "interpolate",
        ["linear"],
        ["zoom"],
        11,
        "hsl(35, 32%, 91%)",
        13,
        "hsl(35, 12%, 89%)",
      ],
    },
  },
  {
    id: "landcover",
    type: "fill",
    source: "composite",
    "source-layer": "landcover",
    layout: {},
    paint: {
      "fill-color": [
        "case",
        ["==", ["get", "class"], "snow"],
        "hsl(0, 0%, 100%)",
        ["match", ["get", "class"], ["wood"], true, false],
        "hsl(95, 74%, 42%)",
        ["match", ["get", "class"], ["scrub"], true, false],
        "hsl(43, 99%, 53%)",
        ["match", ["get", "class"], ["grass"], true, false],
        "hsl(44, 100%, 81%)",
        "hsl(75, 19%, 76%)",
      ],
      "fill-opacity": [
        "interpolate",
        ["exponential", 1.5],
        ["zoom"],
        2,
        0.3,
        17,
        0,
      ],
      "fill-antialias": false,
    },
  },
  {
    id: "national_park-tint-band",
    type: "line",
    source: "composite",
    "source-layer": "landuse_overlay",
    minzoom: 9,
    filter: ["==", ["get", "class"], "national_park"],
    layout: { "line-cap": "round" },
    paint: {
      "line-color": "hsl(100, 62%, 74%)",
      "line-width": [
        "interpolate",
        ["exponential", 1.4],
        ["zoom"],
        9,
        1,
        14,
        8,
      ],
      "line-offset": [
        "interpolate",
        ["exponential", 1.4],
        ["zoom"],
        9,
        0,
        14,
        -2.5,
      ],
      "line-opacity": ["interpolate", ["linear"], ["zoom"], 9, 0, 10, 0.75],
      "line-blur": 3,
    },
  },
  {
    id: "landuse",

    type: "fill",
    source: "composite",
    "source-layer": "landuse",
    minzoom: 5,
    filter: [
      "match",
      ["get", "class"],
      [
        "park",
        "airport",
        "cemetery",
        "glacier",
        "hospital",
        "pitch",
        "sand",
        "school",
        "agriculture",
        "wood",
        "grass",
        "scrub",
      ],
      true,
      false,
    ],
    layout: {},
    paint: {
      "fill-color": [
        "interpolate",
        ["linear"],
        ["zoom"],
        15,
        [
          "match",
          ["get", "class"],
          "park",
          [
            "match",
            ["get", "type"],
            ["garden", "playground", "zoo"],
            "hsl(100, 59%, 81%)",
            "hsl(100, 58%, 76%)",
          ],
          "airport",
          "hsl(230, 15%, 86%)",
          "cemetery",
          "hsl(75, 37%, 81%)",
          "glacier",
          "hsl(196, 72%, 93%)",
          "hospital",
          "hsl(340, 37%, 87%)",
          "pitch",
          "hsl(100, 57%, 72%)",
          "sand",
          "hsl(60, 46%, 87%)",
          "school",
          "hsl(50, 47%, 81%)",
          "agriculture",
          "hsl(75, 37%, 81%)",
          ["wood", "grass", "scrub"],
          "hsl(75, 41%, 74%)",
          "hsla(0, 0%, 0%, 0)",
        ],
        16,
        [
          "match",
          ["get", "class"],
          "park",
          [
            "match",
            ["get", "type"],
            ["garden", "playground", "zoo"],
            "hsl(100, 59%, 81%)",
            "hsl(100, 58%, 76%)",
          ],
          "airport",
          "hsl(230, 29%, 89%)",
          "cemetery",
          "hsl(75, 37%, 81%)",
          "glacier",
          "hsl(196, 72%, 93%)",
          "hospital",
          "hsl(340, 63%, 89%)",
          "pitch",
          "hsl(100, 57%, 72%)",
          "sand",
          "hsl(60, 46%, 87%)",
          "school",
          "hsl(50, 63%, 84%)",
          "agriculture",
          "hsl(75, 37%, 81%)",
          ["wood", "grass", "scrub"],
          "hsl(75, 41%, 74%)",
          "hsla(0, 0%, 0%, 0)",
        ],
      ],
      "fill-opacity": [
        "interpolate",
        ["linear"],
        ["zoom"],
        5,
        0,
        6,
        [
          "match",
          ["get", "class"],
          ["agriculture", "wood", "grass", "scrub"],
          0,
          "glacier",
          0.5,
          1,
        ],
        15,
        [
          "match",
          ["get", "class"],
          "agriculture",
          0.75,
          ["wood", "glacier"],
          0.5,
          "grass",
          0.4,
          "scrub",
          0.2,
          1,
        ],
      ],
    },
  },
  {
    id: "pitch-outline",
    type: "line",
    source: "composite",
    "source-layer": "landuse",
    filter: ["==", ["get", "class"], "pitch"],
    layout: {},
    paint: { "line-color": "hsl(75, 57%, 84%)" },
  },
  {
    id: "waterway-shadow",
    type: "line",
    source: "composite",
    "source-layer": "waterway",
    minzoom: 8,

    layout: {
      "line-cap": ["step", ["zoom"], "butt", 11, "round"],
      "line-join": "round",
    },
    paint: {
      "line-color": "hsl(215, 84%, 69%)",
      "line-width": [
        "interpolate",
        ["exponential", 1.3],
        ["zoom"],
        9,
        ["match", ["get", "class"], ["canal", "river"], 0.1, 0],
        20,
        ["match", ["get", "class"], ["canal", "river"], 8, 3],
      ],
      "line-translate": [
        "interpolate",
        ["exponential", 1.2],
        ["zoom"],
        7,
        ["literal", [0, 0]],
        16,
        ["literal", [-1, -1]],
      ],
      "line-translate-anchor": "viewport",
      "line-opacity": ["interpolate", ["linear"], ["zoom"], 8, 0, 8.5, 1],
    },
  },
  {
    id: "water-shadow",
    type: "fill",
    source: "composite",
    "source-layer": "water",
    layout: {},
    paint: {
      "fill-color": "hsl(215, 84%, 69%)",
      "fill-translate": [
        "interpolate",
        ["exponential", 1.2],
        ["zoom"],
        7,
        ["literal", [0, 0]],
        16,
        ["literal", [-1, -1]],
      ],
      "fill-translate-anchor": "viewport",
    },
  },
  {
    id: "waterway",
    type: "line",
    source: "composite",
    "source-layer": "waterway",
    minzoom: 8,
    layout: {
      "line-cap": ["step", ["zoom"], "butt", 11, "round"],
      "line-join": "round",
    },
    paint: {
      "line-color": "hsl(205, 87%, 76%)",
      "line-width": [
        "interpolate",
        ["exponential", 1.3],
        ["zoom"],
        9,
        ["match", ["get", "class"], ["canal", "river"], 0.1, 0],
        20,
        ["match", ["get", "class"], ["canal", "river"], 8, 3],
      ],
      "line-opacity": ["interpolate", ["linear"], ["zoom"], 8, 0, 8.5, 1],
    },
  },
  {
    id: "water",
    type: "fill",
    source: "composite",
    "source-layer": "water",
    layout: {},
    paint: { "fill-color": "hsl(196, 80%, 70%)" },
  },
  {
    id: "wetland",
    type: "fill",
    source: "composite",
    "source-layer": "landuse_overlay",
    minzoom: 5,
    filter: [
      "match",
      ["get", "class"],
      ["wetland", "wetland_noveg"],
      true,
      false,
    ],
    paint: {
      "fill-color": "hsl(185, 43%, 74%)",
      "fill-opacity": [
        "interpolate",
        ["linear"],
        ["zoom"],
        10,
        0.25,
        10.5,
        0.15,
      ],
    },
  },
  {
    id: "wetland-pattern",
    type: "fill",
    source: "composite",
    "source-layer": "landuse_overlay",
    minzoom: 5,
    filter: [
      "match",
      ["get", "class"],
      ["wetland", "wetland_noveg"],
      true,
      false,
    ],
    paint: {
      "fill-color": "hsl(185, 43%, 74%)",
      "fill-opacity": ["interpolate", ["linear"], ["zoom"], 10, 0, 10.5, 1],
      "fill-pattern": "wetland",
      "fill-translate-anchor": "viewport",
    },
  },
  {
    id: "hillshade",
    type: "fill",
    source: "composite",
    "source-layer": "hillshade",
    layout: {},
    paint: {
      "fill-color": [
        "match",
        ["get", "class"],
        "shadow",
        "hsl(56, 59%, 22%)",
        "hsl(0, 0%, 100%)",
      ],
      "fill-opacity": [
        "interpolate",
        ["linear"],
        ["zoom"],
        14,
        ["match", ["get", "level"], [67, 56], 0.08, [89, 78], 0.07, 0.15],
        16,
        0,
      ],
      "fill-antialias": false,
    },
  },
  {
    id: "road-primary-case",
    type: "line",
    metadata: { "mapbox:group": "1444855786460.0557" },
    source: "composite",
    "source-layer": "road",
    filter: [
      "all",
      ["==", ["get", "class"], "primary"],
      ["match", ["get", "structure"], ["none", "ford"], true, false],
      ["==", ["geometry-type"], "LineString"],
    ],
    layout: { "line-cap": "round", "line-join": "round" },
    paint: {
      "line-width": [
        "interpolate",
        ["exponential", 1.5],
        ["zoom"],
        10,
        1,
        18,
        2,
      ],
      "line-color": "hsl(230, 24%, 87%)",
      "line-gap-width": [
        "interpolate",
        ["exponential", 1.5],
        ["zoom"],
        5,
        0.75,
        18,
        32,
      ],
      "line-opacity": ["step", ["zoom"], 0, 10, 1],
    },
  },
  {
    id: "road-motorway-trunk-case",
    type: "line",
    metadata: { "mapbox:group": "1444855786460.0557" },
    source: "composite",
    "source-layer": "road",
    filter: [
      "all",
      ["match", ["get", "class"], ["motorway", "trunk"], true, false],
      ["match", ["get", "structure"], ["none", "ford"], true, false],
      ["==", ["geometry-type"], "LineString"],
    ],
    layout: { "line-cap": "round", "line-join": "round" },
    paint: {
      "line-width": [
        "interpolate",
        ["exponential", 1.5],
        ["zoom"],
        10,
        1,
        18,
        2,
      ],
      "line-color": "hsl(0, 0%, 100%)",
      "line-gap-width": [
        "interpolate",
        ["exponential", 1.5],
        ["zoom"],
        5,
        0.75,
        18,
        32,
      ],
      "line-opacity": [
        "step",
        ["zoom"],
        ["match", ["get", "class"], "motorway", 1, 0],
        6,
        1,
      ],
    },
  },
  {
    id: "road-secondary-tertiary",
    type: "line",
    metadata: { "mapbox:group": "1444855786460.0557" },
    source: "composite",
    "source-layer": "road",
    filter: [
      "all",
      ["match", ["get", "class"], ["secondary", "tertiary"], true, false],
      ["match", ["get", "structure"], ["none", "ford"], true, false],
      ["==", ["geometry-type"], "LineString"],
    ],
    layout: { "line-cap": "round", "line-join": "round" },
    paint: {
      "line-width": [
        "interpolate",
        ["exponential", 1.5],
        ["zoom"],
        5,
        0.1,
        18,
        26,
      ],
      "line-color": "hsl(0, 0%, 100%)",
    },
  },
  {
    id: "road-primary",
    type: "line",
    metadata: { "mapbox:group": "1444855786460.0557" },
    source: "composite",
    "source-layer": "road",
    filter: [
      "all",
      ["==", ["get", "class"], "primary"],
      ["match", ["get", "structure"], ["none", "ford"], true, false],
      ["==", ["geometry-type"], "LineString"],
    ],
    layout: { "line-cap": "round", "line-join": "round" },
    paint: {
      "line-width": [
        "interpolate",
        ["exponential", 1.5],
        ["zoom"],
        5,
        0.75,
        18,
        32,
      ],
      "line-color": "hsl(0, 0%, 100%)",
    },
  },
  {
    id: "road-motorway-trunk",
    type: "line",
    metadata: { "mapbox:group": "1444855786460.0557" },
    source: "composite",
    "source-layer": "road",
    filter: [
      "all",
      ["match", ["get", "class"], ["motorway", "trunk"], true, false],
      ["match", ["get", "structure"], ["none", "ford"], true, false],
      ["==", ["geometry-type"], "LineString"],
    ],
    layout: { "line-cap": "round", "line-join": "round" },
    paint: {
      "line-width": [
        "interpolate",
        ["exponential", 1.5],
        ["zoom"],
        5,
        0.75,
        18,
        32,
      ],
      "line-color": [
        "step",
        ["zoom"],
        [
          "match",
          ["get", "class"],
          "motorway",
          "hsl(26, 87%, 62%)",
          "hsl(0, 0%, 100%)",
        ],
        6,
        [
          "match",
          ["get", "class"],
          "motorway",
          "hsl(26, 87%, 62%)",
          "hsl(46, 80%, 60%)",
        ],
        9,
        [
          "match",
          ["get", "class"],
          "motorway",
          "hsl(26, 67%, 70%)",
          "hsl(46, 69%, 68%)",
        ],
      ],
    },
  },
  {
    id: "admin-1-boundary-bg",
    type: "line",
    metadata: { "mapbox:group": "1444934295202.7542" },
    source: "composite",
    "source-layer": "admin",
    filter: [
      "all",
      ["==", ["get", "admin_level"], 1],
      ["==", ["get", "maritime"], "false"],
      ["match", ["get", "worldview"], ["all", "US"], true, false],
    ],
    layout: { "line-join": "bevel" },
    paint: {
      "line-color": [
        "interpolate",
        ["linear"],
        ["zoom"],
        8,
        "hsl(35, 12%, 89%)",
        16,
        "hsl(230, 49%, 90%)",
      ],
      "line-width": ["interpolate", ["linear"], ["zoom"], 7, 3.75, 12, 5.5],
      "line-opacity": ["interpolate", ["linear"], ["zoom"], 7, 0, 8, 0.75],
      "line-dasharray": [1, 0],
      "line-translate": [0, 0],
      "line-blur": ["interpolate", ["linear"], ["zoom"], 3, 0, 8, 3],
    },
  },
  {
    id: "admin-0-boundary-bg",
    type: "line",
    metadata: { "mapbox:group": "1444934295202.7542" },
    source: "composite",
    "source-layer": "admin",
    minzoom: 1,
    filter: [
      "all",
      ["==", ["get", "admin_level"], 0],
      ["==", ["get", "maritime"], "false"],
      ["match", ["get", "worldview"], ["all", "US"], true, false],
    ],
    layout: {},
    paint: {
      "line-width": ["interpolate", ["linear"], ["zoom"], 3, 3.5, 10, 8],
      "line-color": [
        "interpolate",
        ["linear"],
        ["zoom"],
        6,
        "hsl(35, 12%, 89%)",
        8,
        "hsl(230, 49%, 90%)",
      ],
      "line-opacity": ["interpolate", ["linear"], ["zoom"], 3, 0, 4, 0.5],
      "line-translate": [0, 0],
      "line-blur": ["interpolate", ["linear"], ["zoom"], 3, 0, 10, 2],
    },
  },
  {
    id: "admin-1-boundary",
    type: "line",
    metadata: { "mapbox:group": "1444934295202.7542" },
    source: "composite",
    "source-layer": "admin",
    filter: [
      "all",
      ["==", ["get", "admin_level"], 1],
      ["==", ["get", "maritime"], "false"],
      ["match", ["get", "worldview"], ["all", "US"], true, false],
    ],
    layout: { "line-join": "round", "line-cap": "round" },
    paint: {
      "line-dasharray": [
        "step",
        ["zoom"],
        ["literal", [2, 0]],
        7,
        ["literal", [2, 2, 6, 2]],
      ],
      "line-width": ["interpolate", ["linear"], ["zoom"], 7, 0.75, 12, 1.5],
      "line-opacity": ["interpolate", ["linear"], ["zoom"], 2, 0, 3, 1],
      "line-color": [
        "interpolate",
        ["linear"],
        ["zoom"],
        3,
        "hsl(230, 14%, 77%)",
        7,
        "hsl(230, 8%, 62%)",
      ],
    },
  },
  {
    id: "admin-0-boundary",
    type: "line",
    metadata: { "mapbox:group": "1444934295202.7542" },
    source: "composite",
    "source-layer": "admin",
    minzoom: 1,
    filter: [
      "all",
      ["==", ["get", "admin_level"], 0],
      ["==", ["get", "disputed"], "false"],
      ["==", ["get", "maritime"], "false"],
      ["match", ["get", "worldview"], ["all", "US"], true, false],
    ],
    layout: { "line-join": "round", "line-cap": "round" },
    paint: {
      "line-color": "hsl(230, 8%, 51%)",
      "line-width": ["interpolate", ["linear"], ["zoom"], 3, 0.5, 10, 2],
    },
  },
  {
    id: "admin-0-boundary-disputed",
    type: "line",
    metadata: { "mapbox:group": "1444934295202.7542" },
    source: "composite",
    "source-layer": "admin",
    minzoom: 1,
    filter: [
      "all",
      ["==", ["get", "disputed"], "true"],
      ["==", ["get", "admin_level"], 0],
      ["==", ["get", "maritime"], "false"],
      ["match", ["get", "worldview"], ["all", "US"], true, false],
    ],
    layout: { "line-join": "round" },
    paint: {
      "line-dasharray": [1.5, 1.5],
      "line-color": "hsl(230, 8%, 51%)",
      "line-width": ["interpolate", ["linear"], ["zoom"], 3, 0.5, 10, 2],
    },
  },
  {
    id: "road-number-shield",
    type: "symbol",
    source: "composite",
    "source-layer": "road",
    minzoom: 6,
    filter: [
      "all",
      ["has", "reflen"],
      ["<=", ["get", "reflen"], 6],
      [
        "step",
        ["zoom"],
        ["==", ["geometry-type"], "Point"],
        11,
        [">", ["get", "len"], 5000],
        12,
        [">", ["get", "len"], 2500],
        13,
        [">", ["get", "len"], 1000],
        14,
        true,
      ],
    ],
    layout: {
      "text-size": 9,
      "icon-image": [
        "concat",
        ["get", "shield"],
        "-",
        ["to-string", ["get", "reflen"]],
      ],
      "icon-rotation-alignment": "viewport",
      "text-max-angle": 38,
      "symbol-spacing": ["interpolate", ["linear"], ["zoom"], 11, 150, 14, 200],
      "text-font": ["DIN Offc Pro Bold", "Arial Unicode MS Bold"],
      "symbol-placement": ["step", ["zoom"], "point", 11, "line"],
      "text-rotation-alignment": "viewport",
      "text-field": ["get", "ref"],
      "text-letter-spacing": 0.05,
    },
    paint: {
      "text-color": [
        "match",
        ["get", "shield_text_color"],
        "black",
        "hsl(230, 21%, 37%)",
        "yellow",
        "hsl(50, 81%, 74%)",
        "orange",
        "hsl(25, 73%, 63%)",
        "blue",
        "hsl(230, 40%, 58%)",
        "hsl(0, 0%, 100%)",
      ],
    },
  },
  {
    id: "natural-line-label",
    type: "symbol",
    source: "composite",
    "source-layer": "natural_label",
    minzoom: 4,
    filter: [
      "all",
      ["match", ["get", "class"], ["glacier", "landform"], true, false],
      ["==", ["geometry-type"], "LineString"],
      ["<=", ["get", "filterrank"], 4],
    ],
    layout: {
      "text-size": [
        "step",
        ["zoom"],
        ["step", ["get", "sizerank"], 18, 5, 12],
        17,
        ["step", ["get", "sizerank"], 18, 13, 12],
      ],
      "text-max-angle": 30,
      "text-field": ["coalesce", ["get", "name_en"], ["get", "name"]],
      "text-font": ["DIN Offc Pro Medium", "Arial Unicode MS Regular"],
      "symbol-placement": "line-center",
      "text-pitch-alignment": "viewport",
    },
    paint: {
      "text-halo-width": 0.5,
      "text-halo-color": "hsl(0, 0%, 100%)",
      "text-halo-blur": 0.5,
      "text-color": [
        "step",
        ["zoom"],
        [
          "step",
          ["get", "sizerank"],
          "hsl(26, 20%, 42%)",
          5,
          "hsl(26, 25%, 32%)",
        ],
        17,
        [
          "step",
          ["get", "sizerank"],
          "hsl(26, 20%, 42%)",
          13,
          "hsl(26, 25%, 32%)",
        ],
      ],
    },
  },
  {
    id: "natural-point-label",
    type: "symbol",
    source: "composite",
    "source-layer": "natural_label",
    minzoom: 4,
    filter: [
      "all",
      [
        "match",
        ["get", "class"],
        ["dock", "glacier", "landform", "water_feature", "wetland"],
        true,
        false,
      ],
      ["==", ["geometry-type"], "Point"],
      ["<=", ["get", "filterrank"], 4],
    ],
    layout: {
      "text-size": [
        "step",
        ["zoom"],
        ["step", ["get", "sizerank"], 18, 5, 12],
        17,
        ["step", ["get", "sizerank"], 18, 13, 12],
      ],
      "icon-image": [
        "step",
        ["zoom"],
        ["concat", ["get", "maki"], "-11"],
        15,
        ["concat", ["get", "maki"], "-15"],
      ],
      "text-font": ["DIN Offc Pro Medium", "Arial Unicode MS Regular"],
      "text-offset": [
        "step",
        ["zoom"],
        [
          "step",
          ["get", "sizerank"],
          ["literal", [0, 0]],
          5,
          ["literal", [0, 0.75]],
        ],
        17,
        [
          "step",
          ["get", "sizerank"],
          ["literal", [0, 0]],
          13,
          ["literal", [0, 0.75]],
        ],
      ],
      "text-anchor": [
        "step",
        ["zoom"],
        ["step", ["get", "sizerank"], "center", 5, "top"],
        17,
        ["step", ["get", "sizerank"], "center", 13, "top"],
      ],
      "text-field": ["coalesce", ["get", "name_en"], ["get", "name"]],
    },
    paint: {
      "icon-opacity": [
        "step",
        ["zoom"],
        ["step", ["get", "sizerank"], 0, 5, 1],
        17,
        ["step", ["get", "sizerank"], 0, 13, 1],
      ],
      "text-halo-color": "hsl(0, 0%, 100%)",
      "text-halo-width": 0.5,
      "text-halo-blur": 0.5,
      "text-color": [
        "step",
        ["zoom"],
        [
          "step",
          ["get", "sizerank"],
          "hsl(26, 20%, 42%)",
          5,
          "hsl(26, 25%, 32%)",
        ],
        17,
        [
          "step",
          ["get", "sizerank"],
          "hsl(26, 20%, 42%)",
          13,
          "hsl(26, 25%, 32%)",
        ],
      ],
    },
  },
  {
    id: "water-line-label",
    type: "symbol",
    source: "composite",
    "source-layer": "natural_label",
    filter: [
      "all",
      [
        "match",
        ["get", "class"],
        ["bay", "ocean", "reservoir", "sea", "water"],
        true,
        false,
      ],
      ["==", ["geometry-type"], "LineString"],
    ],
    layout: {
      "text-size": [
        "interpolate",
        ["linear"],
        ["zoom"],
        7,
        ["step", ["get", "sizerank"], 24, 6, 18, 12, 12],
        10,
        ["step", ["get", "sizerank"], 18, 9, 12],
        18,
        ["step", ["get", "sizerank"], 18, 9, 16],
      ],
      "text-max-angle": 30,
      "text-letter-spacing": [
        "match",
        ["get", "class"],
        "ocean",
        0.25,
        ["sea", "bay"],
        0.15,
        0,
      ],
      "text-font": ["DIN Offc Pro Italic", "Arial Unicode MS Regular"],
      "symbol-placement": "line-center",
      "text-pitch-alignment": "viewport",
      "text-field": ["coalesce", ["get", "name_en"], ["get", "name"]],
    },
    paint: {
      "text-color": [
        "match",
        ["get", "class"],
        ["bay", "ocean", "sea"],
        "hsl(205, 84%, 88%)",
        "hsl(230, 48%, 44%)",
      ],
    },
  },
  {
    id: "water-point-label",
    type: "symbol",
    source: "composite",
    "source-layer": "natural_label",
    filter: [
      "all",
      [
        "match",
        ["get", "class"],
        ["bay", "ocean", "reservoir", "sea", "water"],
        true,
        false,
      ],
      ["==", ["geometry-type"], "Point"],
    ],
    layout: {
      "text-line-height": 1.3,
      "text-size": [
        "interpolate",
        ["linear"],
        ["zoom"],
        7,
        ["step", ["get", "sizerank"], 24, 6, 18, 12, 12],
        10,
        ["step", ["get", "sizerank"], 18, 9, 12],
      ],
      "text-font": ["DIN Offc Pro Italic", "Arial Unicode MS Regular"],
      "text-field": ["coalesce", ["get", "name_en"], ["get", "name"]],
      "text-letter-spacing": [
        "match",
        ["get", "class"],
        "ocean",
        0.25,
        ["bay", "sea"],
        0.15,
        0.01,
      ],
      "text-max-width": [
        "match",
        ["get", "class"],
        "ocean",
        4,
        "sea",
        5,
        ["bay", "water"],
        7,
        10,
      ],
    },
    paint: {
      "text-color": [
        "match",
        ["get", "class"],
        ["bay", "ocean", "sea"],
        "hsl(205, 84%, 88%)",
        "hsl(230, 48%, 44%)",
      ],
    },
  },
  {
    id: "poi-label",
    type: "symbol",
    source: "composite",
    "source-layer": "poi_label",
    minzoom: 6,
    filter: [
      "let",
      "densityByClass",
      [
        "match",
        ["get", "class"],
        [
          "food_and_drink_stores",
          "historic",
          "landmark",
          "medical",
          "motorist",
        ],
        3,
        ["park_like", "sport_and_leisure", "visitor_amenities"],
        4,
        2,
      ],
      [
        "<=",
        ["get", "filterrank"],
        [
          "case",
          ["<", 0, ["var", "densityByClass"]],
          ["+", ["step", ["zoom"], 0, 16, 1, 17, 2], ["var", "densityByClass"]],
          ["var", "densityByClass"],
        ],
      ],
    ],
    layout: {
      "text-size": [
        "step",
        ["zoom"],
        ["step", ["get", "sizerank"], 18, 5, 12],
        17,
        ["step", ["get", "sizerank"], 18, 13, 12],
      ],
      "icon-image": [
        "step",
        ["zoom"],
        ["concat", ["get", "maki"], "-11"],
        15,
        ["concat", ["get", "maki"], "-15"],
      ],
      "text-font": ["DIN Offc Pro Medium", "Arial Unicode MS Regular"],
      "text-offset": [
        "step",
        ["zoom"],
        [
          "step",
          ["get", "sizerank"],
          ["literal", [0, 0]],
          5,
          ["literal", [0, 0.75]],
        ],
        17,
        [
          "step",
          ["get", "sizerank"],
          ["literal", [0, 0]],
          13,
          ["literal", [0, 0.75]],
        ],
      ],
      "text-anchor": [
        "step",
        ["zoom"],
        ["step", ["get", "sizerank"], "center", 5, "top"],
        17,
        ["step", ["get", "sizerank"], "center", 13, "top"],
      ],
      "text-field": ["coalesce", ["get", "name_en"], ["get", "name"]],
    },
    paint: {
      "icon-opacity": [
        "step",
        ["zoom"],
        ["step", ["get", "sizerank"], 0, 5, 1],
        17,
        ["step", ["get", "sizerank"], 0, 13, 1],
      ],
      "text-halo-color": "hsl(0, 0%, 100%)",
      "text-halo-width": 0.5,
      "text-halo-blur": 0.5,
      "text-color": [
        "step",
        ["zoom"],
        [
          "step",
          ["get", "sizerank"],
          [
            "match",
            ["get", "class"],
            "food_and_drink",
            "hsl(22, 55%, 55%)",
            "park_like",
            "hsl(100, 45%, 37%)",
            "education",
            "hsl(51, 40%, 40%)",
            "medical",
            "hsl(340, 30%, 52%)",
            "hsl(26, 20%, 42%)",
          ],
          5,
          [
            "match",
            ["get", "class"],
            "food_and_drink",
            "hsl(22, 85%, 38%)",
            "park_like",
            "hsl(100, 100%, 20%)",
            "education",
            "hsl(51, 100%, 20%)",
            "medical",
            "hsl(340, 39%, 42%)",
            "hsl(26, 25%, 32%)",
          ],
        ],
        17,
        [
          "step",
          ["get", "sizerank"],
          [
            "match",
            ["get", "class"],
            "food_and_drink",
            "hsl(22, 55%, 55%)",
            "park_like",
            "hsl(100, 45%, 37%)",
            "education",
            "hsl(51, 40%, 40%)",
            "medical",
            "hsl(340, 30%, 52%)",
            "hsl(26, 20%, 42%)",
          ],
          13,
          [
            "match",
            ["get", "class"],
            "food_and_drink",
            "hsl(22, 85%, 38%)",
            "park_like",
            "hsl(100, 100%, 20%)",
            "education",
            "hsl(51, 100%, 20%)",
            "medical",
            "hsl(340, 39%, 42%)",
            "hsl(26, 25%, 32%)",
          ],
        ],
      ],
    },
  },
  {
    id: "airport-label",
    type: "symbol",
    source: "composite",
    "source-layer": "airport_label",
    minzoom: 8,
    layout: {
      "text-line-height": 1.1,
      "text-size": ["step", ["get", "sizerank"], 18, 9, 12],
      "icon-image": [
        "step",
        ["get", "sizerank"],
        ["concat", ["get", "maki"], "-15"],
        9,
        ["concat", ["get", "maki"], "-11"],
      ],
      "text-font": ["DIN Offc Pro Medium", "Arial Unicode MS Regular"],
      "text-offset": [0, 0.75],
      "text-rotation-alignment": "viewport",
      "text-anchor": "top",
      "text-field": [
        "step",
        ["get", "sizerank"],
        ["coalesce", ["get", "name_en"], ["get", "name"]],
        15,
        ["get", "ref"],
      ],
      "text-letter-spacing": 0.01,
      "text-max-width": 9,
    },
    paint: {
      "text-color": "hsl(230, 48%, 44%)",
      "text-halo-color": "hsl(0, 0%, 100%)",
      "text-halo-width": 1,
    },
  },
  {
    id: "state-label",
    type: "symbol",
    source: "composite",
    "source-layer": "place_label",
    minzoom: 3,
    maxzoom: 9,
    filter: ["==", ["get", "class"], "state"],
    layout: {
      "text-size": [
        "interpolate",
        ["cubic-bezier", 0.85, 0.7, 0.65, 1],
        ["zoom"],
        4,
        ["step", ["get", "symbolrank"], 10, 6, 9.5, 7, 9],
        9,
        ["step", ["get", "symbolrank"], 24, 6, 18, 7, 14],
      ],
      "text-transform": "uppercase",
      "text-font": ["DIN Offc Pro Bold", "Arial Unicode MS Bold"],
      "text-field": [
        "step",
        ["zoom"],
        [
          "step",
          ["get", "symbolrank"],
          ["coalesce", ["get", "name_en"], ["get", "name"]],
          5,
          ["coalesce", ["get", "abbr"], ["get", "name_en"], ["get", "name"]],
        ],
        5,
        ["coalesce", ["get", "name_en"], ["get", "name"]],
      ],
      "text-letter-spacing": 0.15,
      "text-max-width": 6,
    },
    paint: {
      "text-color": "hsl(0, 0%, 0%)",
      "text-halo-color": "hsl(0, 0%, 100%)",
      "text-halo-width": 1,
    },
  },
  {
    id: "country-label",
    type: "symbol",
    source: "composite",
    "source-layer": "place_label",
    minzoom: 1,
    filter: ["==", ["get", "class"], "country"],
    layout: {
      "text-line-height": 1.1,
      "text-size": [
        "interpolate",
        ["cubic-bezier", 0.2, 0, 0.7, 1],
        ["zoom"],
        1,
        ["step", ["get", "symbolrank"], 11, 4, 9, 5, 8],
        9,
        ["step", ["get", "symbolrank"], 28, 4, 22, 5, 21],
      ],
      "icon-image": "dot-11",
      "text-font": ["DIN Offc Pro Medium", "Arial Unicode MS Regular"],
      "text-justify": [
        "step",
        ["zoom"],
        [
          "match",
          ["get", "text_anchor"],
          ["left", "bottom-left", "top-left"],
          "left",
          ["right", "bottom-right", "top-right"],
          "right",
          "center",
        ],
        7,
        "center",
      ],
      "text-offset": [
        "step",
        ["zoom"],
        [
          "match",
          ["get", "text_anchor"],
          "bottom",
          ["literal", [0, -0.25]],
          "bottom-left",
          ["literal", [0.2, -0.05]],
          "left",
          ["literal", [0.4, 0.05]],
          "top-left",
          ["literal", [0.2, 0.05]],
          "top",
          ["literal", [0, 0.25]],
          "top-right",
          ["literal", [-0.2, 0.05]],
          "right",
          ["literal", [-0.4, 0.05]],
          "bottom-right",
          ["literal", [-0.2, -0.05]],
          ["literal", [0, -0.25]],
        ],
        7,
        ["literal", [0, 0]],
      ],
      "text-anchor": [
        "step",
        ["zoom"],
        ["coalesce", ["get", "text_anchor"], "center"],
        7,
        "center",
      ],
      "text-field": ["coalesce", ["get", "name_en"], ["get", "name"]],
      "text-max-width": 6,
    },
    paint: {
      "icon-opacity": [
        "step",
        ["zoom"],
        ["case", ["has", "text_anchor"], 1, 0],
        7,
        0,
      ],
      "text-color": "hsl(0, 0%, 0%)",
      "text-halo-color": [
        "interpolate",
        ["linear"],
        ["zoom"],
        2,
        "rgba(255,255,255,0.75)",
        3,
        "hsl(0, 0%, 100%)",
      ],
      "text-halo-width": 1.25,
    },
  },

  {
    id: "hillshading",
    type: "fill",
    source: "thunderforest-outdoor",
    "source-layer": "hillshade",
    minzoom: 10,
    paint: {
      "fill-color": "#8f8f82",
      "fill-opacity": 0.05,
    },
  },
].map((m) => ({ ...DEFAULTS, ...m }));

export default all;
