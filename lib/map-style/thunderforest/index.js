const DEFAULTS = { minzoom: 10 };

const LANDCOVER_LOWZOOM = {
  id: "landcover-lowzoom",
  type: "fill",
  source: "thunderforest-outdoor",
  "source-layer": "landcover-lowzoom",
  layout: {},
  paint: {
    "fill-color": [
      "interpolate",
      ["linear"],
      ["get", "value"],
      0,
      "hsl(0, 0%, 0%)",
      20,
      "hsl(200, 100%, 100%)",
    ],
    "fill-opacity": [
      "interpolate",
      ["exponential", 1.5],
      ["zoom"],
      2,
      0.3,
      17,
      0,
    ],
    "fill-antialias": false,
  },
};

const LAND_USE = {
  id: "tf-landuse",
  type: "fill",
  source: "thunderforest-outdoor",
  "source-layer": "landuse",
  layout: {},
  paint: {
    "fill-color": [
      "interpolate",
      ["linear"],
      ["get", "value"],
      0,
      "hsl(0, 0%, 0%)",
      20,
      "hsl(200, 100%, 100%)",
    ],
    "fill-opacity": [
      "interpolate",
      ["exponential", 1.5],
      ["zoom"],
      2,
      0.3,
      17,
      0,
    ],
    "fill-antialias": false,
  },
};

const LAND_COVER = {
  id: "tf-landcover",
  type: "fill",
  source: "thunderforest-outdoor",
  "source-layer": "landcover",
  layout: {},
  paint: {
    "fill-color": [
      "case",
      ["==", ["get", "type"], "farmland"],
      "#8FD78B",
      ["==", ["get", "type"], "orchard"],
      "#B6D78B",
      ["==", ["get", "type"], "farm"],
      "#33CD1E",
      ["==", ["get", "type"], "wood"],
      "#238916",
      ["==", ["get", "type"], "meadow"],
      "#FFFA9E",
      ["==", ["get", "type"], "forest"],
      "#238916",
      ["==", ["get", "type"], "scree"],
      "#CFCFCF",
      ["==", ["get", "type"], "scrub"],
      "#CCF178",
      ["==", ["get", "type"], "grassland"],
      "#FBF35F",
      ["==", ["get", "type"], "grass"],
      "#FBF35F",
      ["==", ["get", "type"], "bare_rock"],
      "#B7B7B7",
      ["==", ["get", "type"], "sand"],
      "#CFCFCF",
      ["==", ["get", "type"], "heath"],
      "#FBF35F",
      ["==", ["get", "type"], "golf_course"],
      "#FBF35F",
      "#FF0000",
    ],
    "fill-opacity": ["interpolate", ["linear"], ["zoom"], 2, 0.3, 17, 0.15],
    "fill-antialias": false,
  },
};

const CONTOUR_LINES = {
  id: "contour-lines",
  type: "line",
  source: "thunderforest-outdoor",
  "source-layer": "elevation",
  paint: {
    "line-opacity": 0.5,
    "line-color": "#bfad7e",
    "line-width": [
      "case",
      ["==", ["get", "priority"], "major"],
      1.25,
      ["==", ["get", "priority"], "medium"],
      1,
      0.5,
    ],
  },
};

const CONTOUR_LABELS = {
  id: "contour-labels",
  type: "symbol",
  source: "thunderforest-outdoor",
  "source-layer": "elevation",
  filter: ["==", ["%", ["get", "height"], 50], 0],
  "min-zoom": 11,
  layout: {
    "text-field": "{height}m",
    "symbol-placement": "line",
    "text-pitch-alignment": "viewport",
    "text-max-angle": 25,
    "text-padding": 5,
    "text-font": ["DIN Offc Pro Medium", "Arial Unicode MS Regular"],
    "text-size": ["interpolate", ["linear"], ["zoom"], 15, 11, 20, 12],
  },
  paint: {
    "text-color": "#bfad7e",
    "text-halo-width": 2,
    "text-halo-color": "hsla(0, 0%, 100%, 0.75)",
  },
};

const POI_LABELS = {
  id: "poi-labels",
  type: "symbol",
  source: "thunderforest-outdoor",
  "source-layer": "poi-label",
  filter: [
    "in",
    "feature",
    ["literal", ["parking", "trailhead", "information"]],
  ],
  layout: {
    "text-field": ["coalesce", ["get", "name_en"], ["get", "name"]],
    "text-anchor": "bottom-left",
    "text-size": 12,
  },
  paint: {
    "text-color": "#333333",
    "text-opacity": 0.75,
    "text-halo-color": "#FFFFFF",
    "text-halo-width": 1.25,
  },
};

const PEAK_HEIGHTS = {
  id: "peak-heights",
  type: "symbol",
  source: "thunderforest-outdoor",
  "source-layer": "poi-label",
  filter: ["==", ["get", "feature"], "peak"],
  layout: {
    "text-field": "{ele}m",
    "text-anchor": "top",
    "text-size": 11,
  },
  paint: {
    "text-color": "#bfad7e",
    "text-halo-color": "#FFFFFF",
    "text-halo-width": 2,
  },
};

const WATERWAYS = {
  id: "waterways",
  type: "line",
  source: "thunderforest-outdoor",
  "source-layer": "waterway",
  paint: {
    "line-color": "#12a4e3",
    "line-width": 1,
  },
};

const SMALL_TRACKS_DEFAULT = {
  source: "thunderforest-outdoor",
  "source-layer": "road",
  filter: [
    "all",
    ["==", ["get", "highway"], "track"],
    ["!=", ["get", "tracktype"], "grade1"],
    ["!=", ["get", "tracktype"], "grade2"],
  ],
};

const SMALL_TRACKS = [
  {
    id: "tracks-bg",
    type: "line",
    paint: {
      "line-color": "#ffffff",
      "line-opacity": 1,
      "line-width": [
        "case",
        ["==", ["get", "tracktype"], "grade3"],
        3,
        ["==", ["get", "tracktype"], "grade4"],
        2,
        ["==", ["get", "tracktype"], "grade5"],
        0,
        0,
      ],
    },
  },
  {
    id: "tracks",
    type: "line",
    paint: {
      "line-width": [
        "case",
        ["==", ["get", "tracktype"], "grade3"],
        1,
        ["==", ["get", "tracktype"], "grade4"],
        1,
        ["==", ["get", "tracktype"], "grade5"],
        1,
        1,
      ],
      "line-color": "#000000",
      "line-opacity": 1,
      "line-gap-width": [
        "case",
        ["==", ["get", "tracktype"], "grade3"],
        2,
        ["==", ["get", "tracktype"], "grade4"],
        1,
        ["==", ["get", "tracktype"], "grade5"],
        1,
        0,
      ],
      "line-dasharray": [3, 2],
    },
  },
].map((l) => ({ ...SMALL_TRACKS_DEFAULT, ...l }));

const LARGE_TRACK_DEFAULT = {
  source: "thunderforest-outdoor",
  "source-layer": "road",
  filter: [
    "any",
    ["==", ["get", "tracktype"], "grade1"],
    ["==", ["get", "tracktype"], "grade2"],
  ],
};

const LARGE_TRACKS = [
  {
    id: "tracks-grade-1-outer",
    type: "line",
    paint: {
      "line-gap-width": 1.5,
      "line-width": 0.7,
      "line-color": "#000000",
      "line-opacity": 1,
    },
  },
  {
    id: "tracks-grade-1-bg",
    type: "line",
    paint: {
      "line-width": 1.5,
      "line-color": "#FFFFFF",
      "line-opacity": 1,
    },
  },
  {
    id: "tracks-grade-1-dash",
    type: "line",
    filter: ["==", ["get", "tracktype"], "grade1"],
    paint: {
      "line-width": 1.5,
      "line-color": "#000000",
      "line-opacity": 1,
      "line-dasharray": [2, 4],
    },
  },
  {
    id: "tracks-grade-2-dash",
    type: "line",
    filter: ["==", ["get", "tracktype"], "grade2"],
    paint: {
      "line-width": 1.5,
      "line-color": "#000000",
      "line-opacity": 1,
      "line-dasharray": [1, 2],
    },
  },
].map((l) => ({ ...LARGE_TRACK_DEFAULT, ...l }));

const PLACE_LABELS = {
  id: "place-labels",
  type: "symbol",
  source: "thunderforest-outdoor",
  "source-layer": "place-label",
  layout: {
    "text-field": ["coalesce", ["get", "name_en"], ["get", "name"]],
    "text-anchor": "center",
    "text-size": 10,
  },
  paint: {
    "text-opacity": 0.7,
    "text-color": "#000000",
    "text-halo-color": "#FFFFFF",
    "text-halo-width": 1.25,
  },
};

const WATER_POLYS = {
  id: "water-polys",
  type: "fill",
  source: "thunderforest-outdoor",
  "source-layer": "water",
  paint: {
    "fill-color": "#12a4e3",
    "fill-opacity": 0.5,
  },
};

const OCEAN_POLYS = {
  id: "ocean-polys",
  type: "fill",
  source: "thunderforest-outdoor",
  "source-layer": "ocean",
  paint: {
    "fill-color": "#12a4e3",
    "fill-opacity": 0.5,
  },
};

const PATHS = {
  id: "paths",
  type: "line",
  source: "thunderforest-outdoor",
  "source-layer": "path",
  paint: {
    "line-width": 1,
    "line-color": "#ff0000",
    "line-dasharray": [2, 2],
  },
};

const PISTE_LINES = {
  id: "piste-line",
  type: "line",
  source: "thunderforest-outdoor",
  "source-layer": "piste-line",
  paint: {
    "line-width": 1,
    "line-color": "#0000ff",
    "line-dasharray": [2, 2],
  },
};

const PISTE_AREAS = {
  id: "piste-area",
  type: "fill",
  source: "thunderforest-outdoor",
  "source-layer": "piste-area",
  paint: {
    "fill-color": "#0000ff",
    "fill-opacity": 0.2,
  },
};

const RAILWAYS = [
  {
    id: "railway",
    type: "line",
    source: "thunderforest-outdoor",
    "source-layer": "railway",
    paint: {
      "line-color": "#000000",
      "line-width": 1,
    },
  },

  {
    id: "railway-crosshatch",
    type: "line",
    source: "thunderforest-outdoor",
    "source-layer": "railway",
    paint: {
      "line-color": "#000000",
      "line-width": 5,
      "line-dasharray": [0.15, 3],
    },
  },
];

const SNOWMOBILE = {
  id: "snowmobile",
  type: "line",
  source: "thunderforest-outdoor",
  "source-layer": "snowmobile",
  paint: {
    "line-width": 3,
    "line-color": "#0000ff",
    "line-dasharray": [1, 1],
  },
};

const MOUNTAIN_BIKING = {
  id: "mountain-biking",
  type: "line",
  source: "thunderforest-outdoor",
  "source-layer": "mountain-biking",
  paint: {
    "line-width": 1.5,
    "line-color": "#00ff00",
    "line-dasharray": [2, 2],
  },
};

const POWER_LINES = [
  {
    id: "power",
    type: "line",
    source: "thunderforest-outdoor",
    "source-layer": "power",
    paint: {
      "line-width": 1.5,
      "line-color": "#222222",
    },
  },

  {
    id: "power-dots",
    type: "circle",
    source: "thunderforest-outdoor",
    "source-layer": "power",
    paint: {
      "circle-color": "#222222",
      "circle-radius": 3.5,
    },
  },
];

const PATH_LABELS = {
  id: "path-labels",
  type: "symbol",
  source: "thunderforest-outdoor",
  "source-layer": "path-label",
  layout: {
    "text-field": ["coalesce", ["get", "name_en"], ["get", "name"]],
    "text-anchor": "center",
    "text-pitch-alignment": "viewport",
    "text-size": 12,
    "symbol-placement": "line",
  },
  paint: {
    "text-color": "#001100",
    "text-halo-color": "#FFFFFF",
    "text-halo-width": 1.25,
  },
};

const ROADS = {
  id: "roads",
  type: "line",
  source: "thunderforest-outdoor",
  "source-layer": "road",
  filter: ["!=", ["get", "highway"], "track"],
  paint: {
    "line-width": [
      "case",
      ["==", ["get", "highway"], "trunk"],
      2,
      ["==", ["get", "highway"], "trunk_link"],
      1.5,
      ["==", ["get", "highway"], "motorway"],
      1.5,
      ["==", ["get", "highway"], "motorway_link"],
      1.5,
      ["==", ["get", "highway"], "primary"],
      2,
      ["==", ["get", "highway"], "secondary"],
      1.5,
      ["==", ["get", "highway"], "tertiary"],
      1.25,
      ["==", ["get", "highway"], "service"],
      1.0,
      [
        "match",
        ["get", "highway"],
        ["residential", "living_street"],
        true,
        false,
      ],
      0.75,
      ["==", ["get", "highway"], "track"],
      1,
      ["==", ["get", "highway"], "unclassified"],
      1,
      1,
    ],

    "line-color": [
      "case",
      ["==", ["get", "highway"], "trunk"],
      "#001100",
      ["==", ["get", "highway"], "trunk_link"],
      "#001100",
      ["==", ["get", "highway"], "motorway"],
      "#001100",
      ["==", ["get", "highway"], "motorway_link"],
      "#001100",
      ["==", ["get", "highway"], "primary"],
      "#001100",
      ["==", ["get", "highway"], "secondary"],
      "#003300",
      ["==", ["get", "highway"], "tertiary"],
      "#005500",
      ["==", ["get", "highway"], "service"],
      "#4400AA",
      [
        "match",
        ["get", "highway"],
        ["residential", "living_street"],
        true,
        false,
      ],
      "#4455AA",
      ["==", ["get", "highway"], "track"],
      "#000000",
      ["==", ["get", "highway"], "unclassified"],
      "#111111",

      "#FF0000",
    ],
    "line-opacity": 1,
    "line-gap-width": ["case", ["==", ["get", "highway"], "track"], 3, 0],
  },
};

const ROAD_LABELS = {
  id: "road-labels",
  type: "symbol",
  source: "thunderforest-outdoor",
  "source-layer": "road-label",
  layout: {
    "text-field": [
      "coalesce",
      ["get", "ref"],
      ["get", "name_en"],
      ["get", "name"],
    ],
    "text-anchor": "center",
    "text-pitch-alignment": "viewport",
    "text-size": 12,
    "symbol-placement": "line",
  },
  paint: {
    "text-color": "#001100",
    "text-halo-color": "hsla(0,100%,100%,0.75)",
    "text-halo-width": 2,
  },
};

const all = [
  LANDCOVER_LOWZOOM,
  LAND_USE,
  LAND_COVER,

  CONTOUR_LINES,
  CONTOUR_LABELS,

  POI_LABELS,

  PEAK_HEIGHTS,

  WATERWAYS,

  ...SMALL_TRACKS,
  ...LARGE_TRACKS,

  PLACE_LABELS,

  WATER_POLYS,
  OCEAN_POLYS,

  PATHS,

  PISTE_LINES,
  PISTE_AREAS,

  ...RAILWAYS,

  SNOWMOBILE,

  MOUNTAIN_BIKING,

  ...POWER_LINES,

  PATH_LABELS,

  ROADS,

  ROAD_LABELS,
].map((m) => ({ ...DEFAULTS, ...m }));

export default all;
