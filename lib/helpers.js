export default {
  displayAsUSD(number) {
    return Number(number).toLocaleString("en-US", {
      style: "currency",
      currency: "USD",
    });
  },

  USDBalance(amount, price) {
    let number = amount * price;
    return this.displayAsUSD(number);
  },

  truncate(string, length) {
    return string.substring(0, length) + "...";
  },
};
