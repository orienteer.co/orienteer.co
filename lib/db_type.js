import { pause, updateAccessor as UA } from "~/lib/utils";
import { Core, updateAccessor } from "~/components/mixins/ObjectEditor";
import Vue from "vue";
import { gql } from "~/plugins/net";

function upcaseFirst(string) {
  return string[0].toUpperCase() + string.substring(1).toLowerCase();
}

function capitalize(string) {
  var parts = string.split(/(?=[A-Z])|(?:_)/);

  return parts.map(upcaseFirst).join("");
}

function camelize(string) {
  var parts = string.split(/(?=[A-Z])|(?:_)/);

  return parts[0] + parts.slice(1).map(upcaseFirst).join("");
}

export default function (name, fields) {
  const FIELDS_STRING = fields.join(" ");
  const PATCH = `mutation ($nodeId: ID!, $patch: ${capitalize(name)}Patch!) {
  _ : update${capitalize(name)}(input: { nodeId: $nodeId, ${camelize(
    name
  )}Patch: $patch }) {
    object : ${camelize(name)} {${FIELDS_STRING} }
  }
}`;

  const CREATE = `mutation ( $object: ${capitalize(name)}Input!) {
  _ : create${capitalize(name)}(input: { ${camelize(name)}: $object }) {
    object : ${camelize(name)} {${FIELDS_STRING} }
  }
}`;

  const DELETE = `mutation ($nodeId: ID!) {
  _: delete${capitalize(name)}(input: { nodeId: $nodeId }) {
    clientMutationId
  }
}`;

  const EPSILON = 1e-15;

  var keys = fields;

  var computed = {};

  keys.forEach((k) => {
    computed[k] = updateAccessor(k);
  });

  return {
    computed: {
      ...computed,
      id: function () {
        return this.value.id;
      },
      nodeId: function () {
        return this.value.nodeId;
      },
    },
    data() {
      return {
        patch: true,
        debounce: 1000,
      };
    },
    created() {
      if (!this.nodeId) {
        this.input(this.value);
      }
    },
    methods: {
      async del() {
        let { result, error } = await gql(DELETE, { nodeId: this.nodeId });
        if (error) throw new Error(error);
      },
      async input(patch) {
        if (this.nodeId) {
          var variables = { nodeId: this.nodeId, patch: patch };
          var { result, error } = await gql(PATCH, variables);

          if (error) throw new Error(error);
          this.value = result._.object;
        } else {
          var variables = { object: patch };
          var { result, error } = await gql(CREATE, variables);

          if (error) throw new Error(error);

          this.value = { ...this.value, ...result._.object };
        }
      },
    },
  };
}
