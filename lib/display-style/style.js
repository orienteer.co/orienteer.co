import mapboxOutdoor from "../map-style/mapbox-outdoor";
import thunderforest from "../map-style/thunderforest";
import ownership from "../map-style/ownership";

export default {
  version: 8,
  name: "Custom",
  sources: {
    "thunderforest-outdoor": {
      url:
        "https://tile.thunderforest.com/thunderforest.outdoors-v2.json?apikey=f2e014081ef54bc69ae4966ed9310b2c",
      type: "vector",
      minzoom: 10,
    },
    composite: {
      url: "mapbox://mapbox.mapbox-streets-v8,mapbox.mapbox-terrain-v2",
      type: "vector",
      maxzoom: 10,
    },
    "mapbox-satellite": {
      type: "raster",
      url: "mapbox://mapbox.satellite",
      tileSize: 256,
    },
    "land-ownership": {
      tiles: ["https://tiles.orienteer.co/ownership/{z}/{x}/{y}.mvt"],
      type: "vector",
    },
  },
  sprite:
    "mapbox://sprites/dstaudigel/ck3ny7zy30ntr1co7is782iiy/ayuihl2d38ufkjhmu0pwx0cto",
  glyphs: "mapbox://fonts/dstaudigel/{fontstack}/{range}.pbf",
  layers: [
    {
      id: "background",
      type: "background",
      paint: {
        "background-color": "#ffffff",
      },
    },

    ...mapboxOutdoor,

    {
      id: "satellite",
      type: "raster",
      source: "mapbox-satellite",
      layout: {
        visibility: "none",
      },
    },

    ...thunderforest,
    ...ownership,
  ],
};
