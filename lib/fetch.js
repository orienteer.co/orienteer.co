function getFetch() {
  if (typeof fetch != "undefined") {
    return Promise.resolve(fetch);
  }

  return import("isomorphic-unfetch").then((e) => e.default || e);
}

const internalFetch = (url, options) =>
  getFetch().then((fetch) => {
    if (url.match(/^\//) && process.server) {
      url = `http://localhost:${process.env.PORT}${url}`;
    }

    return fetch(url, options);
  });

export default internalFetch;
