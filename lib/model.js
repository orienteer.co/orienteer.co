import { pause, updateAccessor as UA } from "~/lib/utils";
import { Core, updateAccessor } from "~/components/mixins/ObjectEditor";
import Vue from "vue";

export default function (endpoint, hash) {
  var keys = Object.keys(hash);

  var computed = {};

  keys.forEach((k) => {
    console.log("Adding accessor: ", k);
    computed[k] = updateAccessor(k);
  });

  return new Vue({
    mixins: [Core],
    computed,
    data() {
      return {
        value: hash,
        patch: false,
        debounce: 1000,
      };
    },
    methods: {
      input(patch) {
        console.log("INTERNAL PATCH: ", patch);
      },
    },
  });
}
