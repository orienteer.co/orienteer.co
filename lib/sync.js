import { gql } from "../plugins/net";

// The default filter doesn't send anything that already has an ID:
const DEFAULT_ARGS = {
  filter: (c) => !c.createdAt,
  batch: 10,
};

function upcaseFirst(string) {
  return string[0].toUpperCase() + string.substring(1).toLowerCase();
}

function capitalize(string) {
  var parts = string.split(/(?=[A-Z])|(?:_)/);

  return parts.map(upcaseFirst).join("");
}

function camelize(string) {
  var parts = string.split(/(?=[A-Z])|(?:_)/);

  return parts[0] + parts.slice(1).map(upcaseFirst).join("");
}

let outAtATime = 0;

export async function syncBatch(args) {
  console.log("SYNC BATCH");

  const { data, batch, filter, table } = { ...DEFAULT_ARGS, ...args };

  if (!data) throw new Error("Must specify data to sync");
  if (!table) throw new Error("Must specify table to sync");

  const set = [];
  for (let i = 0; set.length < batch && i < data.length; i++) {
    const row = data[i];

    if (filter(row)) set.push(row);
  }

  if (set.length == 0) {
    console.log("Returning prematurely");
    return { result: [] };
  }

  console.log("SET: ", set);

  const meta = [];
  const variables = {};
  const actions = [];
  const outKeys = [];

  set.forEach((r, i) => {
    variables[`input_${i}`] = r;
    meta.push(`$input_${i}:${capitalize(table)}Input!`);

    actions.push(
      `row_${i}:upsert${capitalize(table)}( input: { ${camelize(
        table
      )}: $input_${i}}) { _:${camelize(table)} { id createdAt updatedAt } }`
    );
    outKeys.push(`row_${i}`);
  });

  const mutation = `
mutation(${meta.join(",")}) {
${actions.join("\n")}
}
`;

  outAtATime++;
  console.log("OUT: ", outAtATime);
  const { error, result } = await gql(mutation, variables);
  outAtATime--;

  console.log("FINISHED SYNC");

  if (error) return { error };
  if (result) {
    return { result: outKeys.map((r) => result[r]._) };
  }
}
