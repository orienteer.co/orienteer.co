var fetch = require("isomorphic-unfetch");

module.exports = function go(hash) {
  if ("mutation" in hash) {
    hash.query = hash.mutation;
    delete hash.mutation;
  }

  return fetch(`http://localhost:${process.env.PORT}/graphql`, {
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    method: "POST",
    body: JSON.stringify(hash),
  }).then((x) => x.json());
};
