import fetch from "./fetch";

export default (req) => (query, variables = {}) => {
  var h = {
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    method: "POST",
    body: JSON.stringify({ query, variables }),
  };

  if (req) {
    h.headers["cookie"] = req.headers.cookie;
  } else {
    h.credentials = "include";
  }

  return fetch("/graphql", h).then((x) => x.json());
};
