import { pause, updateAccessor as UA } from "~/lib/utils";

export function updateAccessor(name) {
  return UA(["update", "outgoing", "value"], name);
}

export const Core = {
  data() {
    return {
      update: {},
      outgoing: {},
    };
  },
  computed: {
    saved() {
      return (
        Object.keys(this.update).length + Object.keys(this.outgoing).length == 0
      );
    },
    saving() {
      return Object.keys(this.outgoing).length != 0;
    },
  },
  methods: {
    onChange() {
      if (this.debounce) {
        if (this._changeTimer) {
          clearTimeout(this._changeTimer);
        }

        this._changeTimer = setTimeout(this.__debouncedOnChange, this.debounce);
      } else {
        return this.__debouncedOnChange();
      }
    },

    async __debouncedOnChange() {
      this._dirty = true;
      if (this._emitting) return;
      this._emitting = true;

      while (this._dirty) {
        try {
          this._dirty = false;
          var update;

          this.outgoing = { ...this.update };
          this.update = {};

          if (this.patch) update = { ...this.outgoing };
          else update = { ...this.value, ...this.outgoing };

          if (this.internalInput) {
            await this.internalInput(update);
          } else if (this.input) {
            await this.input(update);
          } else {
            this.$emit("input", update);
          }

          this.outgoing = {};
        } catch (x) {
          console.error(x);
          throw x;
        }
      }
      this._emitting = false;
    },
  },
};

export default {
  mixins: [Core],
  props: {
    value: { type: Object, required: true },
    patch: { type: Boolean, required: false, default: true },
    input: { type: Function, required: false },
    debounce: { type: Number, default: 1000 },
  },
};
