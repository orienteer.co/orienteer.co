import posthog from "posthog-js";

export default {
  props: ["feature"],
  data() {
    return {
      enabled: false,
    };
  },
  mounted() {
    posthog.onFeatureFlags(this.update);
  },
  methods: {
    update() {
      try {
        this.enabled = posthog.isFeatureEnabled(this.feature);
      } catch (x) {}
    },
  },
  watch: {
    feature: "update",
  },
  render(h) {
    if (this.enabled) return this.$slots.default;
    else return null;
  },
};
