import { boundsFor } from "~/lib/utils";

export default {
  props: {
    items: {},
    bounds: {},
    watching: {
      default: true,
    },
  },
  render(h) {
    return null;
  },
  inject: ["map"],
  mounted() {
    this.connect();
    this.fit();
  },
  watch: {
    items() {
      if (this.watching) {
        this.fit();
      }
    },
  },
  beforeDestroy() {
    this.disconnect();
  },
  methods: {
    fit() {
      if (!this.map) return;

      const bounds =
        this.bounds ||
        (this.items && this.items.length && boundsFor(this.items));

      if (bounds) {
        if (!this.map.__fitted) {
          this.map.fitBounds(bounds, {
            padding: 100,
            maxZoom: 13,
            animate: false,
          });
        } else {
          this.$nextTick(() => {
            this.map.fitBounds(bounds, {
              padding: 100,
              maxZoom: 13,
              animate: true,
            });
          });
        }

        this.map.__fitted = true;
      }
    },
    connect() {
      const offs = (this._offs = this._offs = []);

      function listen(map, event, handler) {
        map.on(event, handler);
        offs.push(() => map.off(event, handler));
      }

      for (let k in this.$listeners) {
        listen(this.map, k, this.$listeners[k]);
      }
    },
    disconnect() {
      for (let off of this._offs) {
        console.log("offing...");

        off();
      }
    },
  },
};
