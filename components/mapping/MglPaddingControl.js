import controlMixin from "vue-mapbox/src/components/UI/controls/controlMixin";

class PaddingControl {
  constructor(options) {
    this.height = options.height;
  }

  onAdd(map) {
    this.map = map;
    this.container = document.createElement("div");
    this.container.className = "padding-control";
    this.container.style.height = this.height;

    return this.container;
  }
  onRemove() {
    this.container.parentNode.removeChild(this.container);
    this.map = undefined;
  }
}

export default {
  name: "PaddingControl",
  mixins: [controlMixin],
  props: {
    height: {
      default: "20px",
    },
  },

  created() {
    this.control = new PaddingControl(this.$props);
    this.$_addControl();
  },
};
