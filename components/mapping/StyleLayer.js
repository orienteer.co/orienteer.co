import layerMixin from "vue-mapbox/src/components/layer/layerMixin";

export default {
  mixins: [layerMixin],
};
