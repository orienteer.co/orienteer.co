var JWT = require("jsonwebtoken");
var webpack = require("webpack");
var path = require("path");

module.exports = {
  modern: true,

  /*
   ** Headers of the page
   */
  head: {
    titleTemplate: "%s - orienteer.co",

    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
    ],
    link: [],
    script: [{ src: "https://js.stripe.com/v3/" }],
  },

  render: {
    resourceHints: false,
  },

  router: {
    middleware: ["target-domain"],
    extendRoutes(routes, resolve) {},
  },

  plugins: [
    "~/plugins/element.js",
    "~/plugins/components.js",
    "~/plugins/ui.js",
    "~/plugins/errors.js",
    { src: "~/plugins/geolocation.js", ssr: false },
    { src: "~/plugins/persisted-state.js", ssr: false },
    "~/plugins/net.js",
    "~/plugins/posthog.js",
    { src: "~/plugins/mapbox.js", ssr: false },
  ],

  modules: [
    "@nuxtjs/pwa",
    "@nuxtjs/sentry",
    "portal-vue/nuxt",
    "@brd.com/node-app/nuxt/module",
  ],

  sentry: {
    dsn: process.env.SENTRY_DSN,
    config: {
      release: process.env.VERSION,
    },
  },

  /*
   ** Customize the progress bar color
   */
  loading: { color: "#F9A538" },
  /*
   ** Build configuration
   */

  build: {
    parallel: true,
    cache: true,
    /*
     ** Run ESLint on save
     */

    extend(config, { isDev, isClient, isServer }) {
      if (isClient) {
        config.resolve.alias = {
          ...config.resolve.alias,
          fs: "pdfkit/js/virtual-fs.js",
        };

        config.module.rules = [
          ...config.module.rules,
          {
            enforce: "post",
            test: /fontkit[/\\]index.js$/,
            loader: "transform-loader?brfs",
          },
          {
            enforce: "post",
            test: /unicode-properties[/\\]index.js$/,
            loader: "transform-loader?brfs",
          },
          {
            enforce: "post",
            test: /linebreak[/\\]src[/\\]linebreaker.js/,
            loader: "transform-loader?brfs",
          },
          { test: /src[/\\]assets/, loader: "arraybuffer-loader" },
          { test: /\.afm$/, loader: "raw-loader" },
        ];
      }

      return config;
    },
  },
};
