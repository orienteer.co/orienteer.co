var server = require("./cluster");

server.start({
  dev: false,
  workers: process.env.WORKERS ? Number(process.env.WORKERS) : 1,
  workerMaxRequests: process.env.MAX_REQUESTS
    ? Number(process.env.MAX_REQUESTS)
    : 1000, // gracefully restart worker after 1000 requests
  workerMaxLifetime: process.env.MAX_LIFETIME
    ? Number(process.env.MAX_LIFETIME)
    : 3600, // gracefully restart worker after 1 hour
  rootDir: "/application/",
  address: "0.0.0.0",
  port: process.env.PORT,
});
