const test = require("ava");
const unet = require("unet");
const { uuid } = require("../lib/utils");
const REGEX = /[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}/;
function testGql(options = {}) {
  let cookie = options.cookie;

  return async function (query, variables) {
    let headers = {};

    if (cookie) {
      headers.cookie = cookie;
    }

    const { result, error, response } = await unet({
      method: "POST",
      url: "/graphql",
      body: { query, variables },
      headers,
    });

    const setCookie = response.headers["set-cookie"];

    if (setCookie) {
      if (setCookie.map) {
        cookie = setCookie.map((c) => c.split(";")[0]);
      } else {
        cookie = setCookie.split(";")[0];
      }
    }

    if (error) {
      const e = new Error("graphql-error");

      e.data = error;
      e.query = query;
      e.variables = variables;

      throw e;
    }
    if (result.errors) {
      const e = new Error("graphql-error");
      e.data = result.errors[0];
      e.query = query;
      e.variables = variables;
      throw e;
    }

    if (result.data) {
      let d = result.data;
      while (d && "_" in d) d = d._;

      return d;
    }

    return result;
  };
}

const LOGIN = `mutation($username: String!, $password: String!) {
_:login(input: { username: $username , password: $password}) {
  _:user {
      id
    }
  }
}`;

const REGISTER = `mutation($username:String!,$password:String!,$email:String!) {
_:register(input: { username: $username , password: $password, email: $email}) {
  _:user {
      id
    }
  }
}`;

const CURRENT_USER = `query {
_:currentUser {
  id
}
}`;

const CREATE_COURSE = `
mutation {
  _:createEmptyCourse( input: { } ) {
    _:course {
      id
    }
  }
}`;

const ADD_CHECKPOINT = `
mutation($input:CheckpointInput!) {
  _:createCheckpoint(input: {checkpoint:$input}) {
    _:checkpoint {
      id      
    }
  }
}
`;

const UPDATE_COURSE = `
mutation($id:UUID!,$patch:CoursePatch!) {
_:updateCourse(input: { id: $id, patch:$patch}) {
  _:course {
      id 
    }
  }
}`;

const DELETE_COURSE = `
mutation($id:UUID!,$patch:CoursePatch!) {
  deleteCourse(input: { id: $id }) {
    course {
      id 
    }
  }
}`;

const GET_COURSE = `
query($courseId:UUID!) {
  _:course(id: $courseId) {
    id checkpoints {
      nodes {
        lat lng
      }
    }
  }
}
`;

const ADD_USER = `
mutation($userId: Int!, $courseId: UUID!, $owner: Boolean, $read: Boolean, $write:Boolean) {
  createCourseUser(input: { courseUser: { userId:$userId, courseId:$courseId, owner:$owner, read:$read,write:$write} }) {
    courseUser {
      courseId userId
    }
  }
}
`;

const UPDATE_USER = `
mutation($userId: Int!, $courseId:UUID!, $read:Boolean, $write:Boolean, $owner:Boolean) {
  updateCourseUserByUserIdAndCourseId(input: { 
    userId: $userId,
    courseId: $courseId,
    patch: { read:$read write:$write owner:$owner }
  }) {
    clientMutationId
  }
}
`;

const userA = testGql();
const userB = testGql();
const userC = testGql();

let userAId, userBId, userCId;

test.before(async (t) => {
  const [a, b, c] = [uuid(), uuid(), uuid()].map((u) => u.replace(/-/g, "_"));

  const A = await userA(REGISTER, {
    username: `test_${a}`.substring(0, 23),
    password: "asdfasdf",
    email: `${a}@mailinator.com`,
  });
  const B = await userB(REGISTER, {
    username: `test_${b}`.substring(0, 23),
    password: "asdfasdf",
    email: `${b}@mailinator.com`,
  });
  const C = await userC(REGISTER, {
    username: `test_${c}`.substring(0, 23),
    password: "asdfasdf",
    email: `${c}@mailinator.com`,
  });

  userAId = A.id;
  userBId = B.id;
  userCId = C.id;
});

test("user a course invisible to user b until it is public", async (t) => {
  const course = await userA(CREATE_COURSE);

  t.regex(course.id, REGEX);

  await userA(ADD_CHECKPOINT, {
    input: { courseId: course.id, lat: "1.54", lng: "2.56" },
  });

  // Can't read:
  t.falsy(await userB(GET_COURSE, { courseId: course.id }));
  // Can't update:
  await t.throwsAsync(
    userB(UPDATE_COURSE, { id: course.id, patch: { public: true } })
  );
  // Can't delete:
  await t.throwsAsync(userB(DELETE_COURSE, { id: course.id }));
  // Can't add checkpoints
  await t.throwsAsync(
    userB(ADD_CHECKPOINT, {
      input: { courseId: course.id, lat: "12", lng: "3" },
    })
  );

  t.deepEqual(
    {
      id: course.id,
      checkpoints: { nodes: [{ lat: "1.54000000", lng: "2.56000000" }] },
    },
    await userA(GET_COURSE, { courseId: course.id })
  );

  await userA(UPDATE_COURSE, { id: course.id, patch: { public: true } });
  t.truthy(await userB(GET_COURSE, { courseId: course.id }));
});

test("user a course inaccessible to user b until it is shared with them", async (t) => {
  const course = await userA(CREATE_COURSE);

  t.regex(course.id, REGEX);

  await userA(ADD_CHECKPOINT, {
    input: { courseId: course.id, lat: "1.54", lng: "2.56" },
  });

  // Can't read:
  t.falsy(await userB(GET_COURSE, { courseId: course.id }));
  // Can't update:
  await t.throwsAsync(
    userB(UPDATE_COURSE, { id: course.id, patch: { public: true } })
  );
  // Can't delete:
  await t.throwsAsync(userB(DELETE_COURSE, { id: course.id }));
  // Can't add checkpoints
  await t.throwsAsync(
    userB(ADD_CHECKPOINT, {
      input: { courseId: course.id, lat: "12", lng: "3" },
    })
  );

  // Add the user as a read-only user:
  await userA(ADD_USER, { userId: userBId, courseId: course.id, read: true });

  // can read now:
  t.truthy(await userB(GET_COURSE, { courseId: course.id }));
  // but still can't update:
  await t.throwsAsync(
    userB(UPDATE_COURSE, { id: course.id, patch: { public: true } })
  );

  // Add write ability:
  await userA(UPDATE_USER + `# USERB: ${userBId}`, {
    userId: userBId,
    courseId: course.id,
    write: true,
  });

  // can still read:
  t.truthy(await userB(GET_COURSE, { courseId: course.id }));
  // now can update:
  t.truthy(
    userB(UPDATE_COURSE, { id: course.id, patch: { name: "a new name" } })
  );
  // but can't add add another user:
  await t.throwsAsync(
    userB(ADD_USER, { userId: userCId, courseId: course.id, read: true })
  );

  // add owner capability:
  await userA(UPDATE_USER, {
    userId: userBId,
    courseId: course.id,
    owner: true,
  });

  await userB(ADD_USER, { userId: userCId, courseId: course.id, read: true });
  // now user B can add third user:

  // now user C can read:
  t.truthy(await userC(GET_COURSE, { courseId: course.id }));
});
