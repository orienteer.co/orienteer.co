import fs from "fs";
import path from "path";

const storePath = path.resolve(__dirname, "../../store");

const moduleFiles = fs.readdirSync(storePath).filter((f) => f.match(/\.js$/));

let config;
let modules = {};

moduleFiles.forEach((m) => {
  const piece = require(path.resolve(storePath, m));
  const [ignored, name] = m.match(/^(.*?)\./) || [];

  if (name == "index") {
    config = piece;
  } else {
    modules[name] = { namespaced: true, ...piece };
  }
});

export default {
  ...config,
  modules,
};
