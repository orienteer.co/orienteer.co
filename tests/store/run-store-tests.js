import { createLocalVue } from "@vue/test-utils";
import Vuex from "vuex";
import storeConfig from "./store-config";
import { cloneDeep } from "lodash";
import test from "ava";
import unet from "unet";

function initializedStore() {
  const localVue = createLocalVue();
  localVue.use(Vuex);
  const store = new Vuex.Store(cloneDeep(storeConfig));

  store.commit("user", { id: 1 });

  return store;
}

const MOCK_COURSE = {
  course: {
    name: "Horse Ridge",
    id: "46b2083f-2239-4234-8fe7-02b3de0cdc3d",
    startCheckpoint: {
      lat: "43.94603196",
      lng: "-121.04337015",
      radius: 100,
    },
    endCheckpoint: {
      lat: "43.94603196",
      lng: "-121.04337015",
      radius: 100,
    },
    checkpoints: {
      _: [
        {
          id: "8b30e44a-dd90-44b8-81aa-564c9de7a0f1",
          name: "CP1",
          description: "Corner",
          lat: "43.93233028",
          lng: "-121.03653432",
          radius: 100,
        },
        {
          id: "8dae367b-9b5c-4f12-96fd-3825d89ef8b7",
          name: "CP2",
          description: "Jct",
          lat: "43.92271865",
          lng: "-121.03385430",
          radius: 100,
        },
        {
          id: "1627c122-e45d-48d1-8bfe-7c59b8221c29",
          name: "CP3",
          description: "Hairpin",
          lat: "43.92167952",
          lng: "-121.05111476",
          radius: 100,
        },
        {
          id: "5c1e1baf-cb96-42da-a3d6-7af3f11d6fb9",
          name: "Start/Finish",
          description: "Parking Lot",
          lat: "43.94603196",
          lng: "-121.04337015",
          radius: 100,
        },
      ],
    },
  },
};

test("offline run-a-run", async (t) => {
  const store = initializedStore();

  await store.dispatch("run/load", {
    courseId: "foo",
    gql: () => ({ result: MOCK_COURSE }),
  });

  unet.DEFAULT_MIDDLEWARE.unshift((options) => {
    return {
      response: { status: 0 },
      result: null,
      error: { message: "0 - connection error " },
    };
  });

  t.is(store.state.run.courseId, "46b2083f-2239-4234-8fe7-02b3de0cdc3d");

  // Move to somewhere other than start (it's at a checkpoint)
  await store.dispatch("run/location", { lat: 43.94603196, lng: -121.0511147 });

  t.is(store.getters["run/startable"], false);
  t.is(store.getters["run/endable"], false);

  await store.dispatch("run/start");

  t.deepEqual(store.state.run.error, { message: "Not at start!" });
  t.is(store.state.run.checkins.length, 0);

  store.commit("run/error", null);

  // Move near start/finish:
  await store.dispatch("run/location", { lat: 43.9461, lng: -121.04338 });

  t.is(store.getters["run/startable"], true);
  t.is(store.getters["run/endable"], false);

  await store.dispatch("run/start");

  t.is(store.state.run.checkins.length, 1);
  t.falsy(store.state.run.error);
  t.truthy(store.state.run.start);
  t.falsy(store.state.run.end);

  // Get going:
  await store.dispatch("run/location", { lat: 43.1, lng: -121.0 });
  t.is(store.state.run.checkins.length, 1);

  t.is(store.getters["run/startable"], false);
  t.is(store.getters["run/endable"], false);

  // Go get a checkpoint:

  await store.dispatch("run/location", {
    lat: 43.93233028,
    lng: -121.03653432,
  });

  t.is(store.state.run.checkins.length, 2);

  t.is(store.getters["run/startable"], false);
  t.is(store.getters["run/endable"], false);

  t.truthy(store.state.run.start, "Start should be truthy at end");
  t.falsy(store.state.run.end);

  // try to end prematurely:
  const res = await store.dispatch("run/finish");

  t.deepEqual(store.state.run.error, { message: "Not at End!" });
  store.commit("run/error", null);

  // Go to the finish:
  await store.dispatch("run/location", {
    lat: 43.94603,
    lng: -121.04337,
    radius: 100,
  });

  t.is(store.state.run.checkins.length, 3);
  t.is(store.getters["run/startable"], false);
  t.is(store.getters["run/endable"], true);

  await store.dispatch("run/finish");

  t.falsy(store.state.run.error, "Error fshould be false");
  t.truthy(store.state.run.start, "Start should be truthy at end");
  t.truthy(store.state.run.end, "End should be truthy at end");

  unet.DEFAULT_MIDDLEWARE.shift();
});
