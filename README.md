# orienteer.co

> Orienteering Online

## Dependencies:

- docker (and docker-compose), preferrably dinghy
- Self-signed HTTPS Certs (run bin/gen-certs), these are required for chrome to do geolocation for you!

## Getting Started

After installing docker, docker-compose, generating your certs, start the app by:

Generate certs:

```
bin/gen-certs
```

Start services & migrate:

```
make clean-start
```

Then you can open up https://orienteer.co.docker in your browser.

## Architecture

The basic architecture of orienteer.co is as follows:

- Data and core application logic is persisted in a Postgres database
- Postgraphile adapter creates a GraphQL schema and API endpoint for
  accessing it, secured by JWTs and Postgres roles.
- Nuxt.js is the framework used for rendering HTML content
