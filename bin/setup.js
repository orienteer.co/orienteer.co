#!/usr/bin/env node

class SetupPrivate {
  constructor() {
    this.foo = 1;
  }

  async dbpg(database, ...args) {
    this._pg = this._pg || {};
    let url;

    if (process.env.POSTGRES_ROOT_USER) {
      url = `postgresql://${process.env.POSTGRES_ROOT_USER}:${process.env.POSTGRES_ROOT_PASSWORD}@${process.env.POSTGRES_HOST}:5432/${database}`;
    } else if (process.env.POSTGRES_USER) {
      url = `postgresql://${process.env.POSTGRES_USER}:${process.env.POSTGRES_PASSWORD}@${process.env.POSTGRES_HOST}:5432/${database}`;
    } else if (process.env.DATABASE_URL) {
      url = process.env.DATABASE_URL;
    } else {
      throw new Exception("no postgres ");
    }

    if (!this._pg[url]) {
      const pgp = require("pg-promise")(/* { schema: ['public'] } */);

      this._pg[url] = pgp(url);
    }

    console.log(...args);
    try {
      return await this._pg[url].any(...args);
    } catch (x) {
      console.log(
        "Error with query ",
        (args[0] && args[0].substring(0, 100)) || args
      );
      throw x;
    }
  }

  cleanup() {
    for (let k in this._pg) {
      this._pg[k].$pool.end();
    }
  }

  pg(...args) {
    return this.dbpg(
      process.env.POSTGRES_ROOT_DB || process.env.POSTGRES_DB,
      ...args
    );
  }
}

class Setup extends SetupPrivate {
  async "init-db"() {
    const env = process.env;

    await this.pg(`DROP DATABASE IF EXISTS "${env.DATABASE_NAME}";`);
    await this.pg(`DROP DATABASE IF EXISTS "${env.DATABASE_NAME}_shadow";`);
    await this.pg(`DROP DATABASE IF EXISTS "${env.DATABASE_NAME}_test";`);

    await this.pg(`DROP ROLE IF EXISTS "${env.DATABASE_VISITOR}";`);
    await this.pg(`DROP ROLE IF EXISTS "${env.DATABASE_AUTHENTICATOR}";`);
    await this.pg(`DROP ROLE IF EXISTS "${env.DATABASE_OWNER}";`);

    await this.pg(
      `CREATE ROLE "${env.DATABASE_OWNER}" WITH LOGIN PASSWORD '${
        env.DATABASE_OWNER_PASSWORD
      }' ${env.NODE_ENV == "development" ? "SUPERUSER" : ""};`
    );
    await this.pg(
      `CREATE ROLE "${env.DATABASE_AUTHENTICATOR}" WITH LOGIN PASSWORD '${env.DATABASE_AUTHENTICATOR_PASSWORD}' NOINHERIT;`
    );

    await this.pg(`CREATE ROLE "${env.DATABASE_VISITOR}";`);

    console.log("Granting owner to user...");

    await this.pg(
      `GRANT "${env.DATABASE_VISITOR}" TO "${env.DATABASE_AUTHENTICATOR}";`
    );

    await this.pg(
      `CREATE DATABASE "${env.DATABASE_NAME}" OWNER "${env.DATABASE_OWNER}";`
    );

    await this.pg(
      `GRANT CONNECT ON DATABASE "${env.DATABASE_NAME}" TO "${env.DATABASE_OWNER}";`
    );
    await this.pg(
      `GRANT CONNECT ON DATABASE "${env.DATABASE_NAME}" TO "${env.DATABASE_AUTHENTICATOR}";`
    );
    await this.pg(
      `GRANT ALL ON DATABASE "${env.DATABASE_NAME}" TO "${env.DATABASE_OWNER}";`
    );

    await this.dbpg(
      env.DATABASE_NAME,
      `CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;
    CREATE EXTENSION IF NOT EXISTS citext WITH SCHEMA public;
    CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;
    CREATE EXTENSION IF NOT EXISTS cube WITH SCHEMA public;
    CREATE EXTENSION IF NOT EXISTS earthdistance WITH SCHEMA public;`
    );
  }

  async "init-review-db"() {
    const env = process.env;

    const DATABASE_NAME = process.env.DATABASE_NAME;
    const DATABASE_VISITOR = process.env.DATABASE_VISITOR;
    const DATABASE_OWNER = process.env.DATABASE_OWNER;
    const DATABASE_OWNER_PASSWORD = process.env.DATABASE_OWNER_PASSWORD;
    const DATABASE_AUTHENTICATOR = process.env.DATABASE_AUTHENTICATOR;
    const DATABASE_AUTHENTICATOR_PASSWORD =
      process.env.DATABASE_AUTHENTICATOR_PASSWORD;

    const rows = await this.pg(
      `select exists(SELECT datname FROM pg_catalog.pg_database WHERE lower(datname) = lower('${DATABASE_NAME}'));`
    );

    if (!rows[0].exists) {
      console.log("Doesn't exist - blowing out related things and re-creating");
      await this.pg(`DROP DATABASE IF EXISTS "${DATABASE_NAME}";`);
      await this.pg(`DROP DATABASE IF EXISTS "${DATABASE_NAME}_shadow";`);
      await this.pg(`DROP DATABASE IF EXISTS "${DATABASE_NAME}_test";`);

      await this.pg(`DROP ROLE IF EXISTS "${DATABASE_VISITOR}";`);
      await this.pg(`DROP ROLE IF EXISTS "${DATABASE_AUTHENTICATOR}";`);
      await this.pg(`DROP ROLE IF EXISTS "${DATABASE_OWNER}";`);

      await this.pg(
        `CREATE ROLE "${DATABASE_OWNER}" WITH LOGIN PASSWORD '${DATABASE_OWNER_PASSWORD}';`
      );
      await this.pg(
        `CREATE ROLE "${DATABASE_AUTHENTICATOR}" WITH LOGIN PASSWORD '${DATABASE_AUTHENTICATOR_PASSWORD}' NOINHERIT;`
      );

      await this.pg(`CREATE ROLE "${DATABASE_VISITOR}";`);
      const root = env.POSTGRES_ROOT_USER || env.POSTGRES_USER;

      await this.pg(`GRANT "${env.DATABASE_OWNER}" to "${root}";`);
      await this.pg(`GRANT "${env.DATABASE_AUTHENTICATOR}" to "${root}";`);
      await this.pg(`GRANT "${env.DATABASE_VISITOR}" to "${root}";`);

      await this.pg(
        `GRANT "${DATABASE_VISITOR}" TO "${DATABASE_AUTHENTICATOR}";`
      );
      await this.pg(
        `CREATE DATABASE "${DATABASE_NAME}" OWNER "${DATABASE_OWNER}";`
      );

      await this.pg(
        `GRANT CONNECT ON DATABASE "${DATABASE_NAME}" TO "${DATABASE_OWNER}";`
      );
      await this.pg(
        `GRANT CONNECT ON DATABASE "${DATABASE_NAME}" TO "${DATABASE_AUTHENTICATOR}";`
      );
      await this.pg(
        `GRANT ALL ON DATABASE "${DATABASE_NAME}" TO "${DATABASE_OWNER}";`
      );

      await this.dbpg(
        DATABASE_NAME,
        `CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;
    CREATE EXTENSION IF NOT EXISTS citext WITH SCHEMA public;
    CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;
    CREATE EXTENSION IF NOT EXISTS cube WITH SCHEMA public;
    CREATE EXTENSION IF NOT EXISTS earthdistance WITH SCHEMA public;
    `
      );
    } else {
      console.log("Exists - leaving it as is");
    }
  }
}

(async function () {
  const setup = new Setup();

  const operation = process.argv[2];
  const args = process.argv.slice(3);

  if (typeof setup[operation] != "function" || operation == "constructor") {
    console.error("Unknown operation ", operation);
    console.error(
      "  try one of... ",
      Object.getOwnPropertyNames(Object.getPrototypeOf(setup)).filter(
        (k) =>
          typeof setup[k] == "function" && !k.match(/^_/) && k != "constructor"
      )
    );
    process.exit(1);
  }

  try {
    await setup[operation](...args);
  } catch (x) {
    console.error(x.message || x);
    process.exit(1);
  } finally {
    setup.cleanup();
  }

  console.log("DONE");
  process.exit(0);
})();
