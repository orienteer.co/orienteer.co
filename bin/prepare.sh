#!/bin/sh


if docker pull $BUILD_IMAGE ; then
    docker create --name=test-$CI_BUILD_REF   $BUILD_IMAGE
    docker cp test-$CI_BUILD_REF:/application/package.json ./builder-package.json
    docker rm test-$CI_BUILD_REF
    
    if  cmp builder-package.json package.json ; then
        rm builder-package.json
        exit 0
    fi

    rm builder-package.json
fi

docker build -f Dockerfile.dev -t $BUILD_IMAGE .
docker push $BUILD_IMAGE

