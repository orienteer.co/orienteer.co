const { Deployment } = require("@brd.com/deploy-it");

const ENV_SLUG = process.env.CI_ENVIRONMENT_SLUG || "debug";
const IMAGE = process.env.IMAGE || "debug.docker/image:tag";

const IS_PROD = process.env.CI_JOB_STAGE == "deploy-prod";
const stripeEnv = IS_PROD ? `stripe-prod` : "stripe-test";

module.exports = () => {
  const D = new Deployment(`${ENV_SLUG}-worker`);

  D.replicas(1);

  const keys = `${ENV_SLUG}-keys`;

  D.template.push("spec.imagePullSecrets", { name: "gitlab-registry" });

  const C = D.template.container("app").image(IMAGE);

  C.set("args", ["bin/worker"]);

  C.set("resources.limits.cpu", "0.75");
  C.set("resources.requests.cpu", "0.1");

  C.env("NODE_ENV", "production")
    .env("BASE_URL", `https://${process.env.APP_HOST}`)

    // Postgres:
    .secretEnv("POSTGRES_HOST", { name: keys, key: "db-host" })

    .secretEnv("MAILGUN_DOMAIN", { name: "mailgun-prod", key: "domain" })
    .secretEnv("MAILGUN_API_KEY", { name: "mailgun-prod", key: "api-key" })

    // database:

    .secretEnv("DATABASE_NAME", { name: keys, key: "db-name" })

    .env("DATABASE_VISITOR", ENV_SLUG + "_visitor")

    // owner:
    .secretEnv("DATABASE_OWNER", { name: keys, key: "db-owner-user" })
    .secretEnv("DATABASE_OWNER_PASSWORD", {
      name: keys,
      key: "db-owner-password",
    })

    // authorized user:
    .secretEnv("DATABASE_AUTHENTICATOR", {
      name: keys,
      key: "db-auth-user",
    })
    .secretEnv("DATABASE_AUTHENTICATOR_PASSWORD", {
      name: keys,
      key: "db-auth-password",
    })

    //secrets
    .secretEnv("JWT_SECRET", {
      name: `${ENV_SLUG}-keys`,
      key: "jwt-secret",
    })
    .secretEnv("COOKIE_SECRET", {
      name: `${ENV_SLUG}-keys`,
      key: "cookie-secret",
    })
    // Stripe:
    .secretEnv("STRIPE_SECRET_KEY", {
      name: stripeEnv,
      key: "secret-key",
    })
    .secretEnv("STRIPE_PUBLISHABLE_KEY", {
      name: stripeEnv,
      key: "publishable-key",
    })
    .secretEnv("STRIPE_DEFAULT_PRODUCT", {
      name: stripeEnv,
      key: "default-product",
    })
    // AWS:
    .secretEnv("AWS_ENDPOINT", { name: "aws-cred", key: "endpoint" })
    .secretEnv("AWS_BUCKET", { name: "aws-cred", key: "bucket" })
    .secretEnv("AWS_ACCESS_KEY_ID", { name: "aws-cred", key: "access-key-id" })
    .secretEnv("AWS_SECRET_ACCESS_KEY", {
      name: "aws-cred",
      key: "secret-access-key",
    });

  return D;
};
