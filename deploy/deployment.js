const { Deployment } = require("@brd.com/deploy-it");

const ENV_SLUG = process.env.CI_ENVIRONMENT_SLUG || "debug";
const IMAGE = process.env.IMAGE || "debug.docker/image:tag";

const IS_PROD = process.env.CI_JOB_STAGE == "deploy-production";
const stripeEnv = IS_PROD ? `stripe-prod` : "stripe-test";

module.exports = () => {
  const D = new Deployment(ENV_SLUG);

  D.replicas(process.env.REPLICAS || 1);

  const keys = `${ENV_SLUG}-keys`;

  D.template.push("spec.imagePullSecrets", { name: "gitlab-registry" });

  D.label("git-ref", process.env.CI_GIT_REF);
  D.template.label("git-ref", process.env.CI_GIT_REF);

  const C = D.template.container("app").image(IMAGE);

  C.set("args", ["npm", "run", "start"]);

  C.set("resources.limits.cpu", "0.75");
  C.set("resources.requests.cpu", "0.1");

  C.port("app", 8080)
    .env("PORT", 8080)
    .env("HOST", "0.0.0.0")
    .env("NODE_ENV", "production")
    .env("BASE_URL", `https://${process.env.APP_HOST}`)

    // Postgres:
    .secretEnv("POSTGRES_HOST", { name: keys, key: "db-host" })
    // database:

    .secretEnv("DATABASE_NAME", { name: keys, key: "db-name" })
    .secretEnv("GOOGLE_CLIENT_ID", { name: "google-auth", key: "client-id" })
    .secretEnv("GOOGLE_CLIENT_SECRET", {
      name: "google-auth",
      key: "client-secret",
    })

    .env("DATABASE_VISITOR", ENV_SLUG + "_visitor")

    // owner:
    .secretEnv("DATABASE_OWNER", { name: keys, key: "db-owner-user" })
    .secretEnv("DATABASE_OWNER_PASSWORD", {
      name: keys,
      key: "db-owner-password",
    })

    // authorized user:
    .secretEnv("DATABASE_AUTHENTICATOR", {
      name: keys,
      key: "db-auth-user",
    })
    .secretEnv("DATABASE_AUTHENTICATOR_PASSWORD", {
      name: keys,
      key: "db-auth-password",
    })

    .secretEnv("SENTRY_DSN", {
      name: "sentry",
      key: "dsn",
    })

    .env("SENTRY_ENV", ENV_SLUG)

    //secrets
    .secretEnv("JWT_SECRET", {
      name: keys,
      key: "jwt-secret",
    })

    .secretEnv("COOKIE_SECRET", {
      name: keys,
      key: "cookie-secret",
    })

    // Stripe:
    .secretEnv("STRIPE_SECRET_KEY", {
      name: stripeEnv,
      key: "secret-key",
    })
    .secretEnv("STRIPE_PUBLISHABLE_KEY", {
      name: stripeEnv,
      key: "publishable-key",
    })
    .secretEnv("STRIPE_DEFAULT_PRODUCT", {
      name: stripeEnv,
      key: "default-product",
    })
    // AWS:
    .secretEnv("AWS_ENDPOINT", { name: "aws-cred", key: "endpoint" })
    .secretEnv("AWS_BUCKET", { name: "aws-cred", key: "bucket" })
    .secretEnv("AWS_ACCESS_KEY_ID", { name: "aws-cred", key: "access-key-id" })
    .secretEnv("AWS_SECRET_ACCESS_KEY", {
      name: "aws-cred",
      key: "secret-access-key",
    });

  return D;
};
