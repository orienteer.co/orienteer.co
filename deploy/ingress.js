const { Ingress } = require("@brd.com/deploy-it");

const ENV_SLUG = process.env.CI_ENVIRONMENT_SLUG || "debug";
const HOST = process.env.APP_HOST || "test-host.co";
const PATH = process.env.APP_PATH || "/test-path";

module.exports = () => {
  const I = new Ingress(ENV_SLUG);

  I.ingressClass("nginx");
  I.set(
    ["metadata", "annotations", "nginx.ingress.kubernetes.io/proxy-body-size"],
    "50m"
  );

  if (
    ["deploy-staging", "deploy-production"].includes(process.env.CI_JOB_STAGE)
  ) {
    I.set(
      ["metadata", "annotations", "cert-manager.io/acme-challenge-type"],
      "dns01"
    );
    I.set(
      ["metadata", "annotations", "cert-manager.io/cluster-issuer"],
      "letsencrypt-production"
    );
  }

  const host = I.host(`${HOST}`).path(`${PATH}`, ENV_SLUG, 8080);

  if (HOST == "orienteer.co") {
    I.host(`www.${HOST}`).path(`${PATH}`, ENV_SLUG, 8080);

    host.tls("www-" + process.env.TLS_SECRET);
  }

  if (process.env.TLS_SECRET) {
    host.tls(process.env.TLS_SECRET);
  }

  return I;
};
