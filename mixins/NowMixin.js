export default {
  data() {
    return {
      now: new Date(),
    };
  },
  mounted() {
    this.__now_interval = setInterval(() => (this.now = new Date()), 100);
  },
  beforeDestroy() {
    clearInterval(this.__now_interval);
  },
};
