import { get, getMap } from "~/lib/utils";

export default {
  data() {
    return { map: null };
  },
  mounted() {
    this.map = getMap(this.$parent);

    if (!this.map) {
      this._i = setInterval(() => {
        console.log("GETTING MAP");
        this.map = getMap(this.$parent);
        if (this.map) clearInterval(this._i);
      }, 50);
    }
  },
  beforeDestroy() {
    clearInterval(this._i);
  },
};
