const {
  Deployment,
  Service,
  PersistentVolumeClaim,
} = require("@brd.com/deploy-it");

const ENV_SLUG = process.env.CI_ENVIRONMENT_SLUG || "debug";

module.exports = function () {
  const D = new Deployment(`postgres`);
  D.template
    .container("postgres")
    .port("postgres", 5432)
    .image("postgres:11.6")
    // Postgres:
    .secretEnv("POSTGRES_USER", { name: `postgres-root`, key: "user" })
    .secretEnv("POSTGRES_PASSWORD", {
      name: `postgres-root`,
      key: "password",
    })
    .secretEnv("POSTGRES_DB", {
      name: `postgres-root`,
      key: "database",
    })
    .env("PGDATA", "/db-data/data")
    .volumeMount("postgres-storage", "/db-data");

  D.template.volume("postgres-storage", {
    persistentVolumeClaim: { claimName: `postgres` },
  });

  const S = new Service(`postgres`);
  S.port("postgres", 5432).selector("app", `postgres`);

  const PVC = new PersistentVolumeClaim(`postgres`);
  PVC.size("5Gi");
  PVC.accessMode("ReadWriteOnce");

  return [D, PVC, S];
};
