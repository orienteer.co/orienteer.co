#!/usr/bin/env node

const { Pod } = require("@brd.com/deploy-it");

const P = new Pod(`init-db-${process.env.CI_JOB_ID}`);

const CI_ENVIRONMENT_SLUG =
  process.env.CI_ENVIRONMENT_SLUG || "new-schema-review"; // env-slug";
const keysSecret = `${CI_ENVIRONMENT_SLUG}-keys`;
const ROOT_IMAGE = process.env.ROOT_IMAGE || "foo:123";

P.restartPolicy("Never");
P.push("spec.imagePullSecrets", { name: "gitlab-registry" });

let args = process.argv.slice(2);
if (args.length == 0)
  args = [
    "bash",
    "-c",
    "bin/setup.js init-review-db && bin/migrate && bin/migrate watch --once",
  ];

P.set("metadata.labels.type", "db-init-pod");

P.container("app")
  // .command(command)
  .args(args)
  .image(ROOT_IMAGE)
  .secretEnv("POSTGRES_HOST", { name: `postgres-root`, key: "host" })
  // root
  .secretEnv("POSTGRES_USER", { name: `postgres-root`, key: "user" })
  .secretEnv("POSTGRES_PASSWORD", {
    name: `postgres-root`,
    key: "password",
  })
  .secretEnv("POSTGRES_DB", { name: `postgres-root`, key: "database" })

  // database:

  .secretEnv("DATABASE_NAME", { name: keysSecret, key: "db-name" })

  .env("DATABASE_VISITOR", CI_ENVIRONMENT_SLUG + "-visitor")

  // owner:
  .secretEnv("DATABASE_OWNER", { name: keysSecret, key: "db-owner-user" })
  .secretEnv("DATABASE_OWNER_PASSWORD", {
    name: keysSecret,
    key: "db-owner-password",
  })

  // authorized user:
  .secretEnv("DATABASE_AUTHENTICATOR", {
    name: keysSecret,
    key: "db-auth-user",
  })
  .secretEnv("DATABASE_AUTHENTICATOR_PASSWORD", {
    name: keysSecret,
    key: "db-auth-password",
  });

const o = JSON.parse(JSON.stringify(P));

delete o.apiVersion;
delete o.kind;
delete o.metadata.name;

console.log(JSON.stringify(o));
