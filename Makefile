.kubeauth:
	echo "You need a .kubeauth file - ask Dan" ; exit 1

clean-start:
	# Wipe out existing world:
	docker-compose down -v --remove-orphans ; docker-compose down -v
	# start everything fresh, in the background:
	docker-compose up -d --force-recreate
	# wait to be sure postgres is in fact up:
	sleep 2
	# initialize the database:
	docker-compose exec app bash -c 'bin/setup.js init-db && bin/migrate watch --once && bin/worker --once'

start-with-prod-data: .kubeauth
	docker-compose stop pg
	docker-compose rm -f pg
	docker volume rm orienteer_pg-data-2

	docker-compose up -d pg

	sleep 5

	docker-compose run --rm app bash -c 'bin/setup.js init-db && bin/migrate && bin/worker --once'

	bin/db pg_dump prod -a --disable-triggers --schema=app_private --schema=app_public | bin/db psql local

	docker-compose restart app

	echo 'All done - w00t!'