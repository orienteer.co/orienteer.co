const targetHost = process.env.TARGET_HOST;

export default async function ({ redirect, req, res }) {
  if (process.server && req && req.hostname && targetHost) {
    if (
      req.hostname != targetHost ||
      req.headers["x-forwarded-proto"] == "http"
    ) {
      redirect(`https://${targetHost}${req.url}`);
    }
  }
}
