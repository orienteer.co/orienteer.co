export default async function (ctx) {
  let user;

  if (process.server) {
    var session = ctx.req.session;
    user = session && session.passport && session.passport.user;
  } else {
    user = ctx.store.state.user;
  }

  console.log("USER: ", user);

  if (["/", "/reset", "/verify"].includes(ctx.route.path)) return;

  console.log("THEN: ", ctx.route.path);

  if (!user) {
    ctx.redirect(`/?then=${encodeURIComponent(ctx.route.path)}`);
  }
}
