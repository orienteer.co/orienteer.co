export const state = () => ({
  session_id: null,
  user: null,
  env: {},
  meta: {},
});

export const mutations = {
  session_id(state, val) {
    state.session_id = val;
  },

  user(state, value) {
    state.user = value;
  },

  env(state, list) {
    for (var i in list) {
      var k = list[i];
      state.env[k] = process.env[k];
    }
  },

  error(state, error) {
    console.error("ERROR: ", error);
  },

  meta(state, meta) {
    state.meta = { ...state.meta, ...meta };
  },
};

export const actions = {
  async nuxtServerInit({ commit }, { req, app }) {
    var passport = req.session.passport;

    if (passport && passport.user) {
      commit("session_id", req.session.passport.user);
      const { result, error } = await app.$gql(
        "query { user:currentUser { id isAdmin isVerified name username avatarUrl } }"
      );
      if (result) {
        commit("user", result.user);
      }
    }

    commit("env", ["STRIPE_PUBLISHABLE_KEY"]);
  },
};
