import {
  geodistance,
  getter,
  get,
  byKey,
  ascending,
  uuid,
  filterInPlace,
} from "../lib/utils";

import { gql as globalGql } from "../plugins/net";
import { syncBatch } from "../lib/sync";

export const state = () => ({
  id: null,

  userId: null,
  courseName: null,

  currentLocation: null,
  courseId: null,

  courseStartCheckpoint: null,
  courseEndCheckpoint: null,

  checkpoints: [],
  start: null,
  end: null,

  checkins: [],

  events: [],

  syncing: false,
  netError: null,
  error: null,
});

function atCheckpoint(location, checkpoint) {
  if (!location) return false;
  if (!checkpoint) return false;

  const distance = geodistance(
    checkpoint.lat,
    checkpoint.lng,
    location.lat,
    location.lng
  );
  if (isNaN(distance)) return false;
  return distance < (checkpoint.radius || 100);
}

function capitalize(string) {
  if (!string) return string;
  if (typeof string != "string") return string;
  return ((string[0] && string[0].toUpperCase()) || "") + string.substring(1);
}

export const mutations = {
  reset(state) {
    state.id = null;

    state.currentLocation = null;
    state.courseName = null;
    state.courseId = null;
    state.checkpoints = [];
    state.start = null;
    state.end = null;

    state.checkins = [];
    state.events = [];
    state.error = null;
  },

  fixup(state) {
    state.events.forEach((e) => {
      if (!e.runId) e.runId = state.id;
      if (!e.userId) e.userId = state.userId;
    });

    state.checkins.forEach((c) => {
      if (!c.runId) c.runId = state.id;
    });
  },

  drop_createdAt(state) {
    state.events.forEach((e) => {
      e.createdAt = null;
      e.updatedAt = null;
    });

    state.checkins.forEach((c) => {
      c.createdAt = null;
      c.updatedAt = null;
    });
  },

  course(state, hash) {
    for (var k in hash) {
      state["course" + capitalize(k)] = hash[k];
    }
  },

  checkpoints(state, list) {
    state.checkpoints = [...list];
  },

  currentLocation(state, location) {
    state.currentLocation = { ...location };
  },

  location(state, location) {
    if (!state.id) {
      console.log("Cannot save location, no run ID");
      return;
    }

    const checkins = [];

    for (var i in state.checkpoints) {
      const cp = state.checkpoints[i];
      const distance = geodistance(cp.lat, cp.lng, location.lat, location.lng);

      cp.distance = distance;

      if (distance > (cp.radius || 100)) continue;

      const existingCheckin = state.checkins.find(
        (c) => c.checkpointId == cp.id
      );
      if (existingCheckin) return false;

      const checkin = {
        time: new Date(),
        runId: state.id,
        id: uuid(),
        checkpointId: cp.id,
        lat: location.lat,
        lng: location.lng,
        runEventId: location.runEventId,
      };

      checkins.push(checkin);
    }

    if (checkins.length) state.checkins = [...checkins, ...state.checkins];
  },

  id(state, id) {
    if (state.id) {
      console.log("STATE ID already set, not overwriting");
      return;
    }

    state.id = id;
  },

  cleanEvents(state, maximumEventCount = 500) {
    console.log(`BEF EVENTS ${state.events.length}`);
    state.events = state.events.filter((e) => !e.createdAt);

    if (state.events.length > maximumEventCount)
      state.events = state.events.filter((e, i) => i % 2 == 0);
    console.log(`AFT EVENTS ${state.events.length}`);
  },

  userId(state, userId) {
    state.userId = userId;
  },

  start(state) {
    state.start = new Date();
  },

  end(state) {
    state.end = state.end || new Date();
  },

  event(state, hash) {
    if (!state.id) {
      console.log("Cannot save event, no run ID");

      return;
    }

    var eventKeys = ["lat", "lng", "type"];
    const event = {
      id: hash.id || uuid(),
      runId: state.id,
      userId: state.userId,
      time: new Date(),
      userAgent:
        (typeof navigator != "undefined" && navigator.userAgent) ||
        "no user agent",
    };

    for (var k in hash) {
      if (eventKeys.includes(k)) {
        event[k] = hash[k];
      } else {
        event.data = event.data || {};
        event.data[k] = hash[k];
      }
    }

    state.events.push(event);
  },

  purgeEvents(state) {
    state.events = state.events.filter((e) => !e.createdAt);
  },

  synced(state, { table, id, createdAt, updatedAt }) {
    const record = state[table].find((r) => r.id == id);
    if (record) {
      record.updatedAt = updatedAt;
      record.createdAt = createdAt;
    }
  },

  error(state, value) {
    state.error = value;
  },

  netError(state, value) {
    state.netError = value;
  },
};

export const getters = {
  startable(state) {
    if (state.start) return false;
    if (state.end) return false;
    if (!state.currentLocation) return false;

    if (state.courseStartCheckpoint) {
      if (
        state.currentLocation &&
        atCheckpoint(state.currentLocation, state.courseStartCheckpoint)
      ) {
        return true;
      }

      return false;
    }

    return true;
  },
  endable(state) {
    if (!state.start) return false;
    if (state.end) return false;

    if (state.courseEndCheckpoint) {
      if (
        state.currentLocation &&
        atCheckpoint(state.currentLocation, state.courseEndCheckpoint)
      ) {
        return true;
      }

      return false;
    }

    return true;
  },
};

export const actions = {
  async load(
    { dispatch, commit, state },
    { gql, courseId, allowCache = true }
  ) {
    if (courseId != state.courseId) {
      commit("reset");
    }

    commit("id", uuid());

    const { result, error } = await (gql || globalGql)(COURSE_BY_ID, {
      id: courseId,
    });

    commit("course", result.course);
    commit("checkpoints", result.course.checkpoints._);

    dispatch("sync");
  },

  async location({ dispatch, commit, state }, location) {
    commit("currentLocation", location);

    if (!state.start) return;
    if (state.end) return;

    const eventId = uuid();
    commit("event", {
      ...location,
      id: eventId,
      type: `location-${location.type}`,
    });

    commit("cleanEvents");
    commit("location", { ...location, runEventId: eventId });

    dispatch("sync");
  },

  async start({ dispatch, commit, state }) {
    if (
      state.courseStartCheckpoint &&
      !atCheckpoint(state.currentLocation, state.courseStartCheckpoint)
    ) {
      commit("error", { message: "Not at start!" });
      return;
    }

    commit("start");
    dispatch("location", state.currentLocation);

    dispatch("sync");
  },

  async finish({ dispatch, commit, state }) {
    if (
      state.courseEndCheckpoint &&
      !atCheckpoint(state.currentLocation, state.courseEndCheckpoint)
    ) {
      commit("error", { message: "Not at End!" });
      return;
    }

    commit("end");
    dispatch("sync");
  },

  async syncBatch({ state, commit }, args) {
    const { result, error } = await syncBatch(args);

    if (error) {
      commit("error", error);
      console.log("ERROR: ", error);
      return false;
    }

    if (result && result.length == 0) return false;

    result.forEach((r) => commit("synced", { table: args.storeKey, ...r }));
    return true;
  },

  async sync({ state, dispatch, commit, rootState }, options) {
    const { force } = { force: false, ...options };

    if (rootState.meta.runSyncing) return;
    commit("meta", { runSyncing: true }, { root: true });

    try {
      commit("netError", null);
      commit("userId", rootState.user.id);

      // if (!state.id) {
      let id = state.id;
      if (!id) {
        id = uuid();

        commit("id", id);
      }

      const { result, error } = await globalGql(CREATE_RUN, {
        runId: id,
        courseId: state.courseId,
      });

      if (error) return commit("netError", error);

      {
        const { error } = await globalGql(UPDATE_RUN, {
          id: id,
          patch: {
            startedAt: state.start,
            endedAt: state.end,
          },
        });
        if (error) return commit("netError", error);
      }

      if (force) {
        commit("drop_createdAt");
      }

      commit("fixup");

      console.log("SYNCING RUN EVENTS");
      while (
        await dispatch("syncBatch", {
          storeKey: "events",
          table: "run_event",
          data: state.events,
          batch: 30,
        })
      );

      console.log("SYNCING CHECKINS");
      while (
        await dispatch("syncBatch", {
          storeKey: "checkins",
          table: "checkin",
          data: state.checkins,
          batch: 10,
        })
      );
    } finally {
      commit("meta", { runSyncing: false }, { root: true });
    }
  },
};

const COURSE_BY_ID = `
query($id: UUID!) {
  course(id: $id) {
    name id
    startCheckpoint { lat lng radius }
    endCheckpoint { lat lng radius }
    checkpoints(orderBy: [NAME_ASC]) {
      _:nodes {
        id name description lat lng radius
      }
    }
  }
}`;

const CREATE_RUN = `
mutation($courseId: UUID,$runId: UUID) {
  _:createEmptyRun(
    input: { courseId: $courseId, runId: $runId }
  ) {
    run {
      id
    }
  }
}
`;

const UPDATE_RUN = `
mutation($id: UUID! $patch: RunPatch!) {
  updateRun(input: {
    id: $id
    patch: $patch
  }) {
    run {
      id startedAt endedAt public courseId updatedAt createdAt 
    }
  }
}`;
