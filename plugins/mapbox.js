/* eslint-disable import/no-extraneous-dependencies */
import Vue from "vue";
import * as Mgl from "vue-mapbox";
import Mapbox from "mapbox-gl";

for (var k in Mgl) {
  Vue.component(k, Mgl[k]);
}

Vue.prototype.$mapbox = Mapbox;
