import Vue from "vue";

Vue.mixin({
  beforeDestroy() {
    this.$geoStopWatches();
  },
});

Vue.prototype.$geoStopWatches = function () {
  (this._geoWatchIds || []).forEach((h) => {
    navigator.geolocation.clearWatch(h);
    if (h.onCancel) h.onCancel();
  });
};

// if you return false from the callback, you want stop repeated callbacks
// if you return true from the callback, you want to continue to receive
// returning anything else from.
// you may specify an "onCancel" callback in options that gets called when
// the watch is terminated because of some factor outside your control.

Vue.prototype.$geoWatch = function (callback, in_options = {}) {
  this._geoWatchIds = this._geoWatchIds || [];
  let watchId;
  let options = {
    enableHighAccuracy: true,
    maximumAge: 30000,
    timeout: 60000,
    ...in_options,
  };

  const handle = (res) => {
    if (res === false) {
      navigator.geolocation.clearWatch(watchId);
      this._geoWatchIds = this._geoWatchIds.filter((h) => h.id != watchId);
    }
  };

  watchId = navigator.geolocation.watchPosition(
    function (position) {
      handle(
        callback(null, {
          lat: position.coords.latitude,
          lng: position.coords.longitude,
          altitude: position.coords.altitude,
          accuracy: position.coords.accuracy,
          time: new Date(position.timestamp),
        })
      );
    },
    function (error) {
      handle(callback(error, null));
    },
    options
  );

  this._geoWatchIds.push({ id: watchId, onCancel: in_options.onCancel });

  return () => {
    navigator.geolocation.clearWatch(watchId);
    this._geoWatchIds = this._geoWatchIds.filter((h) => h.id != watchId);
  };
};

Vue.prototype.$geoPosition = function (in_options = {}) {
  return new Promise((resolve, reject) => {
    let options = {
      ...in_options,
      onCancel: function () {
        resolve(null);
      },
    };

    navigator.geolocation.getCurrentPosition(
      (position) => {
        resolve({
          lat: position.coords.latitude,
          lng: position.coords.longitude,
          altitude: position.coords.altitude,
          accuracy: position.coords.accuracy,
          time: new Date(position.timestamp),
        });
      },
      (error) => reject(error),
      { maximumAge: 1000, timeout: 10000, enableHighAccuracy: true, ...options }
    );
  });
};
