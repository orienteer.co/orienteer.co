import posthog from "posthog-js";
import Vue from "vue";

export default (ctx, inject) => {
  if (!process.client) return;

  posthog.init("GTkT-rkJJxCd2ZD-_YK648-cqIVxqVo44DSoo1_VfLk", {
    api_host: "https://analytics.orienteer.co",
  });

  new Vue({
    store: ctx.store,
    computed: {
      user() {
        return this.$store.state.user;
      },
    },
    watch: {
      user: {
        deep: true,
        immediate: true,
        handler() {
          if (this.user) {
            const user = { ...this.user };
            posthog.identify(user.id);
            delete user.id;
            posthog.people.set(user);
          } else {
            posthog.reset();
          }
        },
      },
    },
  });
};
