import Vue from "vue";

export function query(query, variablesCallback) {
  return async function (ctx) {
    let variables;

    if (variablesCallback) {
      variables = await variablesCallback.call(this, ctx);
    }

    var res = await ctx.app.$gql(query, variables);

    console.log("QUERY RES: ", res);

    if (res.error) {
      console.log("RES ", res);

      ctx.error({ statusCode: 500, message: res.error });

      return res;
    }

    return { ...res.result, error: null };
  };
}
