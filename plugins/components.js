import Vue from "vue";
import { get as DP } from "dot-prop";

Vue.filter("get", function (value, argument) {
  return DP(value, argument);
});

Vue.component("vue-json-pretty", () =>
  import("vue-json-pretty").then((d) => d.default || d)
);

function leftPad(length, padding, value) {
  var string = String(value);
  return padding.repeat(Math.max(0, length - string.length)) + string;
}

const MILLIS_PER_SECOND = 1000;
const MILLIS_PER_MINUTE = 60 * MILLIS_PER_SECOND;
const MILLIS_PER_HOUR = 60 * MILLIS_PER_MINUTE;
const MILLIS_PER_DAY = 24 * MILLIS_PER_HOUR;

Vue.filter("formatElapsed", function (time) {
  let millis, seconds, minutes, hours, days;

  if (time && typeof time == "object" && "seconds" in time) {
    millis = leftPad(3, "0", Math.floor(time.seconds * 1000));
    seconds = leftPad(2, "0", Math.floor(time.seconds) || 0);
    minutes = leftPad(2, "0", time.minutes || 0);
    hours = leftPad(2, "0", time.hours || 0);
    days = time.days;
  } else if (time) {
    const inMillis = new Date(time).getTime();

    millis = leftPad(3, "0", Math.floor(inMillis % 1000));
    seconds = leftPad(2, "0", Math.floor(inMillis / MILLIS_PER_SECOND) % 60);
    minutes = leftPad(2, "0", Math.floor(inMillis / MILLIS_PER_MINUTE) % 60);
    hours = leftPad(2, "0", Math.floor(inMillis / MILLIS_PER_HOUR) % 24);
    days = Math.floor(inMillis / MILLIS_PER_DAY);
  } else {
    return "";
  }

  var set = [days, hours, minutes, seconds];
  while (Number(set[0]) == 0) set.shift();

  return set.join(":") + `.${millis.substring(0, 1)}`;
});
