import unet from "unet";

export function _gql(options) {
  return async function (query, variables) {
    let headers = {};

    if (options) {
      if (typeof options.get == "function") {
        headers.cookie = options.get("cookie");
        headers.authorization = options.get("authorization");
      } else {
        headers.cookie = options.cookie;
        headers.authorization = options.authorization;
      }
    }

    const { result, error } = await unet({
      method: "POST",
      url: "/graphql",
      body: { query, variables },
      headers,
    });

    if (error) {
      return { error: error.errors || error };
    }
    if (result.errors) {
      return { error: result.errors[0] };
    }
    if (result.data) return { result: result.data };

    return result;
  };
}

export default ({ req }, inject) => {
  inject("net", unet);
  inject("gql", _gql(req));
};

export const gql = _gql();
