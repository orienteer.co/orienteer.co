import createPersistedState from "vuex-persistedstate";

export default ({ store }) => {
  createPersistedState({
    key: "ort",
    paths: ["run"],
    //      paths: [...]
    //      ...
  })(store);
};
