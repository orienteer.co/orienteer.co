import Vue from "vue";

import Loader from "~/components/ui/Loader";
import LoadingDots from "~/components/ui/LoadingDots";
import Card from "~/components/ui/Card";
import ActionBar from "~/components/ui/ActionBar";
import FeatureGuard from "~/components/ui/FeatureGuard";
import StatefulButton from "~/components/ui/StatefulButton";
import ErrorView from "~/components/ui/ErrorView";

Vue.component("row", require("~/components/ui/Row").default);
Vue.component("column", require("~/components/ui/Column").default);
Vue.component("cell", require("~/components/ui/Cell").default);

Vue.component("card", Card);
Vue.component("loading-dots", LoadingDots);
Vue.component("loader", Loader);
Vue.component("action-bar", ActionBar);
Vue.component("stateful-button", StatefulButton);
Vue.component("feature-guard", FeatureGuard);

Vue.component("error-view", ErrorView);
