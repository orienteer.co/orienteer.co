import Vue from "vue";

const shortId = function () {
  const sets = {
    // I and O omitted for readability:
    L: "ABCDEFGHJKLMNPQRSTUVWXYZ",
    // 0 and 1 omitted for readability:
    D: "23456789",
  };

  const random = (set) => set[Math.floor(Math.random() * set.length)];
  return "LDLDLLL".replace(/(L|D)/g, (m, k) => random(sets[k]));
};

function ignore() {
  return false;
}

export function report(...args) {
  if (typeof $nuxt == "undefined") return;
  const Sentry = $nuxt.$sentry;

  if (!Sentry || ignore(args[0])) {
    console.error(`REPORT: ${args}`);
    if (args[0] instanceof Error) {
      console.error(`stack: ${args[0].stack}`);
    }

    return;
  }

  // already reported:
  if (args[0].key) return args[0].key;

  const key = args[0].key || shortId();

  if (typeof args[0] == "object") {
    args[0].key = key;
  }

  if (args[0] instanceof Error) {
    const extra = args[0].extra || args.slice(1);
    Sentry.captureException(args[0], { extra, tags: { key: key } });
    return key;
  }
  if (typeof args[0] == "string") {
    Sentry.captureException(new Error(args[0]), {
      extra: args.slice(1),
      tags: { key: key },
    });
    return key;
  }

  var name = "object";
  var object = args[0];

  try {
    if ("errors" in object) name = object.errors[0];
    if ("error" in object) name = object.error;

    if (object.response) {
      name = `${object.response.status}-${object.response.url}`;
    }
  } catch (x) {}

  Sentry.captureException(new Error(name), {
    extra: args[0],
    tags: { key: key },
  });

  return key;
}

export default (ctx, inject) => {
  inject("report", report);
};

// Vue.prototype.$report = report;
