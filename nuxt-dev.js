// In development, we need to have some middleware executed before
// things are handed off to the nuxt process.  We need to do this in
// as similar a way as possible compared to production, but we still
// want to have the right hot-reload-goodness of both Nuxt and the
// API, so we run them concurrently and let them cycle independently.

const { Nuxt, Builder } = require("nuxt");

const bodyParser = require("body-parser");
const cookieSession = require("cookie-session");

const app = require("express")();
const isProd = false;
const port = 8081;

// We instantiate Nuxt.js with the options
const config = require("./nuxt.config.js");
config.dev = !isProd;
const nuxt = new Nuxt(config);

app.use(
  cookieSession({
    name: "session",
    keys: [process.env.COOKIE_SECRET],

    httpOnly: true,
    signed: true,
    sameSite: true,

    // Cookie Options
    maxAge: 24 * 7 * 60 * 60 * 1000, // 24 hours
  })
);

// Render every route with Nuxt.js
app.use(nuxt.render);

// Build only in dev mode with hot-reloading
if (config.dev) {
  new Builder(nuxt)
    .build()
    .then(listen)
    .catch((error) => {
      console.error(error);
      process.exit(1);
    });
} else {
  listen();
}

function listen() {
  // Listen the server
  app.listen(port, "0.0.0.0");
  console.log("Server listening on `localhost:" + port + "`.");
}
